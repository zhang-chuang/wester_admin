<?php

namespace Iwester\Http\Controllers\Admin;

use Iwester\Http\Model\Icon;
use Illuminate\Http\Request;
use Iwester\Http\Controllers\Admin\BaseController;
use Iwester\Http\Model\User\User;

class AdminIndexController extends BaseController
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
    }

    /**
     * 后台布局
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function layout()
    {
        return view('iwester::layouts.admin.layout');
    }

    /**
     * 后台首页
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('iwester::admin.index.index');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * 所有icon图标
     */
    public function icons()
    {
        $icons = Icon::orderBy('sort', 'desc')->get();
        return response()->json(['code' => 0, 'msg' => '请求成功', 'data' => $icons]);
    }
}
