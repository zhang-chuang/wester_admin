<?php

namespace Iwester\Http\Controllers\Admin\Book;

use Illuminate\Support\Facades\Auth;
use Iwester\Http\Controllers\Admin\BaseController;
use App\Console\Commands\SiteMap;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Iwester\Http\Model\Article\Article;
use Iwester\Http\Model\Article\ArticleComment;
use Iwester\Http\Model\Article\ArticleCommentLike;
use Iwester\Http\Model\Article\ArticleContent;
use Iwester\Http\Model\Article\ArticleLike;
use Iwester\Http\Model\Article\ArticleTag;
use Iwester\Http\Model\Book\BookCategory;
use Iwester\Http\Model\Config\SiteConfig;
use Iwester\Http\Requests\Book\BookCategoryRequest;
use Monolog\Handler\IFTTTHandler;
use Symfony\Component\Translation\Tests\Writer\BackupDumper;
use View;


class AdminBookCategoryController extends BaseController
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
    }

    /**
     * 书籍 - 分类管理
     * @return Response
     */
    public function category()
    {
        $categories = BookCategory::allCategory();;
        return view('iwester::admin.book.category.category', ['categories'=> $categories]);
    }

    /**
     * 添加栏目
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create($id)
    {
        $category = BookCategory::find($id);
        return view('iwester::admin.book.category.create', ['category' => $category]);
    }

    /**
     * 编辑栏目
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($pId, $id)
    {
        $parentCategory = BookCategory::find($pId);
        $category = BookCategory::find($id);
        return view('iwester::admin.book.category.edit', ['parentCategory'=> $parentCategory,'category' => $category]);
    }

    /**
     * 保存栏目
     * @param Request $request
     */
    public function store(BookCategoryRequest $request)
    {
        try {
            \DB::beginTransaction();
            $cate = $request->get('cate', false);
            $category = new BookCategory();
            $category = $category->create($cate);
            if ($cate['parent_id'] != 0 ){
                $parentCategory = BookCategory::find($cate['parent_id']);
                BookCategory::changeParentSubCategory($parentCategory, $category);
            }
            $category->sub_category = json_encode([$category->id]);
            $category->top_category = $cate['parent_id'] ? ($parentCategory->top_category ==0 ? $parentCategory->id : $parentCategory->top_category) : 0;
            $category->save();
            \DB::commit();
            return ['code' => 200, 'message' => '设置成功！'];
        } catch (\Exception $e) {
            \DB::rollBack();
            return ['code' => 201, 'message' => '保存失败！'];
        }
    }

    /**
     * 编辑保存栏目
     * @param Request $request
     */
    public function update(BookCategoryRequest $request)
    {
        try {
            \DB::beginTransaction();
            $categoryId = $request->get('id', false);
            $cate = $request->get('cate', false);
            $category = BookCategory::find($categoryId);
            $category->update($cate);
            \DB::commit();
            return ['code' => 200, 'message' => '设置成功！'];
        } catch (\Exception $e) {
            \DB::rollBack();
            return ['code' => 201, 'message' => '保存失败！'];
        }
    }

    /**
     * 删除分类 - 文章划到父级 - 一级分类不可删除
     * @param $id
     * @return array
     * @throws \Exception
     */
    public function destroy($id)
    {
        try {
            return ['code' => 201, 'message' => '暂无开放'];
            \DB::beginTransaction();
            $articleCategory = BookCategory::find($id);
            $articleCategory->delete();
            \DB::commit();
            return ['code' => 200, 'message' => '删除成功！'];
        } catch (\Exception $e) {
            \DB::rollBack();
            return ['code' => 201, 'message' => $e->getMessage()];
        }
    }
}
