<?php

namespace Iwester\Http\Controllers\Admin\Book;

use Illuminate\Support\Facades\Auth;
use Iwester\Http\Controllers\Admin\BaseController;
use App\Console\Commands\SiteMap;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Iwester\Http\Model\Article\Article;
use Iwester\Http\Model\Article\ArticleComment;
use Iwester\Http\Model\Article\ArticleCommentLike;
use Iwester\Http\Model\Article\ArticleContent;
use Iwester\Http\Model\Article\ArticleLike;
use Iwester\Http\Model\Article\ArticleTag;
use Iwester\Http\Model\Book\Book;
use Iwester\Http\Model\Book\BookAuthor;
use Iwester\Http\Model\Book\BookCategory;
use Iwester\Http\Model\Book\BookChapter;
use Iwester\Http\Model\Book\BookReadLog;
use Iwester\Http\Model\Config\SiteConfig;
use Monolog\Handler\IFTTTHandler;
use Symfony\Component\Translation\Tests\Writer\BackupDumper;
use View;
use Iwester\Http\Model\Article\ArticleCategory;


class AdminBookShelfController extends BaseController
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
    }

    /**
     * 书籍 - 书架列表
     * @return Response
     */
    public function bookshelf(Request $request)
    {
        $category_id = $request->get('category_id', false);
        $title = $request->get('title', false);
        $book_author_id = $request->get('book_author_id', false);
        $pageSize = 10;
        $whereArr = [];
        if ($category_id) $whereArr['book_category_id'] = $category_id;
        if ($book_author_id) $whereArr['book_author_id'] = $book_author_id;
        if ($title) $whereArr[] = ['name', 'like', '%' . $title . '%'];
        $books = Book::getBookData(Book::$pubWith,$whereArr, [['id' => 'desc']], $pageSize, true);
        $categories = BookCategory::allCategory();
        return view('iwester::admin.book.book.bookshelf', [
            'books' => $books,
            'categories' => $categories,
            'params' => $request->all()
        ]);
    }

    /**
     * 添加 - 编辑书籍
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id = 0)
    {
        $categories = BookCategory::allCategory();
        $book = $id ? Book::find($id): null;
        return view('iwester::admin.book.book.edit', [
            'categories' => $categories,
            'book' => $book,
        ]);
    }

    /**
     * 保存
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            \DB::beginTransaction();
            $id = $request->input('id', false);
            $param = $request->get('book', []);
            if ($id) {
                $book = Book::find($id);
                $book->update($param);
            } else {
                $article = Book::create($param);
            }
            \DB::commit();
            return redirect(route('book.bookshelf'));
        } catch (\Exception $e) {
            \DB::rollBack();
            return redirect()->route('book.bookshelf')->with('errors', ['系统错误，保存失败']);
        }
    }


    /**
     * 书籍 - 章节列表
     * @return Response
     */
    public function chapters(Request $request, $id)
    {
        $book = Book::find($id);
        $chapters = BookChapter::where('book_id', $id)->orderBy('chapter_id', 'desc')->paginate(20);
        return view('iwester::admin.book.book.chapters', [
            'book' => $book,
            'chapters' => $chapters,
            'params' => $request->all(),
        ]);
    }

    /**
     * 章节编辑
     * @return Response
     */
    public function chapterEdit(Request $request, $id)
    {
        $chapter = BookChapter::find($id);
        return view('iwester::admin.book.book.chapter_edit', [
            'chapter' => $chapter,
        ]);
    }

    /**
     * 保存
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function chapterStore(Request $request)
    {
        try {
            \DB::beginTransaction();
            $id = $request->input('id', false);
            $param = $request->get('article', []);
            $chapter = BookChapter::find($id);
            $chapter->update($param);
            \DB::commit();
            return redirect(route('book.bookshelf.chapters', ['id'=> $chapter->book_id]));
        } catch (\Exception $e) {
            \DB::rollBack();
            return redirect()->route('book.bookshelf.chapters', ['id'=> $chapter->book_id])->with('errors', ['系统错误，保存失败']);
        }
    }

    /**
     * 章节阅读日志
     * @return Response
     */
    public function bookReadLog(Request $request)
    {
        $logs = BookReadLog::with(['book','chapter'])->orderBy('id', 'desc')->paginate(20);
        return view('iwester::admin.book.book.read_log', [
            'logs' => $logs,
        ]);
    }
}
