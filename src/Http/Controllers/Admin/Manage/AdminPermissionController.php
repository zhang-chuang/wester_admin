<?php

namespace Iwester\Http\Controllers\Admin\Manage;

use Iwester\Http\Controllers\Admin\BaseController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Iwester\Http\Model\User\Permission;
use Iwester\Http\Model\User\Role;
use Iwester\Http\Model\User\User;
use Iwester\Http\Requests\PermissionStoreRequest;
use Symfony\Component\Translation\Tests\Writer\BackupDumper;
use View;
use Iwester\Http\Model\Config\Banner;
use Iwester\Http\Requests\UserCreateRequest;


class AdminPermissionController extends BaseController
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
    }

    /**
     * 权限管理 列表
     * @return Response
     */
    public function permission(Request $request, $pId = 0)
    {
        $parentPermission = $pId ? Permission::find($pId) : false;
        $permissions = Permission::permission($pId);
        return view('iwester::admin.manage.permission.permission', [
            'pId'=> $pId,
            'parentPermission'=> $parentPermission,
            'permissions'=> $permissions
        ]);
    }

    /**
     * 添加权限 / 编辑权限
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($pId, $id = false)
    {
        $parentPermission = $pId ? Permission::find($pId) : false;
        $curPermission = $id ? Permission::find($id) : false;
        return view('iwester::admin.manage.permission.edit', [
            'parentPermission'=> $parentPermission,
            'curPermission'=> $curPermission,
        ]);
    }

    /**
     * 保存
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(PermissionStoreRequest $request)
    {
        try {
            \DB::beginTransaction();
            $id = $request->input('id', 0);
            $data = $request->get('permission', []);
            if ($id){
                $permission = Permission::find($id);
                if (!$permission){
                    return ['code' => 201, 'message' => '权限不存在'];
                }
                $permission->update($data);
            }else{
                Permission::create($data);
            }
            \DB::commit();
            return ['code' => 200, 'message' => '设置成功！'];
        } catch (\Exception $e) {
            \DB::rollBack();
            return ['code' => 201, 'message' => $e->getMessage()];
        }
    }

    /**
     * 删除
     * @param $id
     * @return array
     * @throws \Exception
     */
    public function destroy($id)
    {
        try {
            \DB::beginTransaction();
            $permission = Permission::find($id);
            if (!$permission){
                return ['code' => 201, 'message' => '权限不存在'];
            }
            //如果有子权限，则禁止删除
            if (Permission::where('parent_id',$id)->first()){
                return ['code' => 201, 'message' => '存在子权限禁止删除'];
            }
            $permission->delete();
            // 删除关联数据 - 删除关联数据
            \DB::commit();
            return ['code' => 200, 'message' => '删除成功！'];
        } catch (\Exception $e) {
            \DB::rollBack();
            return ['code' => 201, 'message' => $e->getMessage()];
        }
    }
}
