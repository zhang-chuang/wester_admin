<?php

namespace Iwester\Http\Controllers\Admin\Manage;

use Iwester\Http\Controllers\Admin\BaseController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Iwester\Http\Model\User\Permission;
use Iwester\Http\Model\User\Role;
use Iwester\Http\Requests\RoleStoreRequest;
use Symfony\Component\Translation\Tests\Writer\BackupDumper;
use View;
use Iwester\Http\Model\Config\Banner;


class AdminRoleController extends BaseController
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
    }

    /**
     * role 列表
     * @return Response
     */
    public function role(Request $request)
    {
        $roles = Role::all();
        return view('iwester::admin.manage.role.role', [
            'roles'=> $roles
        ]);
    }

    /**
     * 添加角色
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id=0)
    {
        $role = $id ? Role::find($id) : false;
        return view('iwester::admin.manage.role.edit', [
            'role'=> $role,
        ]);
    }

    /**
     * 保存
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(RoleStoreRequest $request)
    {
        try {
            \DB::beginTransaction();
            $id = $request->input('id', 0);
            $data = $request->get('role', []);
            if ($id){
                $role = Role::find($id);
                $role->update($data);
            }else{
                Role::create($data);
            }
            \DB::commit();
            return ['code' => 200, 'message' => '操作成功！'];
        } catch (\Exception $e) {
            \DB::rollBack();
            return ['code' => 201, 'message' => $e->getMessage()];
        }
    }

    /**
     * 删除
     * @param $id
     * @return array
     * @throws \Exception
     */
    public function destroy($id)
    {
        try {
            \DB::beginTransaction();
            $role = Role::find($id);
            $role->delete();
            // 删除关联数据
            $role->permissions()->sync([]);
            $role->users()->sync([]);
            \DB::commit();
            return ['code' => 200, 'message' => '删除成功！'];
        } catch (\Exception $e) {
            \DB::rollBack();
            return ['code' => 201, 'message' => $e->getMessage()];
        }
    }

    /**
     * 角色分配权限
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function permission(Request $request, $id)
    {
        $role = Role::find($id);
        $permissions = Permission::permission();
        return view('iwester::admin.manage.role.permission', [
            'role'=> $role,
            'permissions'=> $permissions
        ]);
    }

    /**
     * 保存角色- 权限关系
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function permissionStore(Request $request, $id)
    {
        try {
            \DB::beginTransaction();
            $role = Role::find($id);
            $data = $request->get('permissions', []);
            if (empty($data)){
                $role->permissions()->detach();
            }else{
                $role->syncPermissions($data);
            }
            \DB::commit();
            return redirect()->to(route('manage.role'))->with(['status'=>'已更新角色权限']);
        } catch (\Exception $e) {
            \DB::rollBack();
            return  redirect()->route('manage.role.permission', ['id'=> $id])->with('errors', ['系统错误，保存失败']);
        }
    }
}
