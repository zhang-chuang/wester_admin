<?php

namespace Iwester\Http\Controllers\Admin\Manage;

use Iwester\Http\Controllers\Admin\BaseController;
use App\Console\Commands\SiteMap;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Symfony\Component\Translation\Tests\Writer\BackupDumper;
use View;
use Iwester\Http\Model\Config\SiteConfig;
use Iwester\Http\Model\Config\SiteLink;


class AdminSiteController extends BaseController
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
    }

    /**
     * 站点信息
     * @return Response
     */
    public function siteconfig()
    {
        return view('iwester::admin.manage.siteconfig.siteconfig');
    }
    
    /**
     * 保存站点信息
     * @param Request $request
     */
    public function store(Request $request)
    {
        try {
            \DB::beginTransaction();
            $config = $request->get('site', false);
            $configEntity = SiteConfig::first();
            $configEntity->update($config);
            SiteConfig::delConfigCache();
            \DB::commit();
            return redirect(route('manage.siteconfig'))->with(['status' => '保存成功']);
        } catch (\Exception $e) {
            \DB::rollBack();
            return redirect(route('manage.siteconfig'))->with(['status' => '保存失败']);
        }
    }
}
