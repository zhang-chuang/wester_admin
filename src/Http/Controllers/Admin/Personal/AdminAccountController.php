<?php

namespace Iwester\Http\Controllers\Admin\Personal;

use Iwester\Http\Controllers\Admin\BaseController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Iwester\Http\Model\User\Permission;
use Iwester\Http\Model\User\Role;
use Iwester\Http\Model\User\User;
use Iwester\Http\Requests\SetPasswordRequest;
use Iwester\Http\Requests\UserUpdateRequest;
use Symfony\Component\Translation\Tests\Writer\BackupDumper;
use View;
use Iwester\Http\Model\Config\Banner;
use Iwester\Http\Requests\UserCreateRequest;
use Intervention\Image\ImageManager;
use Intervention\Image\ImageManagerStatic;


class AdminAccountController extends BaseController
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
    }

    /**
     * 账号信息
     * @return Response
     */
    public function information()
    {
        return view('iwester::admin.personal.information', []);
    }

    /**
     * 保存账号信息
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function informationStore(Request $request)
    {
        try {
            \DB::beginTransaction();
            $data = $request->get('user', []);
            $user = User::find(\Auth::id());
            $user->name = $data['name'];
            $user->save();
            \DB::commit();
            return ['code' => 200, 'message' => '账号更新成功！'];
        } catch (\Exception $e) {
            \DB::rollBack();
            return ['code' => 201, 'message' => '账号更新失败'];
        }
    }

    /**
     * 修改头像
     * @return Response
     */
    public function avatar()
    {
        return view('iwester::admin.personal.avatar', []);
    }

    /**
     * 保存头像信息
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function avatarStore(Request $request)
    {
        try {
            \DB::beginTransaction();
            $file = $request->input('file');
            $image = ImageManagerStatic::make(public_path($file));
            $w = $request->get('w', 128);
            $h = $request->get('h', 128);
            $x = $request->get('x', 0);
            $y = $request->get('y', 0);
            $image->crop($w, $h, $x, $y)->save(public_path($file), $file);
            $user = User::find(\Auth::id());
            $user->avatar = $file;
            $user->save();
            \DB::commit();
            return ['code' => 200, 'message' => '头像修改成功！'];
        } catch (\Exception $e) {
            \DB::rollBack();
            return ['code' => 201, 'message' => '头像修改失败'];
        }
    }

    /**
     * 修改密码
     * @return Response
     */
    public function password()
    {
        return view('iwester::admin.personal.password', []);
    }

    /**
     * 修改密码
     * @param ResetPasswordRequest $request
     * @return array
     */
    public function resetPassword(SetPasswordRequest $request)
    {
        try {
            \DB::beginTransaction();
            $data = $request->get('user', []);
            $user = User::find(\Auth::id());
            $user->password = bcrypt($data['password']);
            $user->save();
            \DB::commit();
            return ['code' => 200, 'message' => '密码设置成功'];
        } catch (\Exception $e) {
            \DB::rollBack();
            return ['code' => 201, 'message' => '密码设置失败！'];
        }
    }

    /**
     * 账号绑定
     * @return Response
     */
    public function bind()
    {
        return view('iwester::admin.personal.bind', []);
    }

}
