<?php

namespace Iwester\Http\Controllers\Admin\Seo;

use Iwester\Http\Controllers\Admin\BaseController;
use App\Console\Commands\SiteMap;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Iwester\Http\Model\Book\BookCategory;
use Symfony\Component\Translation\Tests\Writer\BackupDumper;
use View;
use Iwester\Http\Model\Article\ArticleCategory;


class AdminSeoBookController extends BaseController
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
    }

    /**
     * 书籍 - 栏目seo
     * @return Response
     */
    public function category()
    {
        $categories = BookCategory::allCategory();;
        return view('iwester::admin.seo.book.category', ['categories'=> $categories]);
    }

    /**
     * 编辑栏目seo
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function categoryEdit($id)
    {
        $categorySeo = BookCategory::find($id);
        return view('iwester::admin.seo.book.categoryEdit', ['categorySeo' => $categorySeo]);
    }

    /**
     * 保存栏目seo
     * @param Request $request
     */
    public function categoryStore(Request $request)
    {
        try {
            \DB::beginTransaction();
            $seo = $request->get('seo', false);
            $id = $request->get('id', false);
            $categorySeo = BookCategory::find($id);
            $categorySeo->update($seo);
            \DB::commit();
            return ['code' => 200, 'message' => '设置成功！'];
        } catch (\Exception $e) {
            \DB::rollBack();
            return ['code' => 201, 'message' => '保存失败！'];
        }
    }

    /**
     * 书籍 - 栏目文章seo规则
     * @return Response
     */
    public function rule()
    {
        $categories = BookCategory::allCategory();
        return view('iwester::admin.seo.book.rule', ['categories'=> $categories]);
    }

    /**
     * 编辑 - 书籍栏目文章seo规则
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function ruleEdit($id)
    {
        $categorySeo = BookCategory::find($id);
        return view('iwester::admin.seo.book.ruleEdit', ['categorySeo' => $categorySeo]);
    }

    /**
     * 保存 - 栏目文章seo规则
     * @param Request $request
     */
    public function ruleStore(Request $request)
    {
        try {
            \DB::beginTransaction();
            $seo = $request->get('seo', false);
            $id = $request->get('id', false);
            $categorySeo = BookCategory::find($id);
            $categorySeo->update($seo);
            \DB::commit();
            return ['code' => 200, 'message' => '设置成功！'];
        } catch (\Exception $e) {
            \DB::rollBack();
            return ['code' => 201, 'message' => '保存失败！'];
        }
    }
}
