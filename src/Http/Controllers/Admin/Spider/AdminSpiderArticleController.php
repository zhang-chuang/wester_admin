<?php

namespace Iwester\Http\Controllers\Admin\Spider;

use Iwester\Http\Controllers\Admin\BaseController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Iwester\Http\Model\Article\ArticleCategory;
use Iwester\Http\Model\Spider\SpiderArticleContent;
use Iwester\Http\Model\Article\Article;
use Iwester\Http\Model\Article\ArticleComment;
use Iwester\Http\Model\Article\ArticleCommentLike;
use Iwester\Http\Model\Article\ArticleContent;
use Iwester\Http\Model\Spider\SpiderTask;
use Iwester\Http\Model\Spider\SpiderTaskConfig;
use Iwester\Http\Model\Article\ArticleTag;
use Iwester\Http\Model\Config\SiteConfig;
use Iwester\Services\FunctionService;
use Iwester\Services\SpiderService;
use phpDocumentor\Reflection\Types\Collection;
use Symfony\Component\Translation\Tests\Writer\BackupDumper;
use View;
use Iwester\Http\Model\Config\Banner;


class AdminSpiderArticleController extends BaseController
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
    }

    /**
     * 采集文章列表
     * @return Response
     */
    public function list(Request $request)
    {
        $articles = SpiderArticleContent::where('status', 0)->paginate(20);
        return view('iwester::admin.spider.article', [
            'articles'=> $articles,
            'params'=> $request->all()
        ]);
    }

    /**
     * 编辑采集文章
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $tags = ArticleTag::getTags();
        $categories = ArticleCategory::allCategory();
        $article = SpiderArticleContent::find($id);
        return view('iwester::admin.spider.edit', [
            'categories' => $categories,
            'article' => $article,
            'tags' => $tags
        ]);
    }

    /**
     * 保存文章
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            \DB::beginTransaction();
            $id = $request->input('id', false);
            $articleParam = $request->get('article', []);
            $contentParam = $request->get('content', []);
            $tags = $request->get('tags', false);
            if (!isset($articleParam['publish_status'])) $articleParam['publish_status'] = 0;
            $articleParam['published_at'] = date('Y-m-d');
            $article = Article::create($articleParam);
            $contentParam['article_id'] = $article->id;
            if (!$this->siteConfig->on_article_audit) {
                $contentParam['audit_status'] = 1;
            }
            ArticleContent::create($contentParam);
            if ($tags) $article->tags()->sync(json_decode($tags, true));

            $sArticle = SpiderArticleContent::find($id);
            $sArticle->status = 1;
            $sArticle->save();
            \DB::commit();
            return redirect(route('spider.task.article.caiji.list'));
        } catch (\Exception $e) {
            \DB::rollBack();
            return redirect()->route('spider.task.article.caiji.list')->with('errors', ['系统错误，保存失败']);
        }
    }

    /**
     * 废弃文章
     * @param $id
     * @return array
     * @throws \Exception
     */
    public function destroy($id)
    {
        try {
            \DB::beginTransaction();
            $article = SpiderArticleContent::find($id);
            $article->status = -1;
            $article->save();
            \DB::commit();
            return ['code' => 200, 'message' => '废弃成功！', 'url'=> route('spider.task.article.caiji.list')];
        } catch (\Exception $e) {
            \DB::rollBack();
            return ['code' => 201, 'message' => $e->getMessage()];
        }
    }
}
