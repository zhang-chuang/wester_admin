<?php

namespace Iwester\Http\Controllers\Admin\Zixun;

use Illuminate\Support\Facades\Auth;
use Iwester\Http\Controllers\Admin\BaseController;
use App\Console\Commands\SiteMap;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Iwester\Http\Model\Article\Article;
use Iwester\Http\Model\Article\ArticleComment;
use Iwester\Http\Model\Article\ArticleCommentLike;
use Iwester\Http\Model\Article\ArticleContent;
use Iwester\Http\Model\Article\ArticleLike;
use Iwester\Http\Model\Article\ArticleTag;
use Iwester\Http\Model\Config\SiteConfig;
use Monolog\Handler\IFTTTHandler;
use Symfony\Component\Translation\Tests\Writer\BackupDumper;
use View;
use Iwester\Http\Model\Article\ArticleCategory;


class AdminArticleCommentController extends BaseController
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
    }

    /**
     * 资讯 - 评论列表
     * @return Response
     */
    public function comment(Request $request)
    {
        $query = ArticleComment::with('article');
        $auditStatus = $request->get('audit_status', null);
        if ($auditStatus !== null ) $query->where('audit_status', $auditStatus);
        $comments = $query->orderBy('audit_status', 'desc')->paginate(20);
        return view('iwester::admin.zixun.comment.comment', [
            'comments' => $comments,
            'auditStatus' => $auditStatus,
            'params' => $request->all()
        ]);
    }

    /**
     * 删除评论
     * @return array
     * @throws \Exception
     */
    public function destroy(Request $request)
    {
        try {
            \DB::beginTransaction();
            $ids = $request->get('ids', false);
            $ids = json_decode($ids, true);
            ArticleComment::whereIn('id', $ids)->delete();
            \DB::commit();
            return ['code' => 200, 'message' => '删除成功！'];
        } catch (\Exception $e) {
            \DB::rollBack();
            return ['code' => 201, 'message' => $e->getMessage()];
        }
    }

    /**
     * 修改文章审核状态
     * @param $id
     * @return array
     * @throws \Exception
     */
    public function changeAudit(Request $request)
    {
        try {
            \DB::beginTransaction();
            $type = $request->get('type', false);
            $ids = $request->get('ids', false);
            $ids = json_decode($ids, true);
            ArticleComment::whereIn('id', $ids)->update(['audit_status'=> $type]);
            \DB::commit();
            return ['code' => 200, 'message' => '操作成功！'];
        } catch (\Exception $e) {
            \DB::rollBack();
            return ['code' => 201, 'message' => $e->getMessage()];
        }
    }
}
