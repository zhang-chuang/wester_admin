<?php

namespace Iwester\Http\Controllers\Admin\Zixun;

use Iwester\Http\Controllers\Admin\BaseController;
use App\Console\Commands\SiteMap;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Iwester\Http\Model\Article\Article;
use Iwester\Http\Requests\ArticleCategoryRequest;
use Symfony\Component\Translation\Tests\Writer\BackupDumper;
use View;
use Iwester\Http\Model\Article\ArticleCategory;


class AdminCategoryController extends BaseController
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
    }

    /**
     * 资讯 - 分类管理
     * @return Response
     */
    public function category()
    {
        $categories = ArticleCategory::allCategory();;
        return view('iwester::admin.zixun.category.category', ['categories'=> $categories]);
    }

    /**
     * 添加栏目
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create($id)
    {
        $category = ArticleCategory::find($id);
        return view('iwester::admin.zixun.category.create', ['category' => $category]);
    }

    /**
     * 编辑栏目
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($pId, $id)
    {
        $parentCategory = ArticleCategory::find($pId);
        $category = ArticleCategory::find($id);
        return view('iwester::admin.zixun.category.edit', ['parentCategory'=> $parentCategory,'category' => $category]);
    }

    /**
     * 保存栏目
     * @param Request $request
     */
    public function store(ArticleCategoryRequest $request)
    {
        try {
            \DB::beginTransaction();
            $cate = $request->get('cate', false);
            $category = new ArticleCategory();
            $category = $category->create($cate);
            if ($cate['parent_id'] != 0 ){
                $parentCategory = ArticleCategory::find($cate['parent_id']);
                ArticleCategory::changeParentSubCategory($parentCategory, $category);
            }
            $category->sub_category = json_encode([$category->id]);
            $category->top_category = $cate['parent_id'] ? ($parentCategory->top_category ==0 ? $parentCategory->id : $parentCategory->top_category) : 0;
            $category->save();
            \DB::commit();
            return ['code' => 200, 'message' => '设置成功！'];
        } catch (\Exception $e) {
            \DB::rollBack();
            return ['code' => 201, 'message' => '保存失败！'];
        }
    }

    /**
     * 编辑保存栏目
     * @param Request $request
     */
    public function update(ArticleCategoryRequest $request)
    {
        try {
            \DB::beginTransaction();
            $categoryId = $request->get('id', false);
            $cate = $request->get('cate', false);
            $category = ArticleCategory::find($categoryId);
            $category->update($cate);
            \DB::commit();
            return ['code' => 200, 'message' => '设置成功！'];
        } catch (\Exception $e) {
            \DB::rollBack();
            return ['code' => 201, 'message' => '保存失败！'];
        }
    }

    /**
     * 删除分类 - 文章划到父级 - 一级分类不可删除
     * @param $id
     * @return array
     * @throws \Exception
     */
    public function destroy($id)
    {
        try {
            \DB::beginTransaction();
            $articleCategory = ArticleCategory::find($id);
            # 获取分类文章数据
            $articleCount = Article::where('article_category_id', $id)->count();
            # 获取父级
            if ($articleCategory->parent_id == 0 && $articleCount > 0) {
                return ['code' => 201, 'message' => '危险操作，一级分类下有文章不可删除，只可更新名称！'];
            };
            # 迁移文章
            if ($articleCount > 0){
                Article::where('article_category_id', $id)->update(['article_category_id'=> $articleCategory->parent_id]);
            }
            # 删除子分类 - 多级子分类

            # 删除分类
            $articleCategory->delete();
            \DB::commit();
            return ['code' => 200, 'message' => '删除成功！'];
        } catch (\Exception $e) {
            \DB::rollBack();
            return ['code' => 201, 'message' => $e->getMessage()];
        }
    }
}
