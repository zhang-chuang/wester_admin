<?php

namespace Iwester\Http\Model\Article;

use Iwester\Http\Model\Base;
use Illuminate\Support\Facades\Cache;
use Iwester\Http\Model\User\User;
use Carbon\Carbon;

class ArticleComment extends Base
{
    protected $table = 'article_comments';
    const AUDIT_FAIL = -1;  // -1：审核不通过
    const AUDIT_WAIT = 1;  // 未审核
    const AUDIT_PASS = 0;  // 0：审核通过
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'article_id', 'comment_user', 'comment_to_user', 'comment_id', 'comment_content', 'audit_status',
    ];

    public function getCreatedAtAttribute($date)
    {
        // 默认100天前输出完整时间，否则输出人性化的时间
        if (Carbon::now() > Carbon::parse($date)->addDays(180)) {
            return Carbon::parse($date);
        }
        return Carbon::parse($date)->diffForHumans();
    }

    /**
     * 获得此评论所属的文章。
     */
    public function article()
    {
        return $this->belongsTo(Article::class);
    }

    /**
     * 获取当前评论下的子评论
     * @return mixed
     */
    public function subComment()
    {
        return $this->hasMany(ArticleComment::class, 'comment_id', 'id')->orderBy('created_at', 'desc');
    }

    /**
     * 获得此评论 - 点赞数据。
     */
    public function commentZan()
    {
        return $this->hasMany(ArticleCommentLike::class, 'article_comment_id', 'id');
    }

    /**
     * 获得此评论 - 回复者。
     */
    public function commentUser()
    {
        return $this->hasOne(User::class, 'id', 'comment_user');
    }

    /**
     * 获得此评论 - 回复谁。
     */
    public function commentToUser()
    {
        return $this->hasOne(User::class, 'id', 'comment_to_user');
    }

    /**
     * 审核状态
     * @return string
     */
    public function getAuditStatus()
    {
        switch ($this->audit_status) {
            case self::AUDIT_PASS:
                return '审核通过';
            case self::AUDIT_WAIT:
                return '待审核';
            default:
                return '不通过';
        }
    }

}