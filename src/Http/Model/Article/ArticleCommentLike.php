<?php

namespace Iwester\Http\Model\Article;

use Iwester\Http\Model\Base;
use Illuminate\Support\Facades\Cache;

class ArticleCommentLike extends Base
{
    protected $table = 'article_comment_likes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'article_id', 'article_comment_id', 'like_user',
    ];

    public static function store($article_id, $article_comment_id, $like_user){
        try{
            $entity = new ArticleCommentLike();
            $entity->article_id = $article_id;
            $entity->article_comment_id = $article_comment_id;
            $entity->like_user = $like_user;
            $entity->created_at = date('Y-m-d H:i:s');
            $entity->updated_at = date('Y-m-d H:i:s');
            $entity->save();
            return $entity;
        }catch (\Exception $e){
            throw new \Exception($e->getMessage());
        }

    }
}