<?php
namespace Iwester\Http\Model\Article;

use Iwester\Http\Model\Base;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * ArticleSeo Entity
 *
 * @property int $id
 * @property int $article_id
 * @property string $content
 * @property string $title
 * @property string $keyword
 * @property string $description

 */
class ArticleContent extends Base
{

    protected $table = 'article_contents';
    protected $fillable = [
        'title', 'keyword', 'description', 'article_id', 'content'
    ];

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    public function article(){
        return $this->belongsTo(Article::class, 'id', 'article_id');
    }
}
