<?php

namespace Iwester\Http\Model\Article;

use Iwester\Http\Model\Base;
use Illuminate\Support\Facades\Cache;

class ArticleLike extends Base
{
    protected $table = 'article_likes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'article_id', 'like_user',
    ];

    public static function store($article_id, $like_user){
        try{
            $entity = new ArticleLike();
            $entity->article_id = $article_id;
            $entity->like_user = $like_user;
            $entity->created_at = date('Y-m-d H:i:s');
            $entity->updated_at = date('Y-m-d H:i:s');
            $entity->save();
            return $entity;
        }catch (\Exception $e){
            throw new \Exception($e->getMessage());
        }

    }
}