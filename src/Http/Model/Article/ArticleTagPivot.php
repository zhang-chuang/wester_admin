<?php
namespace Iwester\Http\Model\Article;

use Iwester\Http\Model\Base;

/**
 * ArticleTagPivot Entity
 *
 * @property int $id
 * @property int $article_id
 * @property int $article_tag_id
 *
 */
class ArticleTagPivot extends Base
{
    protected $table = 'article_tag_pivot';

    /**
     * 获取当前标签文章
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function article()
    {
        return $this->hasMany(Article::class, 'id', 'article_id');
    }

    /**
     * 获取当前标签
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function tag()
    {
        return $this->hasOne(ArticleTag::class, 'id', 'article_tag_id');
    }

    /**
     * getTags
     * @return mixed
     */
    public static function getTags($showAll = false)
    {
        $tags = ArticleTagPivot::with(['tag'=> function($query) use ($showAll){
            if (!$showAll) {
                $query->where('status', 1);
            }
        }, 'article'=> function($query){
            $query->where('publish_status', 1);
            $query->where('audit_status', 1);
        }])->select(['*']);
        return $tags->get();
    }
}