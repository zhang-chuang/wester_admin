<?php

namespace Iwester\Http\Model;

use Eloquent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @property integer $id
 * @property string $created_at
 * @property string $updated_at
 */
class Base extends Eloquent
{
    /**
     * 复制文件
     * @param $source
     * @param $destination
     * @param bool $delSource
     * @return bool
     */
    public static function copyFile($source, $destination, $delSource = true){
        try{
            $copyStatus = copy($source,$destination);
            if ($copyStatus){
                if ($delSource) unlink($source); //删除旧目录下的文件
                return true;
            }else{
                return false;
            }
        }catch (\Exception $e){
            return false;
        }
    }

    public static function moveFile($file,$newloc){
        try{
            return move_uploaded_file($file,$newloc);
        }catch (\Exception $e){
            return false;
        }
    }

    /**
     * 创建文件夹
     * @param $dir *需要创建的文件夹目录
     * @return bool
     * @Author  z-chuang
     */
    public static function Directory( $dir ){
        return  is_dir ( $dir ) or self::Directory(dirname( $dir )) and  mkdir ( $dir ) and chmod( $dir, 0777 );
    }

    public static function deldir($dir) {
        try{
            //先删除目录下的文件：
            $dh=opendir($dir);
            while ($file=readdir($dh)) {
                if($file!="." && $file!="..") {
                    $fullpath=$dir."/".$file;
                    if(!is_dir($fullpath)) {
                        unlink($fullpath);
                    } else {
                        self::deldir($fullpath);
                    }
                }
            }
            closedir($dh);
            //删除当前文件夹：
            if(rmdir($dir)) {
                return true;
            } else {
                return false;
            }
        }catch (\Exception $e){
            return false;
        }
    }

}


