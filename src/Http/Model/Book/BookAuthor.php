<?php

namespace Iwester\Http\Model\Book;

use Iwester\Http\Model\Base;
use Illuminate\Database\Eloquent\SoftDeletes;
use Iwester\Http\Model\Config\SiteConfig;
use Iwester\Http\Model\User\User;

class BookAuthor extends Base
{
    protected $table = 'book_authors';

    const CAN_USE = 1; //使用
    const NOT_USE = 0; //不使用

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'author_name', 'author_level', 'status', 'write_days', 'description',
    ];

    public static $level = [
        '作家'
    ];

    public function books()
    {
        return $this->hasMany(Book::class, 'book_author_id', 'id');
    }
}
