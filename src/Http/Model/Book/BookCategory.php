<?php

namespace Iwester\Http\Model\Book;

use Iwester\Http\Model\Base;
use Illuminate\Database\Eloquent\SoftDeletes;
use Iwester\Http\Model\Config\SiteConfig;
use Iwester\Http\Model\User\User;

class BookCategory extends Base
{
    protected $table = 'book_categorys';
    const CAN_USE = 1; //使用
    const NOT_USE = 0; //不使用

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    protected $fillable = ['cate_name' ,'alias', 'parent_id', 'status', 'sort', 'title','prefix',
        'keyword', 'description', 'title_rule', 'keyword_rule', 'description_rule'];

    // 获取当前分类下的子分类
    public static function allCategory($showAll = false){
        $category = BookCategory::with(['subCate' => function ($query) {
            $query->oldest('sort');
        }])->where('parent_id', 0);
        if (!$showAll) $category->where('status', self::CAN_USE);
        return $category->oldest('sort')->get();
    }

    // 获取当前分类下的子分类
    public function subCate(){
        return $this->hasMany( BookCategory::class, 'parent_id', 'id')->where('status', self::CAN_USE);
    }

    // 获取当前分类下的父分类
    public function parentCate(){
        return $this->belongsTo( BookCategory::class, 'parent_id', 'id')->where('status', self::CAN_USE);
    }

    /**
     * 依次循环更新父级sub_category
     * @param $categroyId
     * @return BookCategory|BookCategory[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public static function changeParentSubCategory($parentCategory, $addCategory){
        try{
            if ($parentCategory){
                $newSubCategory = json_decode($parentCategory->sub_category);
                $newSubCategory[] = $addCategory->id;
                $parentCategory->sub_category = json_encode(array_unique($newSubCategory));
                $parentCategory->save();
                if ($parentCategory->parent_id !=0 ){
                    $curParentCategory = BookCategory::find($parentCategory->parent_id);
                    return BookCategory::changeParentSubCategory($curParentCategory, $addCategory);
                }
            }
        }catch (\Exception $e){
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * 栏目下文章可选规则
     * @var array
     */
    public static $rules = [
        'title'       => [ '{文章标题}', '{文章分类}', '{网站名称}' ],
        'keyword'     => [ '{文章标题}', '{文章分类}', '{分类SEO关键词}', '{网站名称}' ],
        'description' => [ '{文章标题}', '{内容简介}', '{网站名称}' ]
    ];

    /**
     * 文章默认规则
     * @var array
     */
    public static $articleRules = [
        'title'       => '{文章标题}_{网站名称}',
        'keyword'     => '{分类SEO关键词},{网站名称}',
        'description' => '{内容简介}_{网站名称}'
    ];
}
