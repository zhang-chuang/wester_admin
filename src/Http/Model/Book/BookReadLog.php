<?php

namespace Iwester\Http\Model\Book;

use Iwester\Http\Model\Base;
use Illuminate\Database\Eloquent\SoftDeletes;
use Iwester\Http\Model\Config\SiteConfig;
use Iwester\Http\Model\User\User;

class BookReadLog extends Base
{
    protected $table = 'book_read_logs';
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    public function book(){
        return $this->hasOne(Book::class, 'id', 'book_id');
    }

    public function chapter(){
        return $this->hasOne(BookChapter::class, 'id', 'chapter_id');
    }
}
