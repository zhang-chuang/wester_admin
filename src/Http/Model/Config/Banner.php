<?php

namespace Iwester\Http\Model\Config;

use Iwester\Http\Model\Base;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

/**
 * Banner Entity
 *
 * @property string $category_name
 * @property integer $batch
 * @property string $cover
 * @property string $url
 * @property string $alt
 * @property string $sort
 */
class Banner extends Base
{
    protected $table = 'banners';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_name', 'batch', 'cover', 'url', 'alt', 'sort'
    ];

    /**
     * 获取指定batch banner
     * @param $batch
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function banners($batch)
    {
        return Banner::where('batch', $batch)->orderBy('sort')->get();
    }
}
