<?php

namespace Iwester\Http\Model\Config;

use Iwester\Http\Model\Base;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

/**
 * SiteConfig Entity
 *
 * @property string $site_name
 * @property string $site_logo
 * @property string $site_ico
 * @property string $site_copyright
 * @property string $site_title
 * @property string $site_keyword
 * @property string $site_description
 * @property string $site_script
 * @property string $email
 * @property string $qq
 * @property string $tel
 * @property string $site_url
 */
class SiteConfig extends Base
{
    protected $table = 'site_config';

    public $timestamps = false;

    public static $prefixs = ['article'];

    public static $bookPrefixs = ['book'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'site_name', 'site_logo', 'site_ico', 'site_copyright', 'site_title', 'site_keyword', 'site_description', 'site_script','sitemap_api','sensitive_word',
        'email', 'qq', 'tel', 'site_url', 'is_login', 'on_article_audit', 'on_article_comment', 'on_article_like', 'on_article_comment_like', 'on_article_comment_audit',
        'on_book_model'
    ];

    /**
     * 从缓存中config
     * @return mixed
     */
    public static function getConfig()
    {
        return Cache::remember('siteConfig', 0, function () {
            return SiteConfig::first();
        });
    }

    /**
     * 删除缓存
     */
    public static function delConfigCache()
    {
        if (Cache::has('siteConfig')) {
            Cache::forget('siteConfig');
        }
    }

    /**
     * 敏感词匹配
     * @param $content
     * @return bool|string
     */
    public static function sensitiveWord($sensitiveWord, $content){
        if ($sensitiveWord != ''){
            $pattern = "/".$sensitiveWord."/i";
            if(preg_match_all($pattern, $content, $matches)){
                $patternList = $matches[0];  //匹配到的数组
                $sensitiveWord = implode(',', $patternList); //敏感词数组转字符串
                return $sensitiveWord;
            }
        }
        return false;
    }
}
