<?php

namespace Iwester\Http\Model\Config;

use Iwester\Http\Model\Base;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Link Entity
 *
 * @property int $id
 * @property string $name
 * @property string $url
 * @property string $rel
 * @property string $sort
 */
class SiteLink extends Base
{
    protected $table = 'site_links';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'url', 'rel', 'sort', 'status', 'only_index'
    ];

    /**
     * 获取友链
     * @param array $whereArr
     * @param int $pageSize
     * @return Link[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getLinks($whereArr = [], $inArr = [])
    {
        $Query = SiteLink::where('status', 1);
        if ($whereArr) $Query->where($whereArr);
        if ($inArr) $Query->whereIn($inArr[0], $inArr[1]);
        $Query->orderBy('sort', 'desc');
        return $Query->get();
    }

}
