<?php

namespace Iwester\Http\Model\Config;

use Iwester\Http\Model\Article\ArticleCategory;
use Iwester\Http\Model\Base;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class SiteWebColumn extends Base
{
    protected $table = 'site_web_column';

    public $timestamps = false;

    public static $types = [
        1 => 'URL',
        2 => '单页面指定模板',
        3 => '使用文章分类'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','url', 'blade_template', 'type', 'target', 'sort', 'parent_id', 'params'
    ];

    // 获取当前子栏目
    public function subColumn(){
        return $this->hasMany( SiteWebColumn::class, 'parent_id', 'id');
    }

    public function articleCate(){
        $cate = ArticleCategory::where(json_decode($this->params, true))->first();
        return $cate;
    }
}
