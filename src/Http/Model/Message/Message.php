<?php

namespace Iwester\Http\Model\Message;

use Iwester\Http\Model\Base;
use Illuminate\Database\Eloquent\Model;

class Message extends Base
{
    protected $table = 'messages';
    protected $fillable = ['title','content','read','send_uuid','accept_uuid','flag'];

    const NOT_READ = 1; # 未读
    const IS_READ = 2; # 已读

    public $read_status=[
        '1'=>'未读',
        '2'=>'已读'
    ];

}
