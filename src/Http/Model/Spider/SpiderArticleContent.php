<?php

namespace Iwester\Http\Model\Spider;

use Iwester\Http\Model\Base;
use Illuminate\Support\Facades\Cache;

class SpiderArticleContent extends Base
{
    protected $table = 'spider_article_contents';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'task_id', 'task_config_id', 'list_url', 'detail_url', 'content', 'title', 'published_at', 'article_category'
        , 'cover', 'status', 'other', 'table_template'
    ];
}