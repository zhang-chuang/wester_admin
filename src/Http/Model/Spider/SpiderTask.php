<?php

namespace Iwester\Http\Model\Spider;

use Iwester\Http\Model\Base;
use Illuminate\Support\Facades\Cache;

class SpiderTask extends Base
{
    protected $table = 'spider_tasks';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'freq', 'start_time', 'next_spider_time', 'cur_spider_time', 'cookie',
        'priority', 'charset',
    ];

    public static $freqs = [
        1=> '一小时',
        2=> '半天',
        3=> '一天',
        4=> '一周',
        -1=> '一次性',
    ];

    public static $freqHours = [
        1=> 1,
        2=> 12,
        3=> 24,
        4=> 24 * 7,
        -1=> 1,
    ];

    public static $status = [
        1=> '正常',
        0=> '废弃'
    ];


    public static $charsets = [
        1=> '自动识别',
        2=> 'UTF-8',
        3=> 'GBK',
        4=> 'GB2312',
        5=> 'ASCII',
    ];

    public static $priority = [
        1=> '一般',
        2=> '缓急',
        3=> '紧急',
    ];

    public static $contentTypes = [
        'text','src','href','html','title','alt','data-img','data-src'
    ];

    public static $urlPrefix = [
        1=> 'url前缀-domain',
        2=> 'url前缀-任务url去除page参数'
    ];

    public static $table_template = [
       'article',
    ];

    const PAGE = '[PAGE]';

    public function listConfig(){
        return $this->hasOne(SpiderTaskConfig::class, 'task_id', 'id');
    }

    public function listArticle(){
        return $this->hasOne(SpiderArticleContent::class, 'task_id', 'id');
    }
}