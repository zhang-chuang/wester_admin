<?php

namespace Iwester\Http\Model\Spider;

use Iwester\Http\Model\Base;
use Illuminate\Support\Facades\Cache;

class SpiderTaskConfig extends Base
{
    protected $table = 'spider_task_configs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'task_id', 'table_template', 'list_url', 'list_page_matche', 'list_page_attr', 'list_url_content',
        'detail_use_page', 'detail_page_matche','detail_page_attr','detail_url_content', 'detail_page_url'
    ];
}