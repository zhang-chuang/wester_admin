<?php
namespace Iwester\Http\Model\User;

use Iwester\Http\Model\Icon;

class Permission extends \Spatie\Permission\Models\Permission
{
    // 获取权限与子权限
    public static function permission($pId = 0){
        $permission = Permission::with(['subPermission', 'icon'])->where('parent_id', $pId);
        return $permission->oldest('sort')->get();
    }

    // 获取当前权限下的子权限
    public function subPermission(){
        return $this->hasMany( Permission::class, 'parent_id', 'id');
    }

    // 获取icon
    public function icon(){
        return $this->hasOne(Icon::class, 'id', 'icon_id');
    }
}