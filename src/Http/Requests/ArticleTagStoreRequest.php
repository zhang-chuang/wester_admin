<?php

namespace Iwester\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ArticleTagStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $reture = [
            'tag.tag' => 'required|min:2|max:14',
        ];
        return $reture;
    }

    /**
     * 获取已定义的验证规则的错误消息。
     *
     * @return array
     */
    public function messages()
    {
        return [
            'required' => ':attribute为必填项',
            'numeric' => ':attribute必须为数字',
            'min' => ':attribute长度至少为:min位',
            'max' => ':attribute长度不能超过:max位',
            'regex' => ':attribute格式不正确',
            'confirmed' => ':attribute输入不一致',
            'unique' => ':attribute已存在',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'tag.tag' => '标签名称',
        ];
    }

    // woo 改变验证后的默认行为： 变成 ajax
    public function failedValidation( \Illuminate\Contracts\Validation\Validator $validator ) {
        exit(json_encode(array(
            'code' => 422,
            'success' => false,
            'message' => 'There are incorect values in the form!',
            'errors' => $validator->getMessageBag()->toArray()
        )));
    }
}
