<?php

namespace Iwester\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PermissionStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $reture = [
            'permission.display_name' => 'required|min:2|max:34',
            'permission.name' => 'required|min:2|max:34|unique:permissions,name,'.$this->get('id').'',
        ];
        return $reture;
    }

    /**
     * 获取已定义的验证规则的错误消息。
     *
     * @return array
     */
    public function messages()
    {
        return [
            'required' => ':attribute为必填项',
            'numeric' => ':attribute必须为数字',
            'min' => ':attribute长度至少为:min位',
            'max' => ':attribute长度不能超过:max位',
            'regex' => ':attribute格式不正确',
            'confirmed' => ':attribute输入不一致',
            'unique' => ':attribute已存在',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'permission.display_name' => '显示名称',
            'permission.name' => '权限名称',
        ];
    }

    // woo 改变验证后的默认行为： 变成 ajax
    public function failedValidation( \Illuminate\Contracts\Validation\Validator $validator ) {
        exit(json_encode(array(
            'code' => 422,
            'success' => false,
            'message' => 'There are incorect values in the form!',
            'errors' => $validator->getMessageBag()->toArray()
        )));
    }
}
