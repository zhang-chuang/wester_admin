<?php

namespace Iwester\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $reture = [
            'user.email' => 'required|unique:users,email|email',
//            'user.phone' => 'required|numeric|regex:/^1[3456789][0-9]{9}$/|unique:users,email',
            'user.username' => 'required|min:4|max:14|unique:users,username',
            'user.password' => 'required|min:6|max:14',
//            'user.password_confirmation' => 'required|min:6|max:14'
        ];
        return $reture;
    }

    /**
     * 获取已定义的验证规则的错误消息。
     *
     * @return array
     */
    public function messages()
    {
        return [
            'required' => ':attribute为必填项',
            'numeric' => ':attribute必须为数字',
            'min' => ':attribute长度至少为:min位',
            'max' => ':attribute长度不能超过:max位',
            'regex' => ':attribute格式不正确',
            'confirmed' => ':attribute输入不一致',
            'unique' => ':attribute已存在',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'user.username' => '用户名',
            'user.phone' => '手机号',
            'user.email' => '邮箱',
            'user.code' => '验证码',
            'user.password' => '密码',
            'user.password_confirmation' => '确认密码'
        ];
    }

    // woo 改变验证后的默认行为： 变成 ajax
    public function failedValidation( \Illuminate\Contracts\Validation\Validator $validator ) {
        exit(json_encode(array(
            'code' => 422,
            'success' => false,
            'message' => 'There are incorect values in the form!',
            'errors' => $validator->getMessageBag()->toArray()
        )));
    }
}
