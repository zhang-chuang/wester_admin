<?php namespace Iwester;

class IwesterFacade extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'IwesterFacade';
    }
}
