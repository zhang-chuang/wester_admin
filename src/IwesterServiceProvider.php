<?php namespace Iwester;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Iwester\Http\Model\Config\SiteConfig;

class IwesterServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'Iwester';

    protected $errors= [];
    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
        $this->loadRoutesFrom(__DIR__.'/routes/admin.php'); # 注入路由
        $this->loadMigrationsFrom(__DIR__.'/database/migrations'); # 注入数据迁移
        $this->loadViewsFrom(__DIR__.'/resources/views', 'iwester'); # 注入视图
        $this->publishes([
            __DIR__.'/public' => public_path('vendor/iwester'),
        ], 'public');   # 注入静态资源

        \View::share([
            'errors'=> $this->errors,
        ]);


        \Schema::defaultStringLength(191);
        // 左侧菜单
        view()->composer('iwester::layouts.admin.layout',function($view){
            $menus = \Iwester\Http\Model\User\Permission::with([
                'subPermission'=>function($query){
                    $query->with('icon');
                    $query->orderBy('sort','asc');
                }
                ,'icon'])->where('parent_id',0)->orderBy('sort','asc')->get();
            $unreadMessage = \Iwester\Http\Model\Message\Message::where('read', \Iwester\Http\Model\Message\Message::NOT_READ)
                ->where('accept_uuid',auth()->user()->uuid)->count();

            $siteConfig = SiteConfig::getConfig();
            $view->with('menus',$menus);
            $view->with('siteConfig',$siteConfig);
            $view->with('unreadMessage',$unreadMessage);
        });
    }

    /**
     * 在容器中注册绑定。
     *
     * @return void
     */
    public function register()
    {

    }

}
