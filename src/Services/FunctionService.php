<?php

namespace Iwester\Services;

use App;
use Iwester\Http\Model\Spider\SpiderTask;
use Log;
use QL\QueryList;
use Mews\Purifier\Facades\Purifier;
use GuzzleHttp\Client;
use Storage;
use Intervention\Image\ImageManager;

class FunctionService
{
    public function __construct()
    {
    }

    /**
     * 输出安全的html
     * @param      $text
     * @param null $tags
     * @param bool $istext
     * @return mixed|null|string|string[]
     */
    public static function h($text, $tags = null, $istext = false)
    {
        //去除style样式  "/src=.+?['|\"]/i",
        $text = preg_replace([ "/data-url=.+?['|\"]/i", "/border=.+?['|\"]/i", "/class=.+?['|\"]/i", "/style=.+?['|\"]/i", "/name=.+?['|\"]/i", "/id=.+?['|\"]/i", "/width=.+?['|\"]/i", "/height=.+?['|\"]/i", "/usemap=.+?['|\"]/i", "/shape=.+?['|\"]/i", "/coords=.+?['|\"]/i", "/target=.+?['|\"]/i", "/title=.+?['|\"]/i" ], "", $text);
        //完全过滤注释
        #$text = preg_replace("/<!--[^\!\[]*?(?<!\/\/)-->/", "", $text);
        $text = preg_replace('/<!--?.*-->/', '', $text);
        //完全过滤动态代码
        $text = preg_replace('/<\?|\?' . '>/', '', $text);
        //完全过滤js
        $text = preg_replace('/<script?.*\/script>/', '', $text);
        $text = str_replace('[', '&#091;', $text);
        $text = str_replace(']', '&#093;', $text);
        $text = str_replace('|', '&#124;', $text);
        //过滤换行符
        $text = preg_replace('/\r?\n/', '', $text);
        //br
//	$text = preg_replace('/<br(\s\/)?' . '>/i', '[br]', $text);
//	$text = preg_replace('/<p(\s\/)?' . '>/i', '[br]', $text);
//	$text = preg_replace('/(\[br\]\s*){10,}/i', '[br]', $text);

        //过滤危险的属性，如：过滤on事件lang js
//		while (preg_match('/(<[^><]+)( lang|on|action|background|codebase|dynsrc|lowsrc)[^><]+/i', $text, $mat)) {
//			$text = str_replace($mat[0], $mat[1], $text);
//		}
        while (preg_match('/(<[^><]+)(window\.|javascript:|js:|about:|file:|document\.|vbs:|cookie)([^><]*)/i', $text, $mat)) {
            $text = str_replace($mat[0], $mat[1] . $mat[3], $text);
        }

        if (empty($tags)) {
            $tags = 'table|td|th|tr|i|b|u|strong|img|p|br|div|strong|em|ul|ol|li|dl|dd|dt';
        }

        #$text = preg_replace("/<(\/?a.*?)>/si", "", $text);
        //允许的HTML标签
        $text = preg_replace('/<(' . $tags . ')( [^><\[\]]*)>/i', '[\1\2]', $text);
        $text = preg_replace('/<\/(' . $tags . ')>/Ui', '[/\1]', $text);
        //过滤多余html
        $text = preg_replace('/<\/?(html|head|meta|link|base|basefont|body|bgsound|title|style|script|form|iframe|frame|frameset|applet|id|ilayer|layer|name|script|style|xml|a)[^><]*>/i', '', $text);
        //过滤合法的html标签
        while (preg_match('/<([a-z]+)[^><\[\]]*>[^><]*<\/\1>/i', $text, $mat)) {
            $text = str_replace($mat[0], str_replace('>', ']', str_replace('<', '[', $mat[0])), $text);
        }
        //转换引号
        while (preg_match('/(\[[^\[\]]*=\s*)(\"|\')([^\2=\[\]]+)\2([^\[\]]*\])/i', $text, $mat)) {
            $text = str_replace($mat[0], $mat[1] . '|' . $mat[3] . '|' . $mat[4], $text);
        }
        //过滤错误的单个引号
        while (preg_match('/\[[^\[\]]*(\"|\')[^\[\]]*\]/i', $text, $mat)) {
            $text = str_replace($mat[0], str_replace($mat[1], '', $mat[0]), $text);
        }
        //转换其它所有不合法的 < >
//        $text = str_replace('<', '&lt;', $text);
//        $text = str_replace('>', '&gt;', $text);
//        $text = str_replace('"', '&quot;', $text);
        //反转换
        $text = str_replace('[', '<', $text);
        $text = str_replace(']', '>', $text);
        $text = str_replace('|', '"', $text);
        //过滤多余空格
        $text = str_replace('  ', '', $text);
        $text = str_replace('　　', '', $text);
        $text = str_replace(' ', '', $text);
        $text = str_replace(' ', '', $text);
        if ($istext) {
            $text = strip_tags($text);
        }
        return $text;
    }

    /**
     * 保存网络图片
     * @param $url
     * @param string $extension
     * @param string $dir
     * @return string
     */
    public static function storeImage($url, $extension = '.jpg', $disk='public_book')
    {
        try {
            $client   = new Client([ 'verify' => false ]);
            try{
                $img      = $client->request('get', $url);
            }catch (\Exception $e){
                return '';
            }
            $path     = date('Y-m-d');
            $filename = time() . "_" . uniqid() . $extension;
            if ($img->getStatusCode() == '200') {
                $data = $img->getBody()->getContents();
                Storage::disk($disk)->exists($path) or Storage::disk($disk)->makeDirectory($path);
                Storage::disk($disk)->put($path . '/' . $filename, $data);
                return Storage::disk($disk)->url($path . '/' . $filename);
            } else {
                throw new Exception('文件获取失败~~');
            }
        } catch (Exception $e) {
            return '';
        }
    }

    public static function getImg($str, $isopen = false)
    {
        preg_match_all("/<[img|IMG].*?src=['|\"](.*?(?:[.gif|.jpg]))['|\"].*?[\/]?>/", $str, $matches);
        if (isset($matches[1])) {
            $img = $matches[1];
            if ($isopen) {
                $cover = '';
                foreach ($img as $url) {
                    if (@fopen($url, 'r') && !strstr($url, 'html')) {
                        return $cover = $url;
                    }
                }
                return $cover;
            } else {

                if (isset($img[0]) && filter_var($img[0], FILTER_VALIDATE_URL) != false) {
                    return $img[0];
                } else {
                    return '';
                }
            }
        } else {
            return '';
        }
    }

    /**
     * 整理html
     * @param $text
     * @param bool $istext
     * @return mixed|null|string|string[]
     */
    public static function formatHtml($text, $purifier=[], $istext = false)
    {
        $oldText = $text;
        if ($istext) {
            $text = strip_tags($text);
            if ($text == ''){
                $text = preg_replace('/\r?\n/', '', $oldText);
                $text = str_replace('  ', '', $text);
                $text = str_replace('　　', '', $text);
                $text = str_replace(' ', '', $text);
                $text = strip_tags($text);
            }
            return $text;
        }
        $text    = str_replace('\u003C', '<', $text);
        $text    = str_replace('\u003E', '>', $text);
        $text    = str_replace('\u002F', '/', $text);
        $text =  Purifier::clean($text,  $purifier);
        // 块级元素只保留div
        $text = str_replace('<div></div>', '', $text);
        //过滤多余空格
        $text = self::formatHtmlSpace($text);
        return $text;
    }

    public static function formatHtmlSpace($text)
    {
        $text = str_replace('  ', '', $text);
        $text = str_replace('　　', '', $text);
        $text = str_replace(' ', '', $text);
        $text = str_replace('	', '', $text);
        $text = preg_replace('/\r?\n/', '', $text);
        return $text;
    }

    public static function getDomain($url)
    {
        if (self::is_url($url)) {
            $urls = parse_url($url);
            if (isset($urls['host']) && $urls['host']) {
                return $urls['scheme'] . '://' . $urls['host'];
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /***
     * [转码]
     * @param $text
     * @return bool|false|string|string[]|null
     */
    public static function convertEncoding($text)
    {

        $encode = mb_detect_encoding($text, [ 'ASCII', 'GB2312', 'GBK', 'UTF-8', 'BIG5' ]);
        $text   = mb_convert_encoding($text, 'UTF-8', $encode);
        return $text;
    }

    /**
     * Get User-Agent
     * @return mixed
     */
    public static function getUserAgent()
    {

        $user_agents = [
            "Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-CN) AppleWebKit/523.15 (KHTML, like Gecko, Safari/419.3) Arora/0.3 (Change: 287 c9dfb30)",
            "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.2pre) Gecko/20070215 K-Ninja/2.1.1",
            "Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-CN; rv:1.9) Gecko/20080705 Firefox/3.0 Kapiko/3.0",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/535.20 (KHTML, like Gecko) Chrome/19.0.1036.7 Safari/535.20",
            "Opera/9.80 (Macintosh; Intel Mac OS X 10.6.8; U; fr) Presto/2.9.168 Version/11.52",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.11 TaoBrowser/2.0 Safari/536.11",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.71 Safari/537.1 LBBROWSER",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.84 Safari/535.11 LBBROWSER",
            "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.89 Safari/537.1",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.89 Safari/537.1",
            "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:2.0b13pre) Gecko/20110307 Firefox/4.0b13pre",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11",
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36",
            'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36',

        ];

        $Android_USER_AGENT = [
            'Mozilla/5.0 (Linux; Android 7.1.1; MI 6 Build/NMF26X; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/57.0.2987.132 MQQBrowser/6.2 TBS/043807 Mobile Safari/537.36 MicroMessenger/6.6.1.1220(0x26060135) NetType/WIFI Language/zh_CN',
            'Mozilla/5.0 (Linux; Android 7.1.1; OD103 Build/NMF26F; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/53.0.2785.49 Mobile MQQBrowser/6.2 TBS/043632 Safari/537.36 MicroMessenger/6.6.1.1220(0x26060135) NetType/4G Language/zh_CN',
            'Mozilla/5.0 (Linux; Android 6.0.1; SM919 Build/MXB48T; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/53.0.2785.49 Mobile MQQBrowser/6.2 TBS/043632 Safari/537.36 MicroMessenger/6.6.1.1220(0x26060135) NetType/WIFI Language/zh_CN',
            'Mozilla/5.0 (Linux; Android 5.1.1; vivo X6S A Build/LMY47V; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/53.0.2785.49 Mobile MQQBrowser/6.2 TBS/043632 Safari/537.36 MicroMessenger/6.6.1.1220(0x26060135) NetType/WIFI Language/zh_CN',
            'Mozilla/5.0 (Linux; Android 5.1; HUAWEI TAG-AL00 Build/HUAWEITAG-AL00; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/53.0.2785.49 Mobile MQQBrowser/6.2 TBS/043622 Safari/537.36 MicroMessenger/6.6.1.1220(0x26060135) NetType/4G Language/zh_CN',
            'Mozilla/5.0 (Linux; Android 7.0; FRD-AL10 Build/HUAWEIFRD-AL10; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/62.0.3202.84 Mobile Safari/537.36 MicroMessenger/6.7.3.1360(0x26070336) NetType/WIFI Language/zh_CN Process/appbrand0'
        ];

        $iPhone_USER_AGENT = [
            'Mozilla/5.0 (iPhone; CPU iPhone OS 9_3_2 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Mobile/13F69 MicroMessenger/6.6.1 NetType/4G Language/zh_CN',
            'Mozilla/5.0 (iPhone; CPU iPhone OS 11_2_2 like Mac OS X) AppleWebKit/604.4.7 (KHTML, like Gecko) Mobile/15C202 MicroMessenger/6.6.1 NetType/4G Language/zh_CN',
            'Mozilla/5.0 (iPhone; CPU iPhone OS 11_1_1 like Mac OS X) AppleWebKit/604.3.5 (KHTML, like Gecko) Mobile/15B150 MicroMessenger/6.6.1 NetType/WIFI Language/zh_CN',
            'Mozilla/5.0 (iphone x Build/MXB48T; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/53.0.2785.49 Mobile MQQBrowser/6.2 TBS/043632 Safari/537.36 MicroMessenger/6.6.1.1220(0x26060135) NetType/WIFI Language/zh_CN'
        ];

        $user_agent = $user_agents[rand(0, count($user_agents) - 1)];
        return $user_agent;
    }


    public static function getProxy()
    {
        $proxy = [
            'http://39.137.77.68:8080',
            'http://178.128.72.132:80',
            'http://119.180.157.246:8060',
            'http://39.137.107.98:8080',
            'http://13.126.134.227:3128',
            'http://117.191.11.108:80',
            'http://117.191.11.79:8080',
            'http://120.210.219.103:8080',
            'http://111.230.7.27:3128',
            'http://121.12.85.2:80',
            'http://218.60.8.99:3129',
            'http://178.128.72.132:8080',
            'http://120.79.99.27:80',
            'http://66.7.113.39:3128',
            'http://119.36.161.174:80',
            'http://117.191.11.110:80',
            'http://183.179.199.232:8080',
            'http://101.37.118.54:8888',
            'http://120.210.219.102:80',
            'http://120.210.219.104:80',
            'http://210.34.24.103:3128',
            'http://47.89.13.67:80',
            'http://117.191.11.112:80',
            'http://120.210.219.105:80',
            'http://58.240.53.196:8080',
            'http://47.56.0.164:80',
            'http://47.97.237.134:80',
            'http://27.221.32.190:80',
            'http://120.210.219.101:80',
            'http://120.210.219.102:8080',
            'http://117.191.11.80:8080',
            'http://39.137.77.67:80',
            'http://39.137.77.67:8080',
            'http://120.210.219.104:8080',
            'http://119.41.236.180:8010',
            'http://212.64.91.25:8888',
            'http://218.60.8.83:3129',
            'http://218.60.8.98:3129',
            'http://117.191.11.111:80',
            'http://117.191.11.71:80',
            'http://62.234.188.240:3128',
            'http://120.234.63.196:3128',
            'http://39.137.69.10:8080',
            'http://59.49.72.137:80',
            'http://39.137.69.7:80',
            'http://47.92.233.84:8080',
            'http://157.230.149.189:8080',
            'http://59.49.72.138:80',
        ];
        return $proxy[rand(0, count($proxy) - 1)];
    }


    public static function setHeader($url, $cookie, $is_roxy = false)
    {
        $host    = parse_url($url, PHP_URL_HOST);
        $headers = [
            'timeout' => 60,
            'headers' => [
                'Referer'                   => "http://$host/",
                'Accept'                    => "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
                'Upgrade-Insecure-Requests' => "1",
                'Host'                      => $host,
                'DNT'                       => "1",
                'User-Agent'                => self::getUserAgent(),
                'Cookie'                    => $cookie
            ],
        ];
        if ($is_roxy) {
            $headers['proxy'] = self::getProxy();
        }
        return $headers;
    }

    /**
     * 验证URl
     * @param $url
     * @return bool
     */
    public static function is_url($url)
    {
        $pattern = "#(http|https|//)(.*\.)?.*\..*#i";
        if (preg_match($pattern, $url)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * [urlcheck]
     * @param $url
     * @param $baseurl
     * @return string
     */
    public static function urlCheck($url, $baseurl)
    {
        $urlinfo = parse_url($baseurl);

        $baseurl = $urlinfo['scheme'] . '://' . $urlinfo['host'] . (substr($urlinfo['path'], -1, 1) === '/' ? substr($urlinfo['path'], 0, -1) : str_replace('\\', '/', dirname($urlinfo['path']))) . '/';
        $domain  = $urlinfo['scheme'] . '://' . $urlinfo['host'];
        if (strpos($url, '://') === false) {
            if ($url[0] == '/') {
                $url = $urlinfo['scheme'] . '://' . $urlinfo['host'] . $url;
            } else {
                if (str_contains($url, 'javascript')) {
                    $url = '';
                } else {
                    if ($url[0] == '.') {
                        if ($url[1] == '.') {
                            $url = $domain . '/' . str_replace('../', '', $url);
                        } else {
                            $url = $baseurl . preg_replace('/.\//', '', $url, 1);
                        }
                        #$url = $baseurl . str_replace('./', '', $url);
                    } else {
                        $url = $baseurl . $url;
                    }
                }
            }
        }
        return $url;
    }
}