<?php

namespace Iwester\Services;

use App;
use function GuzzleHttp\Psr7\str;
use Illuminate\Support\Facades\Storage;
use Iwester\Http\Model\Article\Article;
use Iwester\Http\Model\Article\ArticleCategory;
use Iwester\Http\Model\Article\ArticleTag;
use Log;
use Roumen\Sitemap\Sitemap;

class SitemapService
{
    public $siteConfig;
    public function __construct($siteConfig)
    {
        $this->siteConfig = $siteConfig;
    }

    protected function putFile($p){
        if (!file_exists($p)) {
            Storage::put($p, '', 'public');
        }
    }

    public function buildArticles()
    {
        $sitemap = App::make("sitemap");
        $lastModTime = 0;

        Article::select(['id','created_at', 'updated_at'])->where([
            'publish_status'=> 1,
            'audit_status'=> 1
        ])->chunk(100,
            function ($articles) use (&$sitemap, &$lastModTime) {
            foreach ($articles as $article) {
                $tagLastModTime = strtotime($article->updated_at);
                if ($tagLastModTime > $lastModTime) $lastModTime = $tagLastModTime;
                $url = route('article.detail', ['id'=> $article->id]);
                $url = str_replace('http://localhost', $this->siteConfig->site_url, $url);
                $sitemap->add($url, date(DATE_RFC3339, strtotime($article->updated_at)), '0.8', 'daily');
            }
        });
        $this->putFile('sitemap/article.xml');
        $info = $sitemap->store('xml','article', public_path('/sitemap'));
        Log::info($info);
        return $lastModTime;
    }

    public function buildCategories()
    {
        $sitemap = App::make("sitemap");
        $lastModTime = 0;

        ArticleCategory::chunk(100, function ($categories) use ($sitemap, &$lastModTime) {
            foreach ($categories as $category) {
                $catLastModTime = strtotime($category->updated_at);
                if ($catLastModTime > $lastModTime) $lastModTime = $catLastModTime;
                $url = $this->siteConfig->site_url.'/'.$category->alias;
                $sitemap->add($url, date(DATE_RFC3339, strtotime($category->updated_at)), '0.9', 'daily');
            }
        });
        $this->putFile('sitemap/categories.xml');
        $info = $sitemap->store('xml','categories', public_path('/sitemap'));
        Log::info($info);
        return $lastModTime;
    }

    public function buildTags()
    {
        $sitemap = App::make("sitemap");
        $lastModTime = 0;

        ArticleTag::where('status',1)->chunk(100, function ($tags) use ($sitemap, &$lastModTime) {
            foreach ($tags as $tag) {
                $catLastModTime = strtotime($tag->updated_at);
                if ($catLastModTime > $lastModTime) $lastModTime = $catLastModTime;
                $url = $this->siteConfig->site_url.'/'.$tag->tag;
                $sitemap->add($url, date(DATE_RFC3339, strtotime($tag->updated_at)), '0.9', 'daily');
            }
        });
        $this->putFile('sitemap/tag.xml');
        $info = $sitemap->store('xml','tag', public_path('/sitemap'));
        Log::info($info);
        return $lastModTime;
    }

    public function buildHome()
    {
        $sitemap = App::make("sitemap");
        $sitemap->add($this->siteConfig->site_url, date(DATE_RFC3339, time()), '1.0', 'daily');
        $this->putFile('sitemap/home.xml');
        $info = $sitemap->store('xml', 'home', public_path('/sitemap'));
        Log::info($info);
        return true;
    }


    public function buildIndex()
    {
        $sitemap = App::make ("sitemap");
        if ($this->buildHome()) {
            $sitemap->addSitemap($this->siteConfig->site_url . '/sitemap/home.xml', date(DATE_RFC3339, time()));
        }

        if ($lastModTime = $this->buildArticles()) {
            $sitemap->addSitemap($this->siteConfig->site_url . '/sitemap/article.xml', date(DATE_RFC3339, $lastModTime));
        }

        if ($lastModTime = $this->buildCategories()) {
            $sitemap->addSitemap($this->siteConfig->site_url . '/sitemap/categories.xml', date(DATE_RFC3339, $lastModTime));
        }

        if ($lastModTime = $this->buildTags()) {
            $sitemap->addSitemap($this->siteConfig->site_url . '/sitemap/tag.xml', date(DATE_RFC3339, $lastModTime));
        }
        $sitemap->store('sitemapindex', 'sitemap');
    }

}