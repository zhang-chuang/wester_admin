$(function () {
    alert(1)
    function publicOpenPage(title, area, openUrl, saveUrl, type = 'POST') {
        var openPage = layer.open({
            type: 2,
            title: title,
            shadeClose: false,
            scrollbar: false,
            shift: 3,
            btn: ['提交', '取消'],
            shade: 0.2,
            min: true,
            area: area,
            content: [openUrl],
            yes: function (index, layero) {
                var dataform = $(layero).find("iframe")[0].contentWindow.formdata();
                publicOpenPageSave(dataform, index, saveUrl, type);
            },
            cancel: function (index, layero) {
                layer.close(index)
            }
        });
    }
    function publicOpenPageSave(data, layerindex, saveUrl, type){
        var loading = layer.load( 3, {
            shade: [0.1,'#fff']
        });
        $.ajax({
            url: saveUrl,
            type: type,
            dataType: 'json',
            data: data,
        })
            .done(function (data) {
                if (data.code == 200) {
                    layer.alert(data.message, {
                        icon: 1,
                        closeBtn: 0,
                        shift: 3
                    }, function (index) {
                        layer.close(index);
                        location.reload();
                    });
                } else {
                    layer.close(loading);
                    layer.alert(data.message, {icon: 2, shift: 3});
                }
            })
            .fail(function () {
                layer.close(loading);
                layer.alert('服务器异常', {icon: 2, shift: 3});
            });
    }
})