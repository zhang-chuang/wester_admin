<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/vendor/iwester/static/admin/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/vendor/iwester/static/admin/layuiadmin/style/admin.css" media="all">
    <link rel="stylesheet" href="/vendor/iwester/static/admin/layuiadmin/style/login.css" media="all">
    <style>
        .scanbg{
            width: 300px;
            margin: 20px auto 0 auto;
            text-align: center;
        }
        .scanbg i {
            display: inline-block;
            width: 42px;
            height: 40px;
            background: url('/static/images/loginscan.png') 50% no-repeat;
            margin-right: 15px;
            vertical-align: -3px;
        }
        .scanbg .scantext {
            font-size: 16px;
            text-align: left;
            display: inline-block;
        }
        .scanbg .scantext .scana {
            color: #0c7df5;
        }
    </style>
</head>
<body>

<div class="layadmin-user-login layadmin-user-display-show" style="padding: 0">

    <div class="layadmin-user-login-main">
        <div class="layadmin-user-login-box layadmin-user-login-body layui-form">
            <form method="post" class="forms_item" id="login_forms" style="margin-top: 58px;">
                {{csrf_field()}}
                @if(env('WECHAT_APPID'))
                <div class="img-wx-change change-btn" data-name="scan_login_forms" data-title="微信登录或注册" style="cursor: pointer;position: absolute;right: 0;top: 0;z-index: 2">
                    <img src="/static/images/toqrcodeh.png" alt="">
                </div>
                @endif
                <div class="layui-form-item">
                    <label class="layadmin-user-login-icon layui-icon layui-icon-username" for="LAY-user-login-username"></label>
                    <input type="text" name="username" value="" lay-verify="required" placeholder="用户名/邮箱" class="layui-input">
                </div>
                <div class="layui-form-item">
                    <label class="layadmin-user-login-icon layui-icon layui-icon-password" for="LAY-user-login-password"></label>
                    <input type="password" name="password"  lay-verify="required" placeholder="密码" class="layui-input">
                </div>
                <div class="layui-form-item" style="margin-bottom: 20px;">
                    <input type="checkbox" name="remember" lay-skin="primary" title="记住密码">
                    <a id="" href="javascript:;" class="layadmin-user-jump-change layadmin-link" style="margin-top: 7px;"></a>
                    <a data-name="reset_forms" data-title="找回密码" href="javascript:;" class="layadmin-user-jump-change layadmin-link change-btn">忘记密码？</a>
                </div>
                <div class="layui-form-item">
                    <input type="hidden" name="ajax" value="1">
                    <span onclick="return login_formdata($(this))" class="layui-btn layui-btn-fluid" lay-submit lay-filter="">登 入</span>
                </div>
                <div class="layui-trans layui-form-item layadmin-user-login-other" style="padding-top: 0">
                    {{--<a href="javascript:;"><i class="layui-icon layui-icon-login-qq"></i></a>--}}
                    {{--<a href="javascript:;" class="change-btn" data-name="scan_login_forms" data-title="微信登录或注册"><i class="layui-icon layui-icon-login-wechat"></i></a>--}}
                    {{--<a href="javascript:;"><i class="layui-icon layui-icon-login-weibo"></i></a>--}}
                    <a data-name="register_forms" data-title="用户注册" href="javascript:;" class="layadmin-user-jump-change layadmin-link change-btn">立即注册</a>
                </div>
            </form>
            <form method="post" class="forms_item" id="scan_login_forms"  style="display: none;margin-top: 54px">
                {{csrf_field()}}
                <div class="img-wx-change change-btn" data-name="login_forms" data-title="用户名密码登录" style="cursor: pointer;position: absolute;right: 0;top: 0;z-index: 2">
                    <img src="/static/images/toaccounth.png" alt="">
                </div>
                <div class="layui-form-item">
                    <img src="/static/images/weixin.jpg" alt="" style="width: 180px; height: 180px; display: block;margin: 0 auto">
                </div>
                <div class="scanbg">
                    <i></i>
                    <div class="scantext">
                        <div>扫码<span class="scana">关注公众号</span></div>
                        <div>立即登录</div>
                    </div>
                </div>
            </form>
            <form method="post" class="forms_item" id="register_forms" style="display: none;margin-top: 24px">
                {{csrf_field()}}
                <input type="hidden" name="user[email_verified_at]" value="{{ date('Y-m-d H:i:s') }}">
                <input type="hidden" name="type" value="register">
                <div class="layui-form-item">
                    <label class="layadmin-user-login-icon layui-icon layui-icon-username" for="LAY-user-login-username"></label>
                    <input type="text" name="user[username]" value="" lay-verify="required" placeholder="用户名" class="layui-input">
                </div>
                <div class="layui-form-item">
                    <label class="layadmin-user-login-icon layui-icon layui-icon-password" for="LAY-user-login-password"></label>
                    <input type="password" name="user[password]"  lay-verify="required" placeholder="密码" class="layui-input">
                </div>
                <div class="layui-form-item">
                    <label class="layadmin-user-login-icon layui-icon layui-icon-username" for="LAY-user-login-password"></label>
                    <input type="text" name="user[email]"  lay-verify="required" placeholder="邮箱" class="layui-input">
                </div>
                <div class="layui-form-item">
                    <div class="layui-row">
                        <div class="layui-col-xs7">
                            <label class="layadmin-user-login-icon layui-icon layui-icon-vercode" for="LAY-user-login-vercode"></label>
                            <input type="text" name="vercode" id="LAY-user-login-vercode" lay-verify="required" placeholder="邮箱验证码" class="layui-input">
                        </div>
                        <div class="layui-col-xs5">
                            <div style="margin-left: 10px;">
                                <button type="button" style="border-color: #009688" class="layui-btn layui-btn-primary layui-btn-fluid send_email" id="">邮箱验证码</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <input type="hidden" name="ajax" value="1">
                    <span onclick="return register_formdata($(this))" class="layui-btn layui-btn-fluid" lay-submit lay-filter="">注 册</span>
                </div>
                <div class="layui-trans layui-form-item layadmin-user-login-other" style="padding-top: 0">
                    <a data-name="login_forms" data-title="用户名密码登录" href="javascript:;" class="layadmin-user-jump-change layadmin-link change-btn">已有账号，立即登录</a>
                </div>
            </form>
            <form method="post" class="forms_item" id="reset_forms" style="display: none;margin-top: 24px">
                {{csrf_field()}}
                <input type="hidden" name="type" value="reset">
                <div class="layui-form-item">
                    <label class="layadmin-user-login-icon layui-icon layui-icon-username" for="LAY-user-login-password"></label>
                    <input type="text" name="user[email]"  lay-verify="required" placeholder="邮箱" class="layui-input">
                </div>
                <div class="layui-form-item">
                    <label class="layadmin-user-login-icon layui-icon layui-icon-password" for="LAY-user-login-password"></label>
                    <input type="password" name="user[password]"  lay-verify="required" placeholder="密码" class="layui-input">
                </div>
                <div class="layui-form-item">
                    <label class="layadmin-user-login-icon layui-icon layui-icon-password" for="LAY-user-login-password_confirmation"></label>
                    <input type="password" name="user[password_confirmation]"  lay-verify="required" placeholder="确认密码" class="layui-input">
                </div>
                <div class="layui-form-item">
                    <div class="layui-row">
                        <div class="layui-col-xs7">
                            <label class="layadmin-user-login-icon layui-icon layui-icon-vercode" for="LAY-user-login-vercode"></label>
                            <input type="text" name="vercode" id="LAY-user-login-vercode" lay-verify="required" placeholder="邮箱验证码" class="layui-input">
                        </div>
                        <div class="layui-col-xs5">
                            <div style="margin-left: 10px;">
                                <button type="button" style="border-color: #009688" class="layui-btn layui-btn-primary layui-btn-fluid send_email" id="">邮箱验证码</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <input type="hidden" name="ajax" value="1">
                    <span onclick="return rest_formdata()" class="layui-btn layui-btn-fluid" lay-submit lay-filter="">确认修改</span>
                </div>
                <div class="layui-trans layui-form-item layadmin-user-login-other" style="padding-top: 0">
                    <a data-name="login_forms" data-title="用户名密码登录" href="javascript:;" class="layadmin-user-jump-change layadmin-link change-btn">返回登录/注册</a>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="/vendor/iwester/static/admin/layuiadmin/layui/layui.js"></script>
<script>
    layui.config({
        base: '/vendor/iwester/static/admin/layuiadmin/' //静态资源所在路径
    }).use(['layer'],function () {
        var layer = layui.layer;
    })
</script>
<script src="/vendor/iwester/js/jquery.min.js"></script>
<script>
    $('.change-btn').on('click', function () {
        var name = $(this).data('name');
        var title = $(this).data('title');
        $('#'+name).show().siblings().hide();
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.title(title, index)
    });

    // 用户登录
    var login_formdata = function (_this) {
        if (_this.hasClass('on')){
            return false;
        }
        _this.text('正在登录中...').addClass('on');
        var loading = layer.load( 3, {
            shade: [0.1,'#fff']
        });
        $.ajax({
            url: '{{ route('admin.login') }}',
            type: 'POST',
            dataType: 'json',
            data: $("#login_forms").serializeArray(),
        })
            .done(function (data) {
                layer.close(loading);
                if (data.code == 200) {
                    layer.msg('登录成功~', {icon: 1, shift: 3});
                    setTimeout(function () {
                        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
                        parent.layer.close(index); //再执行关闭
                        window.parent.location.reload();
                    }, 1000);
                } else {
                    _this.text('重新登录').removeClass('on');
                    layer.msg(data.message, {icon: 2, shift: 3});
                }
            })
            .fail(function () {
                _this.text('重新登录').removeClass('on');
                layer.close(loading);
                layer.msg('服务器异常~', {icon: 2, shift: 3});
            });
        return ;
    }

    // 用户注册
    var register_formdata = function (_this) {
        if (_this.hasClass('on')){
            return false;
        }
        _this.text('正在注册中...').addClass('on');
        var loading = layer.load( 3, {
            shade: [0.1,'#fff']
        });
        $.ajax({
            url: '{{ route('admin.register') }}',
            type: 'POST',
            dataType: 'json',
            data: $("#register_forms").serializeArray(),
        })
            .done(function (data) {
                layer.close(loading);
                if (data.code == 200) {
                    layer.msg(data.message, {icon: 1, shift: 3});
                    setTimeout(function () {
                        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
                        parent.layer.close(index); //再执行关闭
                        window.parent.location.reload();
                    }, 1000);
                }else if (data.code == 422) {
                    _this.text('注册').removeClass('on');
                    tip = '';
                    for (var i in data.errors) {
                        tip += data.errors[i][0] + '，';
                    }
                    layer.msg(tip, {icon: 2, shift: 3});
                }else {
                    _this.text('注册').removeClass('on');
                    layer.msg(data.message, {icon: 2, shift: 3});
                }
            })
            .fail(function () {
                _this.text('注册').removeClass('on');
                layer.close(loading);
                layer.msg('服务器异常~', {icon: 2, shift: 3});
            });
        return ;
    }

    // 忘记密码
    var rest_formdata = function () {
        var loading = layer.load( 3, {
            shade: [0.1,'#fff']
        });
        $.ajax({
            url: '{{ route('admin.resetPassword') }}',
            type: 'POST',
            dataType: 'json',
            data: $("#reset_forms").serializeArray(),
        })
            .done(function (data) {
                layer.close(loading);
                if (data.code == 200) {
                    layer.msg(data.message, {icon: 1, shift: 3});
                    setTimeout(function () {
                        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
                        parent.layer.close(index); //再执行关闭
                        window.parent.location.reload();
                    }, 1000);
                }else if (data.code == 422) {
                    tip = '';
                    for (var i in data.errors) {
                        tip += data.errors[i][0] + '，';
                    }
                    layer.msg(tip, {icon: 2, shift: 3});
                }else {
                    layer.msg(data.message, {icon: 2, shift: 3});
                }
            })
            .fail(function () {
                layer.close(loading);
                layer.msg('服务器异常~', {icon: 2, shift: 3});
            });
        return ;
    }
    


    $('.send_email').on('click', function () {
       var email = $(this).parents('form').find('input[name="user[email]"]').val();
       var _token = $(this).parents('form').find('input[name="_token"]').val();
       var type = $(this).parents('form').find('input[name="type"]').val();
       if (!check_email(email)){
           layer.msg('请输入正确的邮箱~', {icon: 2, shift: 3});
           return false;
       }
        send_email(email, _token, type, '进行邮箱验证')
    });

    // 邮箱格式验证
    function check_email(email){
        var reg = new RegExp("^[a-z0-9]+([._\\-]*[a-z0-9])*@([a-z0-9]+[-a-z0-9]*[a-z0-9]+.){1,63}[a-z0-9]+$");
        if(email == ""){
            return false;
        }else if(!reg.test(email)){
            return false;
        }else{
            return true;
        }
    }


    // 发送邮件
    function send_email(email, _token, type, tip) {
        var loading = layer.load( 3, {
            shade: [0.1,'#fff']
        });
        $.ajax({
            url: '{{ route('admin.sendEmail') }}',
            type: 'POST',
            dataType: 'json',
            data: {
                'email': email,
                'tip': tip,
                'type': type,
                '_token': _token
            }
        })
            .done(function (data) {
                layer.close(loading);
                if (data.code == 200) {
                    layer.msg(data.message, {icon: 1, shift: 3});
                }else {
                    layer.msg(data.message, {icon: 2, shift: 3});
                }
            })
            .fail(function () {
                layer.close(loading);
                layer.msg('服务器异常~', {icon: 2, shift: 3});
            });
    }
</script>
</body>
</html>