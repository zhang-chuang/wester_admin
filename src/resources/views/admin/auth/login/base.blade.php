<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>登入 - layuiAdmin</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/vendor/iwester/static/admin/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/vendor/iwester/static/admin/layuiadmin/style/admin.css" media="all">
    <link rel="stylesheet" href="/vendor/iwester/static/admin/layuiadmin/style/login.css" media="all">
</head>
<body>

<div class="layadmin-user-login layadmin-user-display-show" >

    <div class="layadmin-user-login-main">
        <div class="layadmin-user-login-box layadmin-user-login-header">
            <h2>后台管理系统</h2>
            <p></p>
        </div>
        @yield('content')
    </div>
</div>
<script src="/vendor/iwester/static/admin/layuiadmin/layui/layui.js"></script>
<script>
    layui.config({
        base: '/vendor/iwester/static/admin/layuiadmin/' //静态资源所在路径
    }).use(['layer'],function () {
        var layer = layui.layer;

        //表单提示信息
        @if(session('errors'))
            @if(count(session('errors'))>0)
                @foreach(session('errors') as $error)
                    layer.msg("{{$error}}",{icon:5});
                    @break
                @endforeach
            @endif
        @endif
    })
</script>
</body>
</html>