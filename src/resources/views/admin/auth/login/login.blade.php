@extends('iwester::admin.auth.login.base')

@section('content')
    <div class="layadmin-user-login-box layadmin-user-login-body layui-form">
        <form action="{{route('admin.login')}}" method="post">
            {{csrf_field()}}
            <div class="layui-form-item">
                <label class="layadmin-user-login-icon layui-icon layui-icon-username" for="LAY-user-login-username"></label>
                <input type="text" name="username" value="{{ session('old_username') or '' }}" lay-verify="required" placeholder="用户名/邮箱" class="layui-input">
            </div>
            <div class="layui-form-item">
                <label class="layadmin-user-login-icon layui-icon layui-icon-password" for="LAY-user-login-password"></label>
                <input type="password" name="password"  lay-verify="required" placeholder="密码" class="layui-input">
            </div>
            {{--<div class="layui-form-item" style="margin-bottom: 20px;">--}}
                {{--<a href="{{ route('admin.register') }}" target="_blank" class="layadmin-user-jump-change layadmin-link" style="float: left;margin-top: 7px;">注册帐号</a>--}}
                {{--<input type="checkbox" name="remember" lay-skin="primary" title="记住密码">--}}
                {{--<a href="{{ route('admin.register') }}" target="_blank" class="layadmin-user-jump-change layadmin-link" style="margin-top: 7px;">忘记密码？</a>--}}
            {{--</div>--}}
            <div class="layui-form-item">
                <button type="submit" class="layui-btn layui-btn-fluid" lay-submit lay-filter="">登 入</button>
            </div>
        </form>
        {{--<div class="layui-trans layui-form-item layadmin-user-login-other" style="display: none">--}}
            {{--<label>社交账号登入</label>--}}
            {{--<a href="javascript:;"><i class="layui-icon layui-icon-login-qq"></i></a>--}}
            {{--<a href="javascript:;"><i class="layui-icon layui-icon-login-wechat"></i></a>--}}
            {{--<a href="javascript:;"><i class="layui-icon layui-icon-login-weibo"></i></a>--}}

            {{--<a href="reg.html" class="layadmin-user-jump-change layadmin-link">注册帐号</a>--}}
        {{--</div>--}}
    </div>
@endsection