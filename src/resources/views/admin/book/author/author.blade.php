@extends('iwester::admin.base')
@section('css')
    <style>
        .layui-nav a {
            color: #666 !important;
        }
        .layui-nav .layui-nav-item a:hover, .layui-nav .layui-this a {
            color: #009688 !important;
        }
        .layui-laypage li, .layui-laypage li span {
            display: inline-block;
            *display: inline;
            *zoom: 1;
            vertical-align: middle;
            height: 28px;
            line-height: 28px;
            background-color: #fff;
            color: #333;
            font-size: 12px;
        }
        .layui-laypage li.active span{
            background-color: #009688;
            color: #fff;
        }
    </style>
@endsection

@section('content')
    <div class="layui-card">
        <div class="layui-card-body">
            <div class="layui-form">
                <form action="">
                    <div class="layui-input-inline">
                        <input type="text" name="search" id="title" placeholder="请输入作者名称" class="layui-input" value="{{ isset($params['search']) ? $params['search']: '' }}">
                    </div>
                    <button type="submit" class="layui-btn">搜索</button>
                </form>
            </div>
        </div>
        <div class="layui-card-body">
            <table class="layui-table" lay-skin="line">
                <thead>
                <tr>
                    <th>编号</th>
                    <th>作者名称</th>
                    <th>作者等级</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                @foreach($authors as $author)
                    <tr>
                        <td>{{ $author->id }}</td>
                        <td>{{ $author->author_name }}</td>
                        <td>{{ $author->author_level }}</td>
                        <td>
                            <a href="{{ route('book.author.edit', ['id'=> $author->id]) }}">
                                <span class="layui-btn layui-btn-normal layui-btn-sm">
                                    <i class="layui-icon layui-icon-edit"></i>编辑
                                </span>
                            </a>
                            <a href="{{ route('book.bookshelf') }}?book_author_id={{ $author->id }}">
                                <span class="layui-btn layui-btn-normal layui-btn-sm">书籍详情</span>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="layui-box layui-laypage layui-laypage-default" id="">
                {{$authors->appends($params)->render()}}
            </div>
        </div>
    </div>

    <script>
        $('.layui-nav li a').on('click', function () {
            var loading = layer.load( 3, {
                shade: [0.1,'#fff']
            });
        })


        // 全选
        $(".id_checked_all").click(function(){
            if($(this).prop("checked")){
                $(".id_checked").prop("checked",true);
            }else{
                $(".id_checked").removeProp("checked");
            }
            inputDown();
        });
        $(".id_checked").click(function(){
            inputDown();
        });

        var idArr = [];
        function inputDown() {
            idArr = [];
            var idArr1 = [];
            var len = $('.id_checked').length;
            for(var i = 0; i<len; i++){
                var $this = $('.id_checked').eq(i);
                if($this.prop('checked')){
                    var id = $this.val();
                    idArr1.push(id)
                }
            }
            for(var t=0;t<idArr1.length;t++){
                tArray(idArr1[t],idArr);
            }
            if (idArr.length == len) {
                $(".id_checked_all").prop("checked",true);
            }else{
                $(".id_checked_all").removeProp("checked");
            }
            console.log(idArr)
        }

        //数组去重
        function tArray(i,arr){
            var yap=false;
            for(var j=0;j<arr.length;j++){
                if(arr[j]==i){yap=true;break;};
            }
            if(!yap) arr.push(i);
        };
    </script>
@endsection


@section('script')

@endsection
