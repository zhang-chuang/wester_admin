@extends('iwester::admin.base')
@section('css')
    <link rel="stylesheet" type="text/css" href="/vendor/iwester/static/wangEditor/css/wangEditor.min.css">
    <link rel="stylesheet" type="text/css" href="/vendor/iwester/static/select2/select2.min.css">
    <style type="text/css">
        #editor-trigger {
            height: 400px;
            /*max-height: 500px;*/
        }
        .container {
            width: 100%;
            margin: 0 auto;
            position: relative;
        }
        #tags .layui-form-select{
            display: none!important;
        }
        #tags .select2-container,#tags .select2-container--default .select2-selection--single{
            width: 100%!important;
            height: 38px;
            border-radius: 2px;
            border-color: #D2D2D2 !important;
        }
        #tags .select2-container--default .select2-selection--single .select2-selection__rendered,
        #tags .select2-container--default .select2-selection--single .select2-selection__arrow{
            height: 100%!important;
            line-height: 38px;
            color: #666;
        }
    </style>
@endsection
@section('content')
    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <h2>{{ $bookAuthor ? '编辑' : '添加' }}作者</h2>
        </div>
        <div class="layui-card-body">
            <form class="layui-form" action="{{route('book.author.store')}}" method="post">
                {{csrf_field()}}
                <input type="hidden" name="id" value="{{ $bookAuthor ? $bookAuthor->id : 0 }}">
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">作者名称</label>
                    <div class="layui-input-block">
                        <input type="text" name="author[author_name]" value="{{ $bookAuthor ? $bookAuthor->author_name : '' }}" lay-verify="required" placeholder="作者名称" class="layui-input" >
                    </div>
                </div>
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">作者等级</label>
                    <div class="layui-input-block">
                        <select name="author[author_level]" lay-verify="" >
                            <option value="">请选择作者等级</option>
                            @foreach(\Iwester\Http\Model\Book\BookAuthor::$level as $value)
                                <option {{ $bookAuthor && $bookAuthor->author_level == $value ? 'selected' : '' }} value="{{ $value }}">{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <button type="submit" class="layui-btn" lay-submit="" lay-filter="formDemo">确 认</button>
                        <a  class="layui-btn" href="javascript:;" onclick="javascript:history.back(-1);">返 回</a>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script>
        function uploadFile(obj, fileUpload) {
            var loading = layer.load( 2, {
                shade: [0.1,'#fff']
            });
            var input = obj.parents('.layui-input-block').find('input[type=text]');
            $.ajaxFileUpload({
                url: "{{ route('uploadImg') }}",
                secureuri: false,
                fileElementId: fileUpload,
                dataType: 'json',
                success: function (data) {
                    if (data.code) {
                        input.val(data.path);
                        $('.img-cover img').attr('src', data.path)
                    } else {
                        parent.layer.alert(data.msg);
                    }
                    layer.close(loading);
                },
                error: function (data) {
                    layer.close(loading);
                    parent.layer.alert('服务异常~~');
                }
            });

            return false;
        }

        $('.img-cover-input').on('change', function(){
            $(this).parents('.layui-form-item').find('.img-cover img').attr('src', $(this).val());
        });
        function open_image(_this){
            layer.open({
                type: 1,
                title: false,
                closeBtn: 0,
                area: ['auto'],
                skin: 'layui-layer-nobg',
                shadeClose: true,
                content: _this.find('div').html()
            });
        }
    </script>
@endsection


@section('script')
    <script type="text/javascript" src="/vendor/iwester/static/wangEditor/js/wangEditor.js"></script>
    <script type="text/javascript" src="/vendor/iwester/static/select2/select2.full.min.js"></script>
    <script type="text/javascript">


        // 阻止输出log
        // wangEditor.config.printLog = false;
        var editor = new wangEditor('editor-trigger');
        // 上传图片
        editor.config.uploadImgUrl = '{{ route('uploadImg') }}';
        editor.config.uploadImgFileName = 'file';

        // 自定义load事件
        editor.config.uploadImgFns.onload = function (resultText, xhr) {
            resultText = JSON.parse(resultText)
            // resultText 服务器端返回的text
            // xhr 是 xmlHttpRequest 对象，IE8、9中不支持

            // 上传图片时，已经将图片的名字存在 editor.uploadImgOriginalName
            var originalName = editor.uploadImgOriginalName || '';

            // 如果 resultText 是图片的url地址，可以这样插入图片：
            editor.command(null, 'insertHtml', '<img src="' + resultText.path + '" alt="' + originalName + '" style="max-width:100%;"/>');
        };


        editor.create();
    </script>
@endsection
