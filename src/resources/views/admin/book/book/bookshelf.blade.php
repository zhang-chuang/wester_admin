@extends('iwester::admin.base')
@section('css')
    <style>
        .layui-nav a {
            color: #666 !important;
        }
        .layui-nav .layui-nav-item a:hover, .layui-nav .layui-this a {
            color: #009688 !important;
        }
        .layui-laypage li, .layui-laypage li span {
            display: inline-block;
            *display: inline;
            *zoom: 1;
            vertical-align: middle;
            height: 28px;
            line-height: 28px;
            background-color: #fff;
            color: #333;
            font-size: 12px;
        }
        .layui-laypage li.active span{
            background-color: #009688;
            color: #fff;
        }
    </style>
@endsection

@section('content')
    <div class="layui-card">
        <div class="layui-card-body">
            <div class="layui-form">
                <form action="">
                    <div class="layui-input-inline">
                        <select name="category_id" lay-verify="" >
                            <option value="">请选择分类</option>
                            @foreach($categories as $category)
                                <option {{ isset($params['category_id']) && $params['category_id'] == $category->id ? 'selected' : '' }} value="{{ $category->id }}">{{ $category->cate_name }}</option>
                                @include('iwester::admin.book.book.cate_el', ['category'=> $category, 'shendu'=> 1, 'select'=> isset($params['category_id']) ? $params['category_id'] : false])
                            @endforeach
                        </select>
                    </div>
                    <div class="layui-input-inline">
                        <input type="text" name="title" id="title" placeholder="请输入书籍标题" class="layui-input" value="{{ isset($params['title']) ? $params['title']: '' }}">
                    </div>
                    <button type="submit" class="layui-btn">搜索</button>
                </form>
            </div>
        </div>
        <div class="layui-card-body">
            <table class="layui-table" lay-skin="line">
                <thead>
                <tr>
                    <th>编号</th>
                    <th>标题</th>
                    <th>栏目</th>
                    <th>作者</th>
                    <th>连载状态</th>
                    <th>最新更新时间</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                @foreach($books as $book)
                    <tr>
                        <td>{{ $book->id }}</td>
                        <td>{{ $book->name }}</td>
                        <td>{{ $book->category->cate_name ?? '' }}</td>
                        <td>{{ $book->author->author_name ?? '' }}</td>
                        <td>{{ $book->serial_status }}</td>
                        <td>{{ $book->last_update_time }}</td>
                        <td>
                            @can('book.bookshelf.edit')
                                <a href="{{ route('book.bookshelf.edit', ['id'=> $book->id]) }}">
                                    <span class="layui-btn layui-btn-normal layui-btn-sm">
                                        <i class="layui-icon layui-icon-edit"></i>编辑
                                    </span>
                                </a>
                                <a href="{{ route('book.bookshelf.chapters', ['id'=> $book->id]) }}">
                                    <span class="layui-btn layui-btn-normal layui-btn-sm">
                                        章节管理
                                    </span>
                                </a>
                            @endcan
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="layui-box layui-laypage layui-laypage-default" id="">
                {{$books->appends($params)->render()}}
            </div>
        </div>
    </div>

    <script>

    </script>
@endsection


@section('script')

@endsection
