@if($category->subCate)
    @foreach($category->subCate as $subCate)
        @php
            $nbsp = '&nbsp;&nbsp;';
            for($i=0; $i< $shendu; $i++){
                $nbsp .= '&nbsp;&nbsp;';
            }
        @endphp
        <option value="{{ $subCate->id }}" {{ $select && $select == $subCate->id ? 'selected' : '' }}>{!! $loop->last ? $nbsp.'└─ ': $nbsp.'├─ ' !!}{{ $subCate->cate_name }}</option>
        @if($subCate->subCate)
            @include('iwester::admin.book.book.cate_el', ['category'=> $subCate, 'shendu'=> $shendu+1, 'select'=> $select])
        @endif
    @endforeach
@endif