@extends('iwester::admin.base')
@section('css')
    <link rel="stylesheet" type="text/css" href="/vendor/iwester/static/wangEditor/css/wangEditor.min.css">
    <link rel="stylesheet" type="text/css" href="/vendor/iwester/static/select2/select2.min.css">
    <style type="text/css">
        #editor-trigger {
            height: 400px;
            /*max-height: 500px;*/
        }
        .container {
            width: 100%;
            margin: 0 auto;
            position: relative;
        }
    </style>
@endsection
@section('content')
    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <h2>章节编辑</h2>
        </div>
        <div class="layui-card-body">
            <form class="layui-form" action="{{route('book.chapters.store')}}" method="post">
                {{csrf_field()}}
                <input type="hidden" name="id" value="{{ $chapter->id }}">
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">标题</label>
                    <div class="layui-input-block">
                        <input type="text" name="article[title]" value="{{ $chapter->title }}" lay-verify="required" placeholder="请输入标题" class="layui-input" >
                    </div>
                </div>


                <div class="layui-form-item">
                    <label for="" class="layui-form-label">文章内容</label>
                    <div class="layui-input-block">
                        <div id="editor-container" class="container" style="padding-left: 0;padding-right: 0;">
                            <textarea lay-verify="required" id="editor-trigger" name="article[content]">{{ $chapter->content }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <button type="submit" class="layui-btn" lay-submit="" lay-filter="formDemo">确 认</button>
                        <a  class="layui-btn" href="javascript:;" onclick="javascript:history.back(-1);">返 回</a>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection


@section('script')
    <script type="text/javascript" src="/vendor/iwester/static/wangEditor/js/wangEditor.js"></script>
    <script type="text/javascript" src="/vendor/iwester/static/select2/select2.full.min.js"></script>
    <script type="text/javascript">


        // 阻止输出log
        // wangEditor.config.printLog = false;
        var editor = new wangEditor('editor-trigger');
        // 上传图片
        editor.config.uploadImgUrl = '{{ route('uploadImg') }}';
        editor.config.uploadImgFileName = 'file';

        // 自定义load事件
        editor.config.uploadImgFns.onload = function (resultText, xhr) {
            resultText = JSON.parse(resultText)
            // resultText 服务器端返回的text
            // xhr 是 xmlHttpRequest 对象，IE8、9中不支持

            // 上传图片时，已经将图片的名字存在 editor.uploadImgOriginalName
            var originalName = editor.uploadImgOriginalName || '';

            // 如果 resultText 是图片的url地址，可以这样插入图片：
            editor.command(null, 'insertHtml', '<img src="' + resultText.path + '" alt="' + originalName + '" style="max-width:100%;"/>');
        };

        // 隐藏网络图片
        //  editor.config.hideLinkImg = true;

        // 表情显示项
        // editor.config.emotionsShow = 'value';
        // editor.config.emotions = {
        //     'default': {
        //         title: '默认',
        //         data: './emotions.data'
        //     },
        //     'weibo': {
        //         title: '微博表情',
        //         data: [
        //             {
        //                 icon: 'http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/7a/shenshou_thumb.gif',
        //                 value: '[草泥马]'
        //             },
        //             {
        //                 icon: 'http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/60/horse2_thumb.gif',
        //                 value: '[神马]'
        //             },
        //             {
        //                 icon: 'http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/bc/fuyun_thumb.gif',
        //                 value: '[浮云]'
        //             },
        //             {
        //                 icon: 'http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/c9/geili_thumb.gif',
        //                 value: '[给力]'
        //             },
        //             {
        //                 icon: 'http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/f2/wg_thumb.gif',
        //                 value: '[围观]'
        //             },
        //             {
        //                 icon: 'http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/70/vw_thumb.gif',
        //                 value: '[威武]'
        //             }
        //         ]
        //     }
        // };

        // 插入代码时的默认语言
        // editor.config.codeDefaultLang = 'html'

        // 只粘贴纯文本
        // editor.config.pasteText = true;

        // 跨域上传
        // editor.config.uploadImgUrl = 'http://localhost:8012/upload';

        // 第三方上传
        // editor.config.customUpload = true;

        // 普通菜单配置
        // editor.config.menus = [
        //     'img',
        //     'insertcode',
        //     'eraser',
        //     'fullscreen'
        // ];
        // 只排除某几个菜单（兼容IE低版本，不支持ES5的浏览器），支持ES5的浏览器可直接用 [].map 方法
        // editor.config.menus = $.map(wangEditor.config.menus, function(item, key) {
        //     if (item === 'insertcode') {
        //         return null;
        //     }
        //     if (item === 'fullscreen') {
        //         return null;
        //     }
        //     return item;
        // });

        // onchange 事件
        // editor.onchange = function () {
        //     console.log(this.$txt.html());
        // };

        // 取消过滤js
        // editor.config.jsFilter = false;

        // 取消粘贴过来
        // editor.config.pasteFilter = false;

        // 设置 z-index
        // editor.config.zindex = 20000;

        // 语言
        // editor.config.lang = wangEditor.langs['en'];

        // 自定义菜单UI
        // editor.UI.menus.bold = {
        //     normal: '<button style="font-size:20px; margin-top:5px;">B</button>',
        //     selected: '.selected'
        // };
        // editor.UI.menus.italic = {
        //     normal: '<button style="font-size:20px; margin-top:5px;">I</button>',
        //     selected: '<button style="font-size:20px; margin-top:5px;"><i>I</i></button>'
        // };
        editor.create();
    </script>
@endsection
