@extends('iwester::admin.base')
@section('css')
    <style>
        .layui-nav a {
            color: #666 !important;
        }
        .layui-nav .layui-nav-item a:hover, .layui-nav .layui-this a {
            color: #009688 !important;
        }
        .layui-laypage li, .layui-laypage li span {
            display: inline-block;
            *display: inline;
            *zoom: 1;
            vertical-align: middle;
            height: 28px;
            line-height: 28px;
            background-color: #fff;
            color: #333;
            font-size: 12px;
        }
        .layui-laypage li.active span{
            background-color: #009688;
            color: #fff;
        }
    </style>
@endsection

@section('content')
    <div class="layui-card">
        <div class="layui-card-body">
            <div class="layui-form">
                书籍：{{ $book->name }}
            </div>
        </div>
        <div class="layui-card-body">
            <table class="layui-table" lay-skin="line">
                <thead>
                <tr>
                    <th>章节ID</th>
                    <th>章节标题</th>
                    <th>文本字数</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                @foreach($chapters as $chapter)
                    <tr>
                        <td>{{ $chapter->chapter_id }}</td>
                        <td>{{ $chapter->title }}</td>
                        <td>{{ $chapter->word_num }}</td>
                        <td>
                            @can('book.bookshelf.edit')
                                <a href="{{ route('book.chapters.edit', ['id'=> $chapter->id]) }}">
                                    <span class="layui-btn layui-btn-normal layui-btn-sm">
                                        <i class="layui-icon layui-icon-edit"></i>编辑
                                    </span>
                                </a>
                            @endcan
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="layui-box layui-laypage layui-laypage-default" id="">
                {{$chapters->appends($params)->render()}}
            </div>
        </div>
    </div>

    <script>

    </script>
@endsection


@section('script')

@endsection
