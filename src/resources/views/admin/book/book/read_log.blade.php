@extends('iwester::admin.base')
@section('css')
    <style>
        .layui-nav a {
            color: #666 !important;
        }
        .layui-nav .layui-nav-item a:hover, .layui-nav .layui-this a {
            color: #009688 !important;
        }
        .layui-laypage li, .layui-laypage li span {
            display: inline-block;
            *display: inline;
            *zoom: 1;
            vertical-align: middle;
            height: 28px;
            line-height: 28px;
            background-color: #fff;
            color: #333;
            font-size: 12px;
        }
        .layui-laypage li.active span{
            background-color: #009688;
            color: #fff;
        }
    </style>
@endsection

@section('content')
    <div class="layui-card">
        <div class="layui-card-body">
            <table class="layui-table" lay-skin="line">
                <thead>
                <tr>
                    <th>编号</th>
                    <th>小说</th>
                    <th>章节</th>
                    <th>阅读时间</th>
                </tr>
                </thead>
                <tbody>
                @foreach($logs as $log)
                    <tr>
                        <td>{{ $log->id }}</td>
                        <td>{{ $log->book->name }}</td>
                        <td>{{ $log->chapter->title }}</td>
                        <td>{{ $log->created_at }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="layui-box layui-laypage layui-laypage-default" id="">
                {{$logs->render()}}
            </div>
        </div>
    </div>

    <script>

    </script>
@endsection


@section('script')

@endsection
