@extends('iwester::admin.base')

@section('content')
    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <h2>书籍栏目管理
                @can('book.category.create')
                <span style="float: right" class="layui-btn layui-btn-normal layui-btn-sm" onclick="openPage('{{ route('book.category.create', ['id'=> 0]) }}', 'add')">
                    添加一级栏目
                </span>
                @endcan
            </h2>
        </div>
        <div class="layui-card-body">
            <div class="layui-form">
                <table class="layui-table">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>栏目名称</th>
                        <th>详情URL前缀</th>
                        <th>别名</th>
                        <th>状态</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($categories as $category)
                        <tr>
                            <td>{{ $category->id }}</td>
                            <td>{{ $category->cate_name }}</td>
                            <td>{{ $category->prefix }}</td>
                            <td>{{ $category->alias }}</td>
                            <td>
                                <input type="checkbox" lay-skin="switch" lay-text="ON|OFF" {{ $category->status ==1 ? 'checked' : '' }} disabled>
                            <td>
                                @can('book.category.create')
                                <span class="layui-btn layui-btn-normal layui-btn-sm" onclick="openPage('{{ route('book.category.create', ['id'=> $category->id]) }}', 'add')">
                                    <i class="layui-icon"></i>添加子栏目
                                </span>
                                @endcan
                                @can('book.category.edit')
                                <span class="layui-btn layui-btn-normal layui-btn-sm" onclick="openPage('{{ route('book.category.edit', ['pId'=> 0, 'id'=> $category->id]) }}', 'edit')">
                                    <i class="layui-icon layui-icon-edit"></i>编辑
                                </span>
                                @endcan
                                @can('book.category.destroy')
                                    <span class="layui-btn layui-btn-danger layui-btn-sm" onclick="_del('{{ route('book.category.destroy', ['id'=> $category->id]) }}')">
                                        <i class="layui-icon layui-icon-delete"></i>删除
                                    </span>
                                @endcan
                            </td>
                        </tr>
                        @include('iwester::admin.book.category.category_el', ['category'=> $category, 'shendu'=> 1])
                    @empty
                        <tr>
                            <td colspan="6" style="text-align: center">暂无数据</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script>
        function openPage(data, type) {
            var openPage = layer.open({
                type: 2,
                title: type == 'add' ? '添加栏目':'编辑栏目',
                shadeClose: false,
                scrollbar: false,
                shift: 3,
                btn: ['提交', '取消'],
                shade: 0.2,
                min: true,
                area: ['700px', '500px'],
                content: [data],
                yes: function (index, layero) {
                    var dataform = $(layero).find("iframe")[0].contentWindow.formdata();
                    save(dataform, index, type);
                },
                cancel: function (index, layero) {
                    layer.close(index)
                }
            });
        }
        function save(data, layerindex, type){
            var loading = layer.load( 3, {
                shade: [0.1,'#fff']
            });
            $.ajax({
                url: type == 'add' ? '{{ route('book.category.store') }}' : '{{ route('book.category.update') }}',
                type: 'POST',
                dataType: 'json',
                data: data,
            })
                .done(function (data) {
                    if (data.code == 200) {
                        layer.alert(data.message, {
                            icon: 1,
                            closeBtn: 0,
                            shift: 3
                        }, function (index) {
                            layer.close(index);
                            location.reload();
                        });
                    }  else if (data.code == 422) {
                        layer.close(loading);
                        tip = '';
                        for (var i in data.errors) {
                            tip += data.errors[i][0] + '<br />';
                        }
                        layer.alert(tip, {icon: 2, shift: 3});
                    }else {
                        layer.close(loading);
                        layer.alert(data.message, {icon: 2, shift: 3});
                    }
                })
                .fail(function () {
                    layer.close(loading);
                    layer.alert('服务器异常', {icon: 2, shift: 3});
                });
        }
        function _del(url) {
            layer.confirm('确认删除该分类？', {
                btn: ['删除','取消'] //按钮
            }, function(){
                var loading = layer.load( 3, {
                    shade: [0.1,'#fff']
                });
                $.ajax({
                    url: url,
                    type: 'delete',
                    dataType: 'json',
                })
                    .done(function (data) {
                        if (data.code == 200) {
                            layer.alert(data.message, {
                                icon: 1,
                                closeBtn: 0,
                                shift: 3
                            }, function (index) {
                                layer.close(index);
                                location.reload();
                            });
                        } else {
                            layer.close(loading);
                            layer.alert(data.message, {icon: 2, shift: 3});
                        }
                    })
                    .fail(function () {
                        layer.close(loading);
                        layer.alert('服务器异常', {icon: 2, shift: 3});
                    });
            }, function(){

            });


        }
    </script>
@endsection


@section('script')

@endsection
