@if($category->subCate)
    @foreach($category->subCate as $subCate)
        <tr>
            <td>{{ $subCate->id }}</td>
            @php
                $nbsp = '&nbsp;&nbsp;';
                for($i=0; $i< $shendu; $i++){
                    $nbsp .= '&nbsp;&nbsp;';
                }
            @endphp
            <td>{!! $loop->last ? $nbsp.'└─ ': $nbsp.'├─ ' !!}{{ $subCate->cate_name }}</td>
            <td>{{ $subCate->prefix }}</td>
            <td>{{ $subCate->alias }}</td>
            <td><div class="layui-unselect layui-form-switch layui-form-onswitch" lay-skin="_switch"><em>{{ $subCate->status ==1 ? 'ON' : 'OFF' }}</em><i></i></div></td>
            <td>
                @can('book.category.create')
                    <span class="layui-btn layui-btn-normal layui-btn-sm" onclick="openPage('{{ route('book.category.create', ['id'=> $subCate->id]) }}', 'add')">
                        <i class="layui-icon"></i>添加子栏目
                    </span>
                @endcan
                @can('book.category.edit')
                    <span class="layui-btn layui-btn-normal layui-btn-sm" onclick="openPage('{{ route('book.category.edit', ['pId'=> $category->id,'id'=> $subCate->id]) }}', 'edit')">
                        <i class="layui-icon layui-icon-edit"></i>编辑
                    </span>
                @endcan
                @can('book.category.destroy')
                    <span class="layui-btn layui-btn-danger layui-btn-sm" onclick="_del('{{ route('book.category.destroy', ['id'=> $subCate->id]) }}')">
                                    <i class="layui-icon layui-icon-delete"></i>删除
                                </span>
                @endcan
            </td>
        </tr>
        @if($subCate->subCate)
            @include('iwester::admin.book.category.category_el', ['category'=> $subCate, 'shendu'=> $shendu+1])
        @endif
    @endforeach
@endif