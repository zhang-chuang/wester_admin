@extends('iwester::admin.base')

@section('content')
    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <h2>Banner管理
                @can('manage.banner.edit')
                <a href="{{ route('manage.banner.edit', ['pId'=> 0, 'id'=> 0 ]) }}" class="layui-btn" style="float: right">添加Banner分类</a>
                @endcan
            </h2>
        </div>
        <div class="layui-card-body">
            <div class="layui-form">
                <table class="layui-table">
                    <thead>
                    <tr>
                        <th>Banner分类</th>
                        <th>封面</th>
                        <th>跳转链接</th>
                        <th>ALT</th>
                        <th>排序</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($bannerCategories as $bannerCategory)
                        <tr>
                            <td colspan="5">{{ $bannerCategory->category_name }} （调用方式：select * from banners where batch={{ $bannerCategory->batch }}）</td>
                            <td>
                                @can('manage.banner.edit')
                                <span class="layui-btn layui-btn-normal layui-btn-sm" onclick="">
                                    <i class="layui-icon"></i><a style="color: #fff" href="{{ route('manage.banner.edit', ['batch'=> $bannerCategory->batch ]) }}">添加Banner</a>
                                </span>
                                @endcan
                                @can('manage.banner.destroy')
                                <span class="layui-btn layui-btn-danger layui-btn-sm" onclick="_del('{{ route('manage.banner.destroy', ['batch'=> $bannerCategory->batch]) }}', 1)">
                                    <i class="layui-icon layui-icon-delete"></i>删除分类
                                </span>
                                @endcan
                            </td>
                        </tr>
                        @foreach(\Iwester\Http\Model\Config\Banner::banners($bannerCategory->batch) as $banner)
                            <tr>
                                <td>{!! $loop->last ? '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─ ': '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├─ ' !!}{{ $banner->category_name }}</td>
                                <td class="img-cover" onclick="open_image($(this))">
                                    <div style="width: auto; height: 30px;cursor: pointer;">
                                        <img src="{{ $banner->cover }}" style="width: auto; height: 100%;max-width: 700px;" alt="">
                                    </div>
                                </td>
                                <td>{{ $banner->url }}</td>
                                <td>{{ $banner->alt }}</td>
                                <td>{{ $banner->sort }}</td>
                                <td>
                                    @can('manage.banner.edit')
                                    <span class="layui-btn layui-btn-normal layui-btn-sm">
                                        <i class="layui-icon layui-icon-edit"></i><a style="color: #fff" href="{{ route('manage.banner.edit', ['batch'=> $bannerCategory->batch, 'id'=> $banner->id ]) }}">编辑Banner</a>
                                    </span>
                                    @endcan
                                    @can('manage.banner.destroy')
                                    <span class="layui-btn layui-btn-danger layui-btn-sm" onclick="_del('{{ route('manage.banner.destroy', ['batch'=> $bannerCategory->batch, 'id'=> $banner->id ]) }}')">
                                        <i class="layui-icon layui-icon-delete"></i>删除Banner
                                    </span>
                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                    @empty
                        <tr>
                            <td colspan="6" style="text-align: center">暂无数据</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script>
        function _del(url, type) {
            title = type == 1 ? '删除整个分类和分类下banner?' : '确认删除当前banner?';
            layer.confirm(title, {
                btn: ['删除','取消'] //按钮
            }, function(){
                var loading = layer.load( 3, {
                    shade: [0.1,'#fff']
                });
                $.ajax({
                    url: url,
                    type: 'delete',
                    dataType: 'json',
                })
                    .done(function (data) {
                        if (data.code == 200) {
                            layer.alert(data.message, {
                                icon: 1,
                                closeBtn: 0,
                                shift: 3
                            }, function (index) {
                                layer.close(index);
                                location.reload();
                            });
                        } else {
                            layer.close(loading);
                            layer.alert(data.message, {icon: 2, shift: 3});
                        }
                    })
                    .fail(function () {
                        layer.close(loading);
                        layer.alert('服务器异常', {icon: 2, shift: 3});
                    });
            }, function(){

            });


        }

        function open_image(_this){
            layer.open({
                type: 1,
                title: false,
                closeBtn: 0,
                area: ['auto'],
                skin: 'layui-layer-nobg',
                shadeClose: true,
                content: _this.find('div').html()
            });
        }
    </script>
@endsection


@section('script')

@endsection
