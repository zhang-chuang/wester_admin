@extends('iwester::admin.base')
@section('css')
@endsection
@section('content')
    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <h2>{{ $banner ? '编辑' : '添加' }}Banner</h2>
        </div>
        <div class="layui-card-body">
            <form class="layui-form" action="{{route('manage.banner.store')}}" method="post">
                {{csrf_field()}}
                @if($banner)
                    <input type="hidden" name="id" value="{{ $banner->id }}">
                @endif
                <input type="hidden" name="banner[batch]" value="{{ $batchItem ? $batchItem->batch : time() }}">
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">Banner分类名称</label>
                    <div class="layui-input-block">
                        <input type="text"  name="banner[category_name]" {{ !$banner && $batchItem ? 'readonly' : '' }} value="{{ $batchItem  ? $batchItem->category_name : '' }}" lay-verify="required" placeholder="请输入Banner分类名称" class="layui-input" >
                        @if($batchItem && $banner)
                            <p>修改Banner分类名称 - 整个分类都会同步</p>
                        @endif
                    </div>
                </div>
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">Banner图片</label>
                    <div class="layui-input-block">
                        <input type="text" lay-verify="required" name="banner[cover]" value="{{ $banner  ? $banner->cover : '' }}" placeholder="图片地址" class="layui-input img-cover-input" >
                        <div class="layui-upload" style="position: relative">
                            <button type="button" class="layui-btn" id="uploadPic"><i class="layui-icon">&#xe67c;</i>选择图片</button>
                            <div class="layui-upload-list" >
                                <ul id="layui-upload-box" class="layui-clear">
                                    <input type="file" accept="image/*"
                                           style="position:absolute;top:0;left:0;opacity:0;width: 100%;height: 100%;z-index: 999;cursor: pointer"
                                           name="file" id="LOGO" onchange="uploadFile($(this),'LOGO')">
                                </ul>
                            </div>
                        </div>
                        <div class="img-cover" onclick="open_image($(this))" style="display: {{ $banner  ? 'block' : 'none' }}">
                            <div style="width: auto; height: 90px;cursor: pointer;display: inline-block">
                                <img src="{{ $banner  ? $banner->cover : '' }}" style="width: auto; height: 100%;max-width: 700px;" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">url</label>
                    <div class="layui-input-block">
                        <input type="text" name="banner[url]" value="{{ $banner  ? $banner->url : '' }}" lay-verify="" placeholder="请输入url" class="layui-input" >
                    </div>
                </div>
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">alt</label>
                    <div class="layui-input-block">
                        <input type="text" name="banner[alt]" value="{{ $banner  ? $banner->alt : '' }}" lay-verify="" placeholder="请输入alt" class="layui-input" >
                    </div>
                </div>
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">sort</label>
                    <div class="layui-input-block">
                        <input type="number" name="banner[sort]" value="{{ $banner  ? $banner->sort : 1 }}" lay-verify="" class="layui-input" >
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <button type="submit" class="layui-btn" lay-submit="" lay-filter="formDemo">确 认</button>
                        <a  class="layui-btn" href="javascript:;" onclick="javascript:history.back(-1);">返 回</a>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script>
        function uploadFile(obj, fileUpload) {
            var loading = layer.load( 2, {
                shade: [0.1,'#fff']
            });
            var input = obj.parents('.layui-input-block').find('input[type=text]');
            $.ajaxFileUpload({
                url: "{{ route('uploadImg') }}",
                secureuri: false,
                fileElementId: fileUpload,
                dataType: 'json',
                success: function (data) {
                    if (data.code) {
                        input.val(data.path);
                        $('.img-cover img').attr('src', data.path)
                    } else {
                        parent.layer.alert(data.msg);
                    }
                    layer.close(loading);
                },
                error: function (data) {
                    layer.close(loading);
                    parent.layer.alert('服务异常~~');
                }
            });

            return false;
        }

        $('.img-cover-input').on('change', function(){
            $(this).parents('.layui-form-item').find('.img-cover img').attr('src', $(this).val());
        });
        function open_image(_this){
            layer.open({
                type: 1,
                title: false,
                closeBtn: 0,
                area: ['auto'],
                skin: 'layui-layer-nobg',
                shadeClose: true,
                content: _this.find('div').html()
            });
        }
    </script>
@endsection


@section('script')
    <script type="text/javascript">
    </script>
@endsection
