@extends('iwester::admin.base')
@section('css')
    <style>
        /*图标展示*/
        .site-doc-icon{width: 100%;background-color: #fff}
        .site-doc-icon li{cursor:pointer;display: inline-block; vertical-align: middle; width: 19%; height: 105px; line-height: 25px; padding: 20px 0; margin-right: -1px; margin-bottom: -1px; border: 1px solid #e2e2e2; font-size: 14px; text-align: center; color: #666; transition: all .3s; -webkit-transition: all .3s;}
        .site-doc-anim li{height: auto;}
        .site-doc-icon li .layui-icon{display: inline-block; font-size: 36px;}
        .site-doc-icon li .doc-icon-name,
        .site-doc-icon li .doc-icon-code{color: #c2c2c2;}
        .site-doc-icon li .doc-icon-code xmp{margin:0}
        .site-doc-icon li .doc-icon-fontclass{height: 40px; line-height: 20px; padding: 0 5px; font-size: 13px; color: #333; }
        .site-doc-icon li:hover{background-color: #f2f2f2; color: #000;}

    </style>
@endsection
@section('content')
    <div class="layui-card">
        <div class="layui-card-body">
            <form class="layui-form" name="forms" method="post" id="forms">
                {{csrf_field()}}
                @if($curPermission)
                    <input type="hidden" name="id" value="{{ $curPermission->id }}">
                @endif
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">父级权限</label>
                    <div class="layui-input-block">
                        <input type="hidden" name="permission[parent_id]" value="{{ $parentPermission ? $parentPermission->id : 0 }}">
                        <input type="text" value="{{ $parentPermission ? $parentPermission->display_name : '顶级权限' }}" class="layui-input" disabled>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">显示名称</label>
                    <div class="layui-input-block">
                        <input type="text" name="permission[display_name]" value="{{ $curPermission ? $curPermission->display_name: '' }}" lay-verify="required" class="layui-input" placeholder="如：系统管理">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">权限名称</label>
                    <div class="layui-input-block">
                        <input type="text" name="permission[name]" value="{{ $curPermission ? $curPermission->name: '' }} " lay-verify="required" class="layui-input" placeholder="如：system.index">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">路由</label>
                    <div class="layui-input-block">
                        <input class="layui-input" type="text" name="permission[route]" value="{{ $curPermission ? $curPermission->route: '' }}" placeholder="如：admin.member" >
                    </div>
                </div>
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">排序</label>
                    <div class="layui-input-block">
                        <input class="layui-input" type="number" name="permission[sort]" value="{{ $curPermission ? $curPermission->sort: '1' }}" >
                    </div>
                </div>
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">图标</label>
                    <div class="layui-input-inline">
                        <input class="layui-input" type="hidden" name="permission[icon_id]" >
                    </div>
                    <div class="layui-form-mid layui-word-aux" id="icon_box">
                        <i class="layui-icon {{ $permission->icon->class ?? '' }}"></i> {{ $permission->icon->name ?? ''}}
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                        <button type="button" class="layui-btn layui-btn-xs" onclick="javascript:showIconsBox()">选择图标</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script src="/vendor/iwester/js/jquery.min.js"></script>
    <script>
        var formdata = function () {
            return $("#forms").serializeArray();
        }
    </script>
@endsection


@section('script')
    <script>
        //选择图标
        function chioceIcon(obj) {
            var icon_id = $(obj).data('id');
            $("input[name='permission[icon_id]']").val(icon_id);
            $("#icon_box").html('<i class="layui-icon '+$(obj).data('class')+'"></i> '+$(obj).data('name'));
            layer.closeAll();
        }

        //弹出图标
        function showIconsBox() {
            var index = layer.load();
            $.get("<?php echo e(route('admin.icons')); ?>",function (res) {
                layer.close(index);
                if (res.code==0 && res.data.length>0){
                    var html = '<ul class="site-doc-icon">';
                    $.each(res.data,function (index,item) {
                        html += '<li onclick="chioceIcon(this)" data-id="'+item.id+'" data-class="'+item.class+'" data-name="'+item.name+'" >';
                        html += '   <i class="layui-icon '+item.class+'"></i>';
                        html += '   <div class="doc-icon-name">'+item.name+'</div>';
                        html += '   <div class="doc-icon-code"><xmp>'+item.unicode+'</xmp></div>';
                        html += '   <div class="doc-icon-fontclass">'+item.class+'</div>';
                        html += '</li>'
                    });
                    html += '</ul>';
                    layer.open({
                        type:1,
                        title:'选择图标',
                        area: ['700px', '450px'],
                        content:html
                    })
                }else {
                    layer.msg(res.msg);
                }
            },'json')
        }
    </script>
@endsection
