@extends('iwester::admin.base')
@section('css')
@endsection
@section('content')
    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <h2>权限管理 <span style="font-size: 13px; color: red">会根据权限自动生成对应菜单</span>
                <span style="font-size: 13px; margin-left: 10px">父级权限：{{ $parentPermission ? $parentPermission->display_name : '顶级权限' }}</span>
                @can('manage.permission.edit')
                <span style="float: right" class="layui-btn layui-btn-sm" onclick="openPage('{{ route('manage.permission.edit', ['pId'=> $pId]) }}', 'create')">
                    添加权限
                </span>
                @endcan
                <a style="float: right;margin-right: 10px;" class="layui-btn layui-btn-sm" href="{{ route('manage.permission', ['pId'=> $parentPermission ? $parentPermission->parent_id : 0 ]) }}">返回上级</a>
            </h2>
        </div>
        <div class="layui-card-body">
            <div class="layui-form">
                <table class="layui-table">
                    <colgroup>
                        <col width="50">
                        <col width="120">
                        <col width="140">
                        <col width="60">
                        <col width="120">
                        <col width="90">
                        <col>
                    </colgroup>
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>权限名称</th>
                        <th>显示名称</th>
                        <th>排序</th>
                        <th>路由</th>
                        <th>图标</th>
                        <th>创建时间</th>
                        {{--<th>更新时间</th>--}}
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($permissions as $permission)
                        <tr>
                            <td>{{ $permission->id }}</td>
                            <td>{{ $permission->name }}</td>
                            <td>{{ $permission->display_name }}</td>
                            <td>{{ $permission->sort }}</td>
                            <td>{{ $permission->route }}</td>
                            <td>
                                @if($permission->icon)
                                    <i class="layui-icon {{ $permission->icon->class }}"></i>
                                @endif
                            </td>
                            <td>{{ $permission->created_at }}</td>
                            {{--<td>{{ $permission->updated_at }}</td>--}}
                            <td>
                                <a class="layui-btn layui-btn-normal layui-btn-sm" href="{{ route('manage.permission', ['pId'=> $permission->id]) }}">
                                    <i class="layui-icon layui-icon-util"></i>管理子权限
                                </a>
                                @can('manage.permission.edit')
                                <span class="layui-btn layui-btn-normal layui-btn-sm" onclick="openPage('{{ route('manage.permission.edit', ['pId'=> $pId, 'id'=> $permission->id]) }}', 'edit')">
                                    <i class="layui-icon layui-icon-edit"></i>编辑
                                </span>
                                @endcan
                                @can('manage.permission.destroy')
                                <span class="layui-btn layui-btn-danger layui-btn-sm" onclick="_del('{{ route('manage.permission.destroy', ['id'=> $permission->id]) }}')">
                                    <i class="layui-icon layui-icon-delete"></i>删除
                                </span>
                                @endcan
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="9" style="text-align: center">暂无数据</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script>
        function openPage(data, type) {
            var title = type ==  'create' ?  '添加权限' : '编辑权限';
            var openPage = layer.open({
                type: 2,
                title: title,
                shadeClose: false,
                scrollbar: false,
                shift: 3,
                btn: ['提交', '取消'],
                shade: 0.2,
                min: true,
                area: ['700px', '540px'],
                content: [data],
                yes: function (index, layero) {
                    var dataform = $(layero).find("iframe")[0].contentWindow.formdata();
                    save(dataform, index, type);
                },
                cancel: function (index, layero) {
                    layer.close(index)
                }
            });
        }
        function save(data, layerindex, type){
            var loading = layer.load( 3, {
                shade: [0.1,'#fff']
            });
            $.ajax({
                url: '{{ route('manage.permission.store') }}',
                type: 'POST',
                dataType: 'json',
                data: data,
            })
                .done(function (data) {
                    if (data.code == 200) {
                        layer.alert(data.message, {
                            icon: 1,
                            closeBtn: 0,
                            shift: 3
                        }, function (index) {
                            layer.close(index);
                            location.reload();
                        });
                    }else if (data.code == 422) {
                        layer.close(loading);
                        tip = '';
                        for (var i in data.errors) {
                            tip += data.errors[i][0] + '<br />';
                        }
                        layer.alert(tip, {icon: 2, shift: 3});
                    } else {
                        layer.close(loading);
                        layer.alert(data.message, {icon: 2, shift: 3});
                    }
                })
                .fail(function () {
                    layer.close(loading);
                    layer.alert('服务器异常', {icon: 2, shift: 3});
                });
        }

        function _del(url, is_delete) {
            layer.confirm('确认删除该权限？', {
                btn: ['确认','取消'] //按钮
            }, function(){
                var loading = layer.load( 3, {
                    shade: [0.1,'#fff']
                });
                $.ajax({
                    url: url,
                    type: 'delete',
                    dataType: 'json',
                })
                    .done(function (data) {
                        if (data.code == 200) {
                            layer.alert(data.message, {
                                icon: 1,
                                closeBtn: 0,
                                shift: 3
                            }, function (index) {
                                layer.close(index);
                                location.reload();
                            });
                        } else {
                            layer.close(loading);
                            layer.alert(data.message, {icon: 2, shift: 3});
                        }
                    })
                    .fail(function () {
                        layer.close(loading);
                        layer.alert('服务器异常', {icon: 2, shift: 3});
                    });
            }, function(){

            });
        }
    </script>
@endsection


@section('script')

@endsection
