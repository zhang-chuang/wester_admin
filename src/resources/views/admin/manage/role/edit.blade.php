@extends('iwester::admin.base')
@section('css')
@endsection
@section('content')
    <div class="layui-card">
        <div class="layui-card-body">
            <form class="layui-form" name="forms" method="post" id="forms">
                {{csrf_field()}}
                @if($role)
                    <input type="hidden" name="id" value="{{ $role->id }}">
                @endif
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">角色名称</label>
                    <div class="layui-input-block">
                        <input class="layui-input" type="text" name="role[name]" lay-verify="required" value="{{ $role ? $role->name: '' }}" placeholder="如:admin">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">显示名称</label>
                    <div class="layui-input-block">
                        <input class="layui-input" type="text" name="role[display_name]" lay-verify="required" value="{{ $role ? $role->display_name: '' }}" placeholder="如：管理员" >
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script src="/vendor/iwester/js/jquery.min.js"></script>
    <script>
        var formdata = function () {
            return $("#forms").serializeArray();
        }
    </script>
@endsection


@section('script')
    <script type="text/javascript">
    </script>
@endsection
