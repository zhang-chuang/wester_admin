@extends('iwester::admin.base')

@section('content')
    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <h2>角色管理
                @can('manage.role.edit')
                <span style="float: right" class="layui-btn" onclick="openPage('{{ route('manage.role.edit') }}', 'create')">
                    添加角色
                </span>
                @endcan
            </h2>
        </div>
        <div class="layui-card-body">
            <div class="layui-form">
                <table class="layui-table">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>角色</th>
                        <th>显示名称</th>
                        <th>创建时间</th>
                        <th>更新时间</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($roles as $role)
                        <tr>
                            <td>{{ $role->id }}</td>
                            <td>{{ $role->name }}</td>
                            <td>{{ $role->display_name }}</td>
                            <td>{{ $role->created_at }}</td>
                            <td>{{ $role->updated_at }}</td>
                            <td>
                                @can('manage.role.edit')
                                <span class="layui-btn layui-btn-normal layui-btn-sm" onclick="openPage('{{ route('manage.role.edit', ['id'=> $role->id]) }}', 'edit')">
                                    <i class="layui-icon layui-icon-edit"></i>编辑
                                </span>
                                @endcan
                                @can('manage.role.permission')
                                <a href="{{ route('manage.role.permission', ['id'=> $role->id ]) }}" class="layui-btn layui-btn-normal layui-btn-sm">
                                    <i class="layui-icon layui-icon-util"></i>权限
                                </a>
                                @endcan
                                @can('manage.role.destroy')
                                <span class="layui-btn layui-btn-danger layui-btn-sm" onclick="_del('{{ route('manage.role.destroy', ['id'=> $role->id]) }}')">
                                    <i class="layui-icon layui-icon-delete"></i>删除
                                </span>
                                @endcan
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="6" style="text-align: center">暂无数据</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script>
        function openPage(data, type) {
            var title = type ==  'create' ?  '添加角色' : '编辑角色';
            var openPage = layer.open({
                type: 2,
                title: title,
                shadeClose: false,
                scrollbar: false,
                shift: 3,
                btn: ['提交', '取消'],
                shade: 0.2,
                min: true,
                area: ['700px', '340px'],
                content: [data],
                yes: function (index, layero) {
                    var dataform = $(layero).find("iframe")[0].contentWindow.formdata();
                    save(dataform, index, type);
                },
                cancel: function (index, layero) {
                    layer.close(index)
                }
            });
        }
        function save(data, layerindex){
            var loading = layer.load( 3, {
                shade: [0.1,'#fff']
            });
            $.ajax({
                url: '{{ route('manage.role.store') }}',
                type: 'POST',
                dataType: 'json',
                data: data,
            })
                .done(function (data) {
                    if (data.code == 200) {
                        layer.alert(data.message, {
                            icon: 1,
                            closeBtn: 0,
                            shift: 3
                        }, function (index) {
                            layer.close(index);
                            location.reload();
                        });
                    } else if (data.code == 422) {
                        layer.close(loading);
                        tip = '';
                        for (var i in data.errors) {
                            tip += data.errors[i][0] + '<br />';
                        }
                        layer.alert(tip, {icon: 2, shift: 3});
                    } else {
                        layer.close(loading);
                        layer.alert(data.message, {icon: 2, shift: 3});
                    }
                })
                .fail(function () {
                    layer.close(loading);
                    layer.alert('服务器异常', {icon: 2, shift: 3});
                });
        }
        function _del(url) {
            layer.confirm('确认删除当前角色?', {
                btn: ['删除','取消'] //按钮
            }, function(){
                var loading = layer.load( 3, {
                    shade: [0.1,'#fff']
                });
                $.ajax({
                    url: url,
                    type: 'delete',
                    dataType: 'json',
                })
                    .done(function (data) {
                        if (data.code == 200) {
                            layer.alert(data.message, {
                                icon: 1,
                                closeBtn: 0,
                                shift: 3
                            }, function (index) {
                                layer.close(index);
                                location.reload();
                            });
                        } else {
                            layer.close(loading);
                            layer.alert(data.message, {icon: 2, shift: 3});
                        }
                    })
                    .fail(function () {
                        layer.close(loading);
                        layer.alert('服务器异常', {icon: 2, shift: 3});
                    });
            }, function(){

            });
        }
    </script>
@endsection


@section('script')

@endsection
