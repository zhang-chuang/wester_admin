@extends('iwester::admin.base')

@section('content')
    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <h2>网站设置</h2>
        </div>
        <div class="layui-card-body">
            <form class="layui-form" action="{{route('manage.siteconfig.store')}}" method="post">
                {{csrf_field()}}
                <div class="layui-tab layui-tab-brief" lay-filter="docDemoTabBrief">
                    <ul class="layui-tab-title">
                        <li class="layui-this">基础设置</li>
                        <li>SEO配置</li>
                        <li>文章配置</li>
                        <li>书籍模型配置</li>
                    </ul>
                    <div class="layui-tab-content">
                        <div class="layui-tab-item layui-show">
                            <div class="layui-form-item">
                                <label for="" class="layui-form-label">网站名称</label>
                                <div class="layui-input-block">
                                    <input type="text" name="site[site_name]" value="{{ $siteConfig->site_name??old('site[site_name]') }}" lay-verify="required" placeholder="请输入站点名称" class="layui-input" >
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label for="" class="layui-form-label">网站URL</label>
                                <div class="layui-input-block">
                                    <input type="text" name="site[site_url]" value="{{ $siteConfig->site_url??old('site[site_url]') }}" lay-verify="required" placeholder="请输入网站URL" class="layui-input" >
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label for="" class="layui-form-label">站长邮箱</label>
                                <div class="layui-input-block">
                                    <input type="text" name="site[email]" value="{{ $siteConfig->email??old('site[email]') }}" lay-verify="required" placeholder="请输入站长邮箱" class="layui-input" >
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label for="" class="layui-form-label">网站版权</label>
                                <div class="layui-input-block">
                                    <input type="text" name="site[site_copyright]" value="{{ $siteConfig->site_copyright??old('site[site_copyright]') }}" lay-verify="required" placeholder="请输入网站版权" class="layui-input" >
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label for="" class="layui-form-label">敏感词</label>
                                <div class="layui-input-block">
                                    <input type="text" name="site[sensitive_word]" placeholder="敏感词|分割" value="{{ $siteConfig->sensitive_word??old('site[sensitive_word]') }}" lay-verify="required" class="layui-input">
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label for="" class="layui-form-label">网站LOGO</label>
                                <div class="layui-input-block">
                                    <input type="text" name="site[site_logo]" value="{{ $siteConfig->site_logo??old('site[site_logo]') }}" lay-verify="required" placeholder="图片地址" class="layui-input" >
                                    <div class="layui-upload" style="position: relative">
                                        <button type="button" class="layui-btn" id="uploadPic"><i class="layui-icon">&#xe67c;</i>选择图片</button>
                                        <div class="layui-upload-list" >
                                            <ul id="layui-upload-box" class="layui-clear">
                                                <input type="file" accept="image/*"
                                                       style="position:absolute;top:0;left:0;opacity:0;width: 100%;height: 100%;z-index: 999;cursor: pointer"
                                                       name="file" id="LOGO" onchange="uploadFile($(this),'LOGO')">
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label for="" class="layui-form-label">网站ICO</label>
                                <div class="layui-input-block">
                                    <input type="text" name="site[site_ico]" value="{{ $siteConfig->site_ico??old('site[site_ico]') }}" lay-verify="" placeholder="图片地址" class="layui-input" >
                                    <div class="layui-upload" style="position: relative">
                                        <button type="button" class="layui-btn" id="uploadPic"><i class="layui-icon">&#xe67c;</i>选择图片</button>
                                        <div class="layui-upload-list" >
                                            <ul id="layui-upload-box" class="layui-clear">
                                                <input type="file" accept="image/*"
                                                       style="position:absolute;top:0;left:0;opacity:0;width: 100%;height: 100%;z-index: 999;cursor: pointer"
                                                       name="file" id="LOGO" onchange="uploadFile($(this),'LOGO')">
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label">前台登录</label>
                                <div class="layui-input-block">
                                    <input type="radio" name="site[is_login]" value="1" title="开启" {{ $siteConfig->is_login == 1 ? 'checked' : '' }}>
                                    <input type="radio" name="site[is_login]" value="0" title="关闭" {{ $siteConfig->is_login == 0 ? 'checked' : '' }}>
                                </div>
                            </div>

                        </div>
                        <div class="layui-tab-item">
                            <div class="layui-form-item">
                                <label for="" class="layui-form-label">网站title</label>
                                <div class="layui-input-block">
                                    <input type="text" name="site[site_title]" value="{{ $siteConfig->site_title??old('site[site_title]') }}" lay-verify="required" placeholder="请输入title" class="layui-input" >
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label for="" class="layui-form-label">网站keyword</label>
                                <div class="layui-input-block">
                                    <input type="text" name="site[site_keyword]" value="{{ $siteConfig->site_keyword??old('site[site_keyword]') }}" lay-verify="required" placeholder="请输入keyword" class="layui-input" >
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label for="" class="layui-form-label">网站description</label>
                                <div class="layui-input-block">
                                    <input type="text" name="site[site_description]" value="{{ $siteConfig->site_description??old('site[site_description]') }}" lay-verify="required" placeholder="请输入description" class="layui-input" >
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label for="" class="layui-form-label">统计代码</label>
                                <div class="layui-input-block">
                                    <textarea name="site[site_script]" placeholder="请输入统计代码" class="layui-textarea">{{ $siteConfig->site_script??old('site[site_script]') }}</textarea>
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label for="" class="layui-form-label">sitemap提交</label>
                                <div class="layui-input-block">
                                    <input type="text" name="site[sitemap_api]" placeholder="sitemap百度提交地址" value="{{ $siteConfig->sitemap_api??old('site[sitemap_api]') }}"  class="layui-input">
                                </div>
                            </div>

                        </div>
                        <div class="layui-tab-item">
                            <div class="layui-form-item">
                                <label class="layui-form-label">文章审核</label>
                                <div class="layui-input-block">
                                    <input type="radio" name="site[on_article_audit]" value="1" title="开启" {{ $siteConfig->on_article_audit == 1 ? 'checked' : '' }}>
                                    <input type="radio" name="site[on_article_audit]" value="0" title="关闭" {{ $siteConfig->on_article_audit == 0 ? 'checked' : '' }}>
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label">文章点赞</label>
                                <div class="layui-input-block">
                                    <input type="radio" name="site[on_article_like]" value="1" title="开启" {{ $siteConfig->on_article_like == 1 ? 'checked' : '' }}>
                                    <input type="radio" name="site[on_article_like]" value="0" title="关闭" {{ $siteConfig->on_article_like == 0 ? 'checked' : '' }}>
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label">文章评论</label>
                                <div class="layui-input-block">
                                    <input type="radio" name="site[on_article_comment]" value="1" title="开启" {{ $siteConfig->on_article_comment == 1 ? 'checked' : '' }}>
                                    <input type="radio" name="site[on_article_comment]" value="0" title="关闭" {{ $siteConfig->on_article_comment == 0 ? 'checked' : '' }}>
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label">评论点赞</label>
                                <div class="layui-input-block">
                                    <input type="radio" name="site[on_article_comment_like]" value="1" title="开启" {{ $siteConfig->on_article_comment_like == 1 ? 'checked' : '' }}>
                                    <input type="radio" name="site[on_article_comment_like]" value="0" title="关闭" {{ $siteConfig->on_article_comment_like == 0 ? 'checked' : '' }}>
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label">评论审核</label>
                                <div class="layui-input-block">
                                    <input type="radio" name="site[on_article_comment_audit]" value="1" title="开启" {{ $siteConfig->on_article_comment_audit == 1 ? 'checked' : '' }}>
                                    <input type="radio" name="site[on_article_comment_audit]" value="0" title="关闭" {{ $siteConfig->on_article_comment_audit == 0 ? 'checked' : '' }}>
                                </div>
                            </div>
                        </div>
                        <div class="layui-tab-item">
                            <div class="layui-form-item">
                                <label class="layui-form-label">书籍模型</label>
                                <div class="layui-input-block">
                                    <input type="radio" name="site[on_book_model]" value="1" title="开启" {{ $siteConfig->on_book_model == 1 ? 'checked' : '' }}>
                                    <input type="radio" name="site[on_book_model]" value="0" title="关闭" {{ $siteConfig->on_book_model == 0 ? 'checked' : '' }}>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <button type="submit" class="layui-btn" lay-submit="" lay-filter="formDemo">确 认</button>
                        <a  class="layui-btn" href="" >返 回</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection


@section('script')
    <style>
        #layui-upload-box li{
            width: 120px;
            height: 100px;
            float: left;
            position: relative;
            overflow: hidden;
            margin-right: 10px;
            border:1px solid #ddd;
        }
        #layui-upload-box li img{
            width: 100%;
        }
        #layui-upload-box li p{
            width: 100%;
            height: 22px;
            font-size: 12px;
            position: absolute;
            left: 0;
            bottom: 0;
            line-height: 22px;
            text-align: center;
            color: #fff;
            background-color: #333;
            opacity: 0.6;
        }
        #layui-upload-box li i{
            display: block;
            width: 20px;
            height:20px;
            position: absolute;
            text-align: center;
            top: 2px;
            right:2px;
            z-index:999;
            cursor: pointer;
        }
    </style>

    <script>
        function uploadFile(obj, fileUpload) {
            var loading = layer.load( 2, {
                shade: [0.1,'#fff']
            });
            var input = obj.parents('.layui-input-block').find('input[type=text]');
            $.ajaxFileUpload({
                url: "{{ route('uploadImg') }}",
                secureuri: false,
                fileElementId: fileUpload,
                dataType: 'json',
                success: function (data) {
                    if (data.code) {
                        input.val(data.path);
                    } else {
                        parent.layer.alert(data.msg);
                    }
                    layer.close(loading);
                },
                error: function (data) {
                    layer.close(loading);
                    parent.layer.alert('服务异常~~');
                }
            });

            return false;
        }
    </script>
@endsection
