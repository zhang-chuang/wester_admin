@extends('iwester::admin.base')
@section('css')
@endsection
@section('content')
    <div class="layui-card">
        <div class="layui-card-body">
            <form class="layui-form" name="forms" method="post" id="forms">
                {{csrf_field()}}
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">用户名</label>
                    <div class="layui-input-block">
                        <input type="text" name="user[username]" value="" required lay-verify="required" placeholder="请输入用户名" class="layui-input" >
                    </div>
                </div>

                <div class="layui-form-item">
                    <label for="" class="layui-form-label">昵称</label>
                    <div class="layui-input-block">
                        <input type="text" name="user[name]" value="" required lay-verify="required" placeholder="请输入昵称" class="layui-input" >
                    </div>
                </div>

                <div class="layui-form-item">
                    <label for="" class="layui-form-label">邮箱</label>
                    <div class="layui-input-block">
                        <input type="email" name="user[email]" value="" required lay-verify="email" placeholder="请输入Email" class="layui-input" >
                    </div>
                </div>

                <div class="layui-form-item">
                    <label for="" class="layui-form-label">手机号</label>
                    <div class="layui-input-block">
                        <input type="text" name="user[phone]" value="" required="phone" lay-verify="phone" placeholder="请输入手机号" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">密码</label>
                    <div class="layui-input-block">
                        <input type="text" name="user[password]" required lay-verify="required" placeholder="请输入密码" class="layui-input">
                    </div>
                </div>
                {{--<div class="layui-form-item">--}}
                    {{--<label for="" class="layui-form-label">确认密码</label>--}}
                    {{--<div class="layui-input-block">--}}
                        {{--<input type="text" name="user[password_confirmation]" required lay-verify="required" placeholder="请确认密码" class="layui-input">--}}
                    {{--</div>--}}
                {{--</div>--}}
            </form>
        </div>
    </div>
    <script src="/vendor/iwester/js/jquery.min.js"></script>
    <script>
        var formdata = function () {
            return $("#forms").serializeArray();
        }
    </script>
@endsection


@section('script')
@endsection
