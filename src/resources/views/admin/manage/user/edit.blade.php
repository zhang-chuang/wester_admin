@extends('iwester::admin.base')
@section('css')
@endsection
@section('content')
    <div class="layui-card">
        <div class="layui-card-body">
            <form class="layui-form" name="forms" method="post" id="forms">
                {{csrf_field()}}
                <input type="hidden" name="id" value="{{ $user->id }}">
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">用户名</label>
                    <div class="layui-input-block">
                        <input type="text" name="user[username]" value="{{ $user->username }}" lay-verify="required" placeholder="请输入用户名" class="layui-input" >
                    </div>
                </div>

                <div class="layui-form-item">
                    <label for="" class="layui-form-label">昵称</label>
                    <div class="layui-input-block">
                        <input type="text" name="user[name]" value="{{ $user->name }}" lay-verify="required" placeholder="请输入昵称" class="layui-input" >
                    </div>
                </div>

                <div class="layui-form-item">
                    <label for="" class="layui-form-label">邮箱</label>
                    <div class="layui-input-block">
                        <input type="email" name="user[email]" value="{{ $user->email }}" lay-verify="email" placeholder="请输入Email" class="layui-input" >
                    </div>
                </div>
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">手机号</label>
                    <div class="layui-input-block">
                        <input type="text" name="user[phone]" value="{{ $user->phone }}" required="phone" lay-verify="phone" placeholder="请输入手机号" class="layui-input">
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script src="/vendor/iwester/js/jquery.min.js"></script>
    <script>
        var formdata = function () {
            return $("#forms").serializeArray();
        }
    </script>
@endsection


@section('script')
    <script type="text/javascript">
    </script>
@endsection
