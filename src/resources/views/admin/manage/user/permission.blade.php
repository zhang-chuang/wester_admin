@extends('iwester::admin.base')
@section('css')
@endsection

@section('content')
    <style>
        .cate-box{margin-bottom: 15px;padding-bottom:10px;border-bottom: 1px solid #f0f0f0}
        .cate-box dt{margin-bottom: 10px;}
        .cate-box dt .cate-first{padding:10px 20px}
        .cate-box dd{padding:0 50px}
        .cate-box dd .cate-second{margin-bottom: 10px}
        .cate-box dd .cate-third{padding:0 40px;margin-bottom: 10px}
    </style>
    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <h2>用户 【{{ $user->username }}】分配直接权限，直接权限与角色拥有的角色权限不冲突(不可编辑部分为角色权限)</h2>
        </div>
        <div class="layui-card-body">
            <form action="{{ route('manage.user.permission_store',['id'=>$user->id]) }}" method="post" class="layui-form">
                {{csrf_field()}}
                @forelse($permissions as $first)
                    <dl class="cate-box">
                        <dt>
                            <div class="cate-first">
                                <input id="menu{{ $first->id }}" type="checkbox" name="permissions[]" value="{{ $first->id }}"
                                       title="{{ $first->display_name }}" lay-skin="primary"
                                        {{ $user->hasDirectPermission($first->name) ? 'checked' : '' }}
                                        {{ $permissionsViaRoles->search($first->name) !== false ?  'disabled checked' : '' }}
                                >
                            </div>
                        </dt>
                        @if($first->subPermission)
                            @foreach($first->subPermission as $second)
                                <dd>
                                    <div class="cate-second">
                                        <input id="menu{{ $first->id }}-{{ $second->id }}" type="checkbox" name="permissions[]" value="{{ $second->id }}" 
                                               title="{{ $second->display_name }}" lay-skin="primary"
                                                {{ $user->hasDirectPermission($second->name) ? 'checked' : '' }}
                                                {{ $permissionsViaRoles->search($second->name) !== false ?  'disabled checked' : '' }}
                                        ></div>
                                    @if($second->subPermission)
                                        <div class="cate-third">
                                            @foreach($second->subPermission as $thild)
                                                <input type="checkbox" id="menu{{ $first->id }}-{{ $second->id }}-{{ $thild->id }}"
                                                       name="permissions[]" value="{{ $thild->id }}" title="{{ $thild->display_name }}" lay-skin="primary"
                                                        {{ $user->hasDirectPermission($thild->name) ? 'checked' : '' }}
                                                        {{ $permissionsViaRoles->search($thild->name) !== false ?  'disabled checked' : '' }}
                                                >
                                                @if($thild->subPermission)
                                                    @foreach($thild->subPermission as $four)
                                                        <input type="checkbox" id="menu{{ $first->id }}-{{ $second->id }}-{{ $thild->id }}-{{ $four->id }}"
                                                               name="permissions[]" value="{{ $four->id }}" title="{{ $four->display_name }}" lay-skin="primary"
                                                                {{ $user->hasDirectPermission($four->name) ? 'checked' : '' }}
                                                                {{ $permissionsViaRoles->search($four->name) !== false ?  'disabled checked' : '' }}
                                                        >
                                                    @endforeach
                                                @endif
                                            @endforeach
                                        </div>
                                    @endif
                                </dd>
                            @endforeach
                        @endif
                    </dl>
                @empty
                    <div style="text-align: center;padding:20px 0;">
                        无数据
                    </div>
                @endforelse
                <div class="layui-form-item">
                    <button type="submit" class="layui-btn" lay-submit="" >确 认</button>
                    <a href="{{route('manage.user')}}"  class="layui-btn" >返 回</a>
                </div>

            </form>
        </div>
    </div>
@endsection


@section('script')
    <script type="text/javascript">
        layui.use(['layer','table','form'],function () {
            var layer = layui.layer;
            var form = layui.form;
            var table = layui.table;

            form.on('checkbox', function (data) {
                var check = data.elem.checked;//是否选中
                var checkId = data.elem.id;//当前操作的选项框
                if (check) {
                    //选中
                    var ids = checkId.split("-");
                    if (ids.length == 3) {
                        //第三极菜单
                        //第三极菜单选中,则他的上级选中
                        $("#" + (ids[0] + '-' + ids[1])).prop("checked", true);
                        $("#" + (ids[0])).prop("checked", true);
                    } else if (ids.length == 2) {
                        //第二季菜单
                        $("#" + (ids[0])).prop("checked", true);
                        $("input[id*=" + ids[0] + '-' + ids[1] + "]").each(function (i, ele) {
                            $(ele).prop("checked", true);
                        });
                    } else {
                        //第一季菜单不需要做处理
                        $("input[id*=" + ids[0] + "-]").each(function (i, ele) {
                            $(ele).prop("checked", true);
                        });
                    }
                } else {
                    //取消选中
                    var ids = checkId.split("-");
                    if (ids.length == 2) {
                        //第二极菜单
                        $("input[id*=" + ids[0] + '-' + ids[1] + "]").each(function (i, ele) {
                            $(ele).prop("checked", false);
                        });
                    } else if (ids.length == 1) {
                        $("input[id*=" + ids[0] + "-]").each(function (i, ele) {
                            $(ele).prop("checked", false);
                        });
                    }
                }
                form.render();
            });
        })
    </script>
@endsection
