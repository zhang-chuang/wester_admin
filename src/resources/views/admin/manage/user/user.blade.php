@extends('iwester::admin.base')
@section('css')
    <style>
        .layui-laypage li, .layui-laypage li span {
            display: inline-block;
            *display: inline;
            *zoom: 1;
            vertical-align: middle;
            height: 28px;
            line-height: 28px;
            background-color: #fff;
            color: #333;
            font-size: 12px;
        }
        .layui-laypage li.active span{
            background-color: #009688;
            color: #fff;
        }
    </style>
@endsection
@section('content')
    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <h2>用户管理
                @can('manage.user.create')
                <span style="float: right" class="layui-btn" onclick="openPage('{{ route('manage.user.create') }}', 'create')">
                    添加用户
                </span>
                @endcan
            </h2>
        </div>
        <div class="layui-card-body">
            <div class="layui-form">
                <table class="layui-table">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>用户名</th>
                        <th>昵称</th>
                        <th>邮箱</th>
                        <th>电话</th>
                        <th>用户状态</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($users as $user)
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->username }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->phone }}</td>
                            <td>{{ $user->is_delete == 1 ? '被冻结' : '正常' }}</td>
                            <td>
                                @can('manage.user.edit')
                                <span class="layui-btn layui-btn-normal layui-btn-sm" onclick="openPage('{{ route('manage.user.edit', ['id'=> $user->id]) }}', 'edit')">
                                    <i class="layui-icon layui-icon-edit"></i>编辑
                                </span>
                                @endcan
                                @can('manage.user.role')
                                <a href="{{ route('manage.user.role', ['id'=> $user->id ]) }}" class="layui-btn layui-btn-normal layui-btn-sm">
                                    <i class="layui-icon layui-icon-util"></i>角色
                                </a>
                                @endcan
                                @can('manage.user.permission')
                                <a href="{{ route('manage.user.permission', ['id'=> $user->id ]) }}" class="layui-btn layui-btn-normal layui-btn-sm">
                                    <i class="layui-icon layui-icon-util"></i>权限
                                </a>
                                @endcan
                                @can('manage.user.change')
                                    @if($user->is_delete == 1)
                                        <span class="layui-btn layui-btn-sm" onclick="_del('{{ route('manage.user.change', ['id'=> $user->id]) }}', 0)">
                                            <i class="layui-icon layui-icon-edit"></i>解冻
                                        </span>
                                    @else
                                        <span class="layui-btn layui-btn-danger layui-btn-sm" onclick="_del('{{ route('manage.user.change', ['id'=> $user->id]) }}', 1)">
                                            <i class="layui-icon layui-icon-delete"></i>冻结
                                        </span>
                                    @endif
                                @endcan
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="7" style="text-align: center">暂无数据</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
                <div class="layui-box layui-laypage layui-laypage-default" id="">
                    {{ $users->render() }}
                </div>
            </div>
        </div>
    </div>
    <script>
        function openPage(data, type) {
            var title = type ==  'create' ?  '添加用户' : '编辑用户';
            var openPage = layer.open({
                type: 2,
                title: title,
                shadeClose: false,
                scrollbar: false,
                shift: 3,
                btn: ['提交', '取消'],
                shade: 0.2,
                min: true,
                area: ['700px', '440px'],
                content: [data],
                yes: function (index, layero) {
                    var dataform = $(layero).find("iframe")[0].contentWindow.formdata();
                    save(dataform, index, type);
                },
                cancel: function (index, layero) {
                    layer.close(index)
                }
            });
        }
        function save(data, layerindex, type){
            var loading = layer.load( 3, {
                shade: [0.1,'#fff']
            });
            var url = type == 'create' ? '{{ route('manage.user.store') }}' : '{{ route('manage.user.update') }}'
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: data,
            })
                .done(function (data) {
                    if (data.code == 200) {
                        layer.alert(data.message, {
                            icon: 1,
                            closeBtn: 0,
                            shift: 3
                        }, function (index) {
                            layer.close(index);
                            location.reload();
                        });
                    } else if (data.code == 422) {
                        layer.close(loading);
                        tip = '';
                        for (var i in data.errors) {
                            tip += data.errors[i][0] + '<br />';
                        }
                        layer.alert(tip, {icon: 2, shift: 3});
                    }else {
                        layer.close(loading);
                        layer.alert(data.message, {icon: 2, shift: 3});
                    }
                })
                .fail(function () {
                    layer.close(loading);
                    layer.alert('服务器异常', {icon: 2, shift: 3});
                });
        }
        function _del(url, is_delete) {
            var title = is_delete== 1 ? '确认冻结当前用户?' : '确认恢复解冻?';
            layer.confirm(title, {
                btn: ['确认','取消'] //按钮
            }, function(){
                var loading = layer.load( 3, {
                    shade: [0.1,'#fff']
                });
                $.ajax({
                    url: url,
                    type: 'post',
                    dataType: 'json',
                    data: {
                        'is_delete': is_delete
                    }
                })
                    .done(function (data) {
                        if (data.code == 200) {
                            layer.alert(data.message, {
                                icon: 1,
                                closeBtn: 0,
                                shift: 3
                            }, function (index) {
                                layer.close(index);
                                location.reload();
                            });
                        } else {
                            layer.close(loading);
                            layer.alert(data.message, {icon: 2, shift: 3});
                        }
                    })
                    .fail(function () {
                        layer.close(loading);
                        layer.alert('服务器异常', {icon: 2, shift: 3});
                    });
            }, function(){

            });
        }
    </script>
@endsection


@section('script')

@endsection
