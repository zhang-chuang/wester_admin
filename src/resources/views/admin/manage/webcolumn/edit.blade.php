@extends('iwester::admin.base')
@section('css')
@endsection
@section('content')
    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <h2>{{ $webcolumn ? '编辑' : '添加' }}WEB栏目
                @if($parentColumn)
                    <span style="color: red;font-size: 14px;margin-left: 10px;">父级：{{ $parentColumn->name }}</span>
                @endif
            </h2>
        </div>
        <div class="layui-card-body">
            <div class="layui-tab">
                <ul class="layui-tab-title">
                    @foreach(\Iwester\Http\Model\Config\SiteWebColumn::$types as $k=>$value)
                        <li class="{{ $type == $k ? 'layui-this' : '' }}">{{ $value }}</li>
                    @endforeach
                </ul>
                <div class="layui-tab-content">
                    {{--URL--}}
                    <div class="layui-tab-item {{ $type == 1 ?  'layui-show' : ''}}">
                        <form class="layui-form form-item" action="{{route('manage.web_column.store')}}" method="post">
                            {{csrf_field()}}
                            @if($webcolumn)
                                <input type="hidden" name="id" value="{{ $webcolumn->id }}">
                            @endif
                            <input type="hidden" name="params[type]" value="1">
                            <input type="hidden" name="params[parent_id]" value="{{ $parentId }}">
                            <div class="layui-form-item">
                                <label for="" class="layui-form-label">栏目名称</label>
                                <div class="layui-input-block">
                                    <input type="text"  name="params[name]" value="{{ $webcolumn  ? $webcolumn->name : '' }}" lay-verify="required" placeholder="请输入WEB栏目名称" class="layui-input" >
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label for="" class="layui-form-label">跳转URL</label>
                                <div class="layui-input-block">
                                    <input type="text"  name="params[url]" value="{{ $webcolumn  ? $webcolumn->url : '' }}" lay-verify="required" placeholder="请输入URL" class="layui-input" >
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label for="" class="layui-form-label">sort</label>
                                <div class="layui-input-block">
                                    <input type="number" name="params[sort]" value="{{ $webcolumn  ? $webcolumn->sort : 1 }}" lay-verify="" class="layui-input" >
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <div class="layui-input-block">
                                    <button type="submit" class="layui-btn" lay-submit="" lay-filter="formDemo">确 认</button>
                                    <a  class="layui-btn" href="javascript:;" onclick="javascript:history.back(-1);">返 回</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    {{--单页面指定模板--}}
                    <div class="layui-tab-item {{ $type == 2 ?  'layui-show' : ''}}">
                        <form class="layui-form form-item" action="{{route('manage.web_column.store')}}" method="post">
                            {{csrf_field()}}
                            @if($webcolumn)
                                <input type="hidden" name="id" value="{{ $webcolumn->id }}">
                            @endif
                            <input type="hidden" name="params[type]" value="2">
                            <input type="hidden" name="params[parent_id]" value="{{ $parentId }}">
                            <div class="layui-form-item">
                                <label for="" class="layui-form-label">栏目名称</label>
                                <div class="layui-input-block">
                                    <input type="text"  name="params[name]" value="{{ $webcolumn  ? $webcolumn->name : '' }}" lay-verify="required" placeholder="请输入WEB栏目名称" class="layui-input" >
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label for="" class="layui-form-label">路由URL</label>
                                <div class="layui-input-block">
                                    <input type="text"  name="params[url]" value="{{ $webcolumn  ? $webcolumn->url : '' }}" lay-verify="required" placeholder="请输入路由URL如：info.html" class="layui-input" >
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label for="" class="layui-form-label">模板路径</label>
                                <div class="layui-input-block">
                                    <input type="text"  name="params[blade_template]" value="{{ $webcolumn  ? $webcolumn->blade_template : '' }}" lay-verify="required" placeholder="请输入模板路径 view.blade" class="layui-input" >
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label for="" class="layui-form-label">sort</label>
                                <div class="layui-input-block">
                                    <input type="number" name="params[sort]" value="{{ $webcolumn  ? $webcolumn->sort : 1 }}" lay-verify="" class="layui-input" >
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <div class="layui-input-block">
                                    <button type="submit" class="layui-btn" lay-submit="" lay-filter="formDemo">确 认</button>
                                    <a  class="layui-btn" href="javascript:;" onclick="javascript:history.back(-1);">返 回</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    {{--使用文章分类--}}
                    <div class="layui-tab-item {{ $type == 3 ?  'layui-show' : ''}}">
                        <form class="layui-form form-item" action="{{route('manage.web_column.store')}}" method="post">
                            {{csrf_field()}}
                            @if($webcolumn)
                                <input type="hidden" name="id" value="{{ $webcolumn->id }}">
                            @endif
                            <input type="hidden" name="params[type]" value="3">
                            <input type="hidden" name="params[parent_id]" value="{{ $parentId }}">
                            <div class="layui-form-item">
                                <label for="" class="layui-form-label">栏目名称</label>
                                <div class="layui-input-block">
                                    <input type="text"  name="params[name]" value="{{ $webcolumn  ? $webcolumn->name : '' }}" lay-verify="required" placeholder="请输入WEB栏目名称" class="layui-input" >
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label for="" class="layui-form-label">分类ID</label>
                                <div class="layui-input-block">
                                    <input type="text"  name="params[params]" value="{{ $webcolumn  ? $webcolumn->params : '{"id":1}' }}" lay-verify="required" class="layui-input" >
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label for="" class="layui-form-label">sort</label>
                                <div class="layui-input-block">
                                    <input type="number" name="params[sort]" value="{{ $webcolumn  ? $webcolumn->sort : 1 }}" lay-verify="" class="layui-input" >
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <div class="layui-input-block">
                                    <button type="submit" class="layui-btn" lay-submit="" lay-filter="formDemo">确 认</button>
                                    <a  class="layui-btn" href="javascript:;" onclick="javascript:history.back(-1);">返 回</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

    </script>
@endsection


@section('script')
    <script type="text/javascript">
    </script>
@endsection
