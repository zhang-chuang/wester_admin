@extends('iwester::admin.base')

@section('content')
    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <h2>WEB栏目管理
                @can('manage.web_column.edit')
                <a href="{{ route('manage.web_column.edit') }}" class="layui-btn" style="float: right">添加WEB栏目</a>
                @endcan
            </h2>
        </div>
        <div class="layui-card-body">
            <div class="layui-form">
                <table class="layui-table">
                    <thead>
                    <tr>
                        <th>栏目名称</th>
                        <th>跳转链接</th>
                        <th>类型</th>
                        <th>排序</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($webcolumns as $webcolumn)
                        <tr>
                            <td>{{ $webcolumn->name }}</td>
                            <td>
                                @if($webcolumn->params)
                                    /{{ $webcolumn->articleCate() ?  $webcolumn->articleCate()->alias : '--'}}
                                @else
                                    {{ $webcolumn->url }}{{ $webcolumn->blade_template ?  ' -- '.$webcolumn->blade_template : ''}}
                                @endif
                            </td>
                            <td>{{ \Iwester\Http\Model\Config\SiteWebColumn::$types[$webcolumn->type] }}</td>
                            <td>{{ $webcolumn->sort }}</td>
                            <td>
                                @can('manage.web_column.edit')
                                <span class="layui-btn layui-btn-normal layui-btn-sm" >
                                    <i class="layui-icon layui-icon-edit"></i>
                                    <a style="color: #fff" href="{{ route('manage.web_column.edit', ['id'=> $webcolumn->id ]) }}">编辑</a>
                                </span>
                                    @if($webcolumn->type != 3)
                                        <span class="layui-btn layui-btn-normal layui-btn-sm" onclick="">
                                            <i class="layui-icon"></i><a style="color: #fff" href="{{ route('manage.web_column.edit') }}?parent_id={{ $webcolumn->id }}">添加子栏目</a>
                                        </span>
                                    @endif
                                @endcan
                                @can('manage.web_column.destroy')
                                <span class="layui-btn layui-btn-danger layui-btn-sm" onclick="_del('{{ route('manage.web_column.destroy', ['id'=> $webcolumn->id]) }}', 1)">
                                    <i class="layui-icon layui-icon-delete"></i>删除栏目
                                </span>
                                @endcan
                            </td>
                        </tr>
                        @if($webcolumn->subColumn)
                            @foreach($webcolumn->subColumn as $subWebcolumn)
                                <tr>
                                    @php
                                        $nbsp = '&nbsp;&nbsp;';
                                    @endphp
                                    <td>{!! $loop->last ? $nbsp.'└─ ': $nbsp.'├─ ' !!}{{ $subWebcolumn->cate_name }}</td>
                                    <td>
                                        @if($subWebcolumn->params)
                                            {{ $subWebcolumn->articleCate() ?  route('article.list', ['cate'=> $subWebcolumn->articleCate()->alias]) : '--'}}
                                        @else
                                            {{ $subWebcolumn->url }}{{ $subWebcolumn->blade_template ?  ' -- '.$subWebcolumn->blade_template : ''}}
                                        @endif
                                    </td>
                                    <td>{{ \Iwester\Http\Model\Config\SiteWebColumn::$types[$subWebcolumn->type] }}</td>
                                    <td>{{ $subWebcolumn->sort }}</td>
                                    <td>
                                        @can('manage.web_column.edit')
                                            <span class="layui-btn layui-btn-normal layui-btn-sm" >
                                                <i class="layui-icon layui-icon-edit"></i>
                                                <a style="color: #fff" href="{{ route('manage.web_column.edit', ['id'=> $subWebcolumn->id ]) }}?parent_id={{ $webcolumn->id }}">编辑</a>
                                            </span>
                                        @endcan
                                        @can('manage.web_column.destroy')
                                            <span class="layui-btn layui-btn-danger layui-btn-sm" onclick="_del('{{ route('manage.web_column.destroy', ['id'=> $subWebcolumn->id]) }}', 1)">
                                                <i class="layui-icon layui-icon-delete"></i>删除栏目
                                            </span>
                                        @endcan
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    @empty
                        <tr>
                            <td colspan="6" style="text-align: center">暂无数据</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script>
        function _del(url) {
            layer.confirm('确认删除该栏目？', {
                btn: ['删除','取消'] //按钮
            }, function(){
                var loading = layer.load( 3, {
                    shade: [0.1,'#fff']
                });
                $.ajax({
                    url: url,
                    type: 'delete',
                    dataType: 'json',
                })
                    .done(function (data) {
                        if (data.code == 200) {
                            layer.alert(data.message, {
                                icon: 1,
                                closeBtn: 0,
                                shift: 3
                            }, function (index) {
                                layer.close(index);
                                location.reload();
                            });
                        } else {
                            layer.close(loading);
                            layer.alert(data.message, {icon: 2, shift: 3});
                        }
                    })
                    .fail(function () {
                        layer.close(loading);
                        layer.alert('服务器异常', {icon: 2, shift: 3});
                    });
            }, function(){

            });


        }
    </script>
@endsection


@section('script')

@endsection
