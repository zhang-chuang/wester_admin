@extends('iwester::admin.base')
@section('css')
    <link rel="stylesheet" href="/vendor/iwester/css/personal_center.css">
    <link rel="stylesheet" href="/vendor/iwester/css/jquery.Jcrop.min.css">
@endsection
@section('content')
    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <h2>账号信息</h2>
        </div>
        <div class="layui-card-body">
            <div class="section-title-wrap">
                <div class="title">当前我的头像</div>
            </div>
            <div class="avatar-now">
                <span style="text-align: left">如果你还没有设置头像,系统会显示为默认头像,你可以自己上传一张新图片来作为自己的个人头像</span>
                <div class="avatar-show-wrap" style="height: 150px; margin-top: 20px">
                    <div class="avatar-show-item">
                        <div>
                            <img src="{{ \Auth::user()->avatar }}" onerror="this.src='/vendor/iwester/images/avatar/default.png'"/>
                        </div>
                        <div>128*128</div>
                    </div>
                    <div class="avatar-show-item">
                        <div>
                            <img src="{{ \Auth::user()->avatar }}" onerror="this.src='/vendor/iwester/images/avatar/default.png'"/>
                        </div>
                        <div>48*48</div>
                    </div>
                    <div class="avatar-show-item">
                        <div>
                            <img src="{{ \Auth::user()->avatar }}" onerror="this.src='/vendor/iwester/images/avatar/default.png'"/>
                        </div>
                        <div>30*30</div>
                    </div>
                </div>
            </div>
            <div class="section-title-wrap">
                <div class="title">设置我的新头像</div>
            </div>
            <form enctype="multipart/form-data" id="avatar">
                <div class="avatar-upload" style="text-align: left">
                    <div style="text-align: left">请选择一个新图片进行上传编辑。图片保存后可能需要刷新一下本页，才能查看到最新效果。</div>
                    <div class="red" style="text-align: left;color: #ff4038">注：仅支持JPG、GIF、PNG格式，且文件大小不超过1M</div>
                    <div class="layui-upload" style="position: relative; margin: 20px 0;width: 120px">
                        <button type="button" class="layui-btn" id="uploadPic"><i class="layui-icon"></i>选择头像</button>
                        <div class="layui-upload-list">
                            <ul id="layui-upload-box" class="layui-clear">
                                <input type="file" accept="image/*" style="position:absolute;top:0;left:0;opacity:0;width: 100%;height: 100%;z-index: 999;cursor: pointer" name="file" id="avatarInput" onchange="uploadFile($(this),'avatarInput')">
                            </ul>
                        </div>
                    </div>
                    <input type="hidden" id="x" name="x" value="0"/>
                    <input type="hidden" id="y" name="y" value="0"/>
                    <input type="hidden" id="w" name="w" value="128"/>
                    <input type="hidden" id="h" name="h" value="128"/>
                    <input type="hidden" id="file" name="file" value=""/>

                    {{--头像裁剪区域--}}
                    <div class="upload-wrap">
                        <div class="edit" style="position:relative;">
                            <img id="imgSource"/>
                        </div>
                        <div class="preview" id="preview" style="">
                            <div style="width:130px;height:130px;overflow:hidden">
                                <img id="imgCuter"/>
                            </div>
                        </div>
                    </div>
                    <div class="tips" style="text-align: left;margin: 20px 0">拖动可选择裁剪区域或调整大小</div>

                    <div class="layui-form-item">
                        <span class="layui-btn" id="btnSave">修 改</span>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection


@section('script')
    <script src="/vendor/iwester/js/jqueryImage/jquery.Jcrop.min.js"></script>
    <script src="/vendor/iwester/js/jqueryImage/personal_center.js"></script>
    <script>
        function uploadFile(obj, fileUpload) {
            var loading = layer.load( 2, {
                shade: [0.1,'#fff']
            });
            var input = obj.parents('from').find('#file');
            $.ajaxFileUpload({
                url: "{{ route('uploadImg') }}?size=302",
                secureuri: false,
                fileElementId: fileUpload,
                dataType: 'json',
                success: function (data) {
                    if (data.code) {
                        console.log(data.path);
                        $('#imgSource').attr('src', data.path);
                        $('#imgCuter').attr('src', data.path);
                        $(".jcrop-holder img").attr('src', data.path);
                        $('#file').attr('value',data.path);
                    } else {
                        parent.layer.alert(data.msg);
                    }
                    layer.close(loading);
                },
                error: function (data) {
                    layer.close(loading);
                    parent.layer.alert('服务异常~~');
                }
            });
            return false;
        }

        $(function () {
            var judge;
            var timer = setInterval(function () {
                judge = typeof($('#imgSource').attr('src')) !=="undefined";
                console.log(judge);
                if(judge == true){
                    clearInterval(timer);
                    $(".edit").one("click",function () {
                        if (typeof($('#imgSource').attr('src')) !=="undefined") {
                            var jcrop_api,
                                boundx,
                                boundy,

                                // Grab some information about the preview pane
                                $preview = $('#preview'),
                                $pcnt = $('#preview div'),
                                $pimg = $('#preview div img'),

                                xsize = $pcnt.width(),
                                ysize = $pcnt.height();

                            $('#imgSource').Jcrop({
                                onChange: updatePreview,
                                onSelect: updatePreview,
                                aspectRatio: xsize / ysize
                            }, function () {
                                var bounds = this.getBounds();
                                boundx = bounds[0];
                                boundy = bounds[1];
                                jcrop_api = this;

                                $preview.appendTo(jcrop_api.ui.holder);
                            });

                            function updatePreview(c) {

                                $('#x').val(c.x);
                                $('#y').val(c.y);
                                $('#w').val(c.w);
                                $('#h').val(c.h);

                                if (parseInt(c.w) > 0) {
                                    var rx = xsize / c.w;
                                    var ry = ysize / c.h;

                                    $pimg.css({
                                        width: Math.round(rx * boundx) + 'px',
                                        height: Math.round(ry * boundy) + 'px',
                                        marginLeft: '-' + Math.round(rx * c.x) + 'px',
                                        marginTop: '-' + Math.round(ry * c.y) + 'px'
                                    });
                                }
                            };
                        }
                    })
                }
            },200);

            $('#btnSave').click(function () {
                var loading = layer.load( 3, {
                    shade: [0.1,'#fff']
                });
                var formdada = $('#avatar').serialize();
                $.ajax({
                    url: '{{ route('account.avatar.store' )}}',
                    type: 'POST',
                    dataType: 'json',
                    data: formdada,
                })
                    .done(function (data) {
                        if (data.code == 200) {
                            layer.alert(data.message, {
                                icon: 1,
                                closeBtn: 0,
                                shift: 3
                            }, function (index) {
                                layer.close(index);
                                location.reload();
                            });
                        } else {
                            layer.close(loading);
                            layer.alert(data.message, {icon: 2, shift: 3});
                        }
                    })
                    .fail(function () {
                        layer.close(loading);
                        layer.alert('服务器异常', {icon: 2, shift: 3});
                    });
            });

            $('.overlay').click(function () {
                $('#prompt-dialog').hide();
                $('.overlay').hide();
            });
        });


    </script>
@endsection
