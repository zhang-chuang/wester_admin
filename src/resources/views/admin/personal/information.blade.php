@extends('iwester::admin.base')

@section('content')
    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <h2>账号信息</h2>
        </div>
        <div class="layui-card-body">
            <form class="layui-form" method="post" id="forms">
                {{csrf_field()}}

                <div class="layui-form-item">
                    <label for="" class="layui-form-label">用户：</label>
                    <div class="layui-input-inline">
                        <input type="text" name="user[name]" value="{{ \Auth::user()->username  }}" lay-verify="required" class="layui-input name" disabled>
                    </div>
                    <div class="layui-form-mid layui-word-aux">登录用户名，暂时不支持修改</div>
                </div>
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">邮箱：</label>
                    <div class="layui-input-inline">
                        <input type="text" name="" value="{{ \Auth::user()->email  }}" lay-verify="required" class="layui-input" disabled>
                    </div>
                    <div class="layui-form-mid layui-word-aux">可用户登录，暂时不支持修改</div>
                </div>
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">手机：</label>
                    <div class="layui-input-inline">
                        <input type="text" name="" value="{{ \Auth::user()->phone  }}"  class="layui-input" disabled>
                    </div>
                    <div class="layui-form-mid layui-word-aux">暂时不支持修改</div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <span class="layui-btn" id="btnSave">修 改</span>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('#btnSave').click(function () {
            if ($('.name').val().trim() == ''){
                layer.alert('昵称不可为空', {icon: 2, shift: 3});
                return false;
            }
            var loading = layer.load( 3, {
                shade: [0.1,'#fff']
            });
            var formdada = $('#forms').serialize();
            $.ajax({
                url: '{{ route('account.information.store' )}}',
                type: 'POST',
                dataType: 'json',
                data: formdada,
            })
                .done(function (data) {
                    if (data.code == 200) {
                        layer.alert(data.message, {
                            icon: 1,
                            closeBtn: 0,
                            shift: 3
                        }, function (index) {
                            layer.close(index);
                            location.reload();
                        });
                    } else {
                        layer.close(loading);
                        layer.alert(data.message, {icon: 2, shift: 3});
                    }
                })
                .fail(function () {
                    layer.close(loading);
                    layer.alert('服务器异常', {icon: 2, shift: 3});
                });
        });
    </script>
@endsection
