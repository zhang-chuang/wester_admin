@extends('iwester::admin.base')

@section('content')
    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <h2>修改密码</h2>
        </div>
        <div class="layui-card-body">
            <form class="layui-form" method="post" id="forms">
                {{csrf_field()}}
                <div class="layui-form-item">
                    <label for="L_pass" class="layui-form-label">新密码</label>
                    <div class="layui-input-inline">
                        <input type="password" id="L_pass" name="user[password]" required="" lay-verify="required" autocomplete="off" class="layui-input">
                    </div>
                    <div class="layui-form-mid layui-word-aux">6到14个字符</div>
                </div>
                <div class="layui-form-item">
                    <label for="L_repass" class="layui-form-label">确认密码</label>
                    <div class="layui-input-inline">
                        <input type="password" id="L_repass" name="user[password_confirmation]" required="" lay-verify="required" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <span class="layui-btn" id="btnSave">修 改</span>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('#btnSave').click(function () {
            var loading = layer.load(3, {
                shade: [0.1, '#fff']
            });
            var formdada = $('#forms').serialize();
            $.ajax({
                url: '{{ route('account.password.store' )}}',
                type: 'POST',
                dataType: 'json',
                data: formdada,
            })
                .done(function (data) {
                    if (data.code == 200) {
                        layer.alert(data.message, {
                            icon: 1,
                            closeBtn: 0,
                            shift: 3
                        }, function (index) {
                            layer.close(index);
                            location.reload();
                        });
                    } else if (data.code == 422) {
                        tip = '';
                        for (var i in data.errors) {
                            tip += data.errors[i][0] + '，';
                        }
                        layer.msg(tip, {icon: 2, shift: 3});
                    } else {
                        layer.close(loading);
                        layer.alert(data.message, {icon: 2, shift: 3});
                    }
                })
                .fail(function () {
                    layer.close(loading);
                    layer.alert('服务器异常', {icon: 2, shift: 3});
                });
        });
    </script>
@endsection
