@extends('iwester::admin.base')

@section('content')
    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <h2>书籍文章SEO规则</h2>
        </div>
        <div class="layui-card-body">
            <div class="layui-form">
                <table class="layui-table">
                    <colgroup>
                        <col width="40">
                        <col width="150">
                        <col width="200">
                        <col>
                    </colgroup>
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>栏目名称</th>
                        <th>标题规则</th>
                        <th>关键词规则</th>
                        <th>描述规则</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($categories as $category)
                        <tr>
                            <td>{{ $category->id }}</td>
                            <td>{{ $category->cate_name }}</td>
                            <td>{{ $category->title_rule }}</td>
                            <td>{{ $category->keyword_rule }}</td>
                            <td>{{ $category->description_rule }}</td>
                            <td>
                                @can('seo.book.rule.edit')
                                <span class="layui-btn layui-btn-normal layui-btn-sm" onclick="openPage('{{ route('seo.book.rule.edit', ['id'=> $category->id]) }}')">
                                    <i class="layui-icon layui-icon-edit"></i>编辑规则
                                </span>
                                @endcan
                            </td>
                        </tr>
                        @if($category->subCate)
                            @foreach($category->subCate as $subCate)
                                <tr>
                                    <td>{{ $subCate->id }}</td>
                                    <td>{!! $loop->last ? '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─ ': '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├─ ' !!}{{ $subCate->cate_name }}</td>
                                    <td>{{ $subCate->title_rule }}</td>
                                    <td>{{ $subCate->keyword_rule }}</td>
                                    <td>{{ $subCate->description_rule }}</td>
                                    <td>
                                        @can('seo.book.rule.edit')
                                        <span class="layui-btn layui-btn-normal layui-btn-sm" onclick="openPage('{{ route('seo.book.rule.edit', ['id'=> $subCate->id]) }}')">
                                            <i class="layui-icon layui-icon-edit"></i>编辑规则
                                        </span>
                                        @endcan
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    @empty
                        <tr>
                            <td colspan="6" style="text-align: center">暂无数据</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script>
        function openPage(data) {
            var openPage = layer.open({
                type: 2,
                title: '资讯文章SEO规则',
                shadeClose: false,
                scrollbar: false,
                shift: 3,
                btn: ['提交', '取消'],
                shade: 0.2,
                min: true,
                area: ['700px', '400px'],
                content: [data],
                yes: function (index, layero) {
                    var dataform = $(layero).find("iframe")[0].contentWindow.formdata();
                    save(dataform, index);
                },
                cancel: function (index, layero) {
                    layer.close(index)
                }
            });
        }
        function save(data, layerindex){
            var loading = layer.load( 3, {
                shade: [0.1,'#fff']
            });
            $.ajax({
                url: '{{ route('seo.book.rule.store') }}',
                type: 'POST',
                dataType: 'json',
                data: data,
            })
                .done(function (data) {
                    if (data.code == 200) {
                        layer.alert(data.message, {
                            icon: 1,
                            closeBtn: 0,
                            shift: 3
                        }, function (index) {
                            layer.close(index);
                            location.reload();
                        });
                    } else {
                        layer.close(loading);
                        layer.alert(data.message, {icon: 2, shift: 3});
                    }
                })
                .fail(function () {
                    layer.close(loading);
                    layer.alert('服务器异常', {icon: 2, shift: 3});
                });
        }

    </script>
@endsection


@section('script')

@endsection
