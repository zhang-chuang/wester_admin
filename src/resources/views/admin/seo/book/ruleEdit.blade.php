@extends('iwester::admin.base')

@section('content')
    <div class="layui-card">
        <div class="layui-card-body">
            <form class="layui-form" name="forms" method="post" id="forms">
                {{csrf_field()}}
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">SEO标题规则</label>
                    <input type="hidden" name="id" value="{{ $categorySeo->id }}">
                    <div class="layui-input-block">
                        <input type="text" name="seo[title_rule]" value="{{ $categorySeo->title_rule }}" lay-verify="" placeholder="请输入SEO标题规则" class="layui-input" >
                        <p class="rule help-block m-b-none" style="margin-bottom: 0!important;">
                            @foreach(\Iwester\Http\Model\Book\BookCategory::$rules['title'] as $rule)
                                <span style="color: #0d8ddb;cursor: pointer;margin-right: 4px;">{{ $rule }}</span>
                            @endforeach
                        </p>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">SEO关键词规则</label>
                    <div class="layui-input-block">
                        <input type="text" name="seo[keyword_rule]" value="{{ $categorySeo->keyword_rule }}" lay-verify="" placeholder="请输入SEO关键词规则" class="layui-input" >
                        <p class="rule" style="margin-bottom: 0!important;">
                            @foreach(\Iwester\Http\Model\Book\BookCategory::$rules['keyword'] as $rule)
                                <span style="color: #0d8ddb;cursor: pointer;margin-right: 4px;">{{ $rule }}</span>
                            @endforeach
                        </p>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">SEO描述规则</label>
                    <div class="layui-input-block">
                        <input type="text" name="seo[description_rule]" value="{{ $categorySeo->description_rule }}" lay-verify="" placeholder="请输入SEO关键词规则" class="layui-input" >
                        <p class="rule" style="margin-bottom: 0!important;">
                            @foreach(\Iwester\Http\Model\Book\BookCategory::$rules['description'] as $rule)
                                <span style="color: #0d8ddb;cursor: pointer;margin-right: 4px;">{{ $rule }}</span>
                            @endforeach
                        </p>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script src="/vendor/iwester/js/jquery.min.js"></script>
    <script>
        var formdata = function () {
            return $("#forms").serializeArray();
        }
        $('.rule>span').on('click', function () {
            var r = $(this).text();
            var ruleW = $(this).parents('.layui-input-block').find('.layui-input');
            ruleW.val(ruleW.val()+r);
        })
    </script>
@endsection


@section('script')
@endsection
