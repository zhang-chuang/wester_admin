@extends('iwester::admin.base')

@section('content')
    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <h2>网站友链编辑</h2>
        </div>
        <div class="layui-card-body">
            <form class="layui-form" name="forms" method="post" id="forms">
                {{csrf_field()}}
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">友链标题</label>
                    @if($link)
                        <input type="hidden" name="id" value="{{ $link->id }}">
                    @endif
                    <div class="layui-input-block">
                        <input type="text" name="seo[name]" value="{{ $link ? $link->name : '' }}" lay-verify="required" placeholder="请输入友链标题" class="layui-input" >
                    </div>
                </div>
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">友链地址</label>
                    <div class="layui-input-block">
                        <input type="text" name="seo[url]" value="{{ $link ? $link->url : ''}}" lay-verify="required" placeholder="请输入友链地址" class="layui-input" >
                    </div>
                </div>
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">友链排序</label>
                    <div class="layui-input-block">
                        <input type="number" name="seo[sort]" value="{{ $link ? $link->sort : '1'}}" lay-verify="number" placeholder="友链排序" class="layui-input" >
                    </div>
                </div>
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">是否使用</label>
                    <div class="layui-input-block">
                        <input type="checkbox" checked="" name="seo[status]" lay-skin="switch" lay-filter="switchTest" lay-text="ON|OFF"><div class="layui-unselect layui-form-switch layui-form-onswitch" lay-skin="_switch"><em>{{ !$link || $link->status == '1' ? "ON" : 'OFF' }}</em><i></i></div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">是否首页</label>
                    <div class="layui-input-block">
                        <input type="radio" name="seo[only_index]" value="1" title="首页" checked=""><div class="layui-unselect layui-form-radio layui-form-radioed"><i class="layui-anim layui-icon"></i><div>首页</div></div>
                        <input type="radio" name="seo[only_index]" value="0" title="全站"><div class="layui-unselect layui-form-radio"><i class="layui-anim layui-icon"></i><div>全站</div></div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script src="/vendor/iwester/js/jquery.min.js"></script>
    <script>
        var formdata = function () {
            return $("#forms").serializeArray();
        }
    </script>
@endsection


@section('script')
@endsection
