@extends('iwester::admin.base')

@section('content')
    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <h2>友情链接管理</h2>
        </div>
        <div class="layui-card-body">
            <div style="padding-bottom: 10px;">
                @can('seo.sitelink.edit')
                <span class="layui-btn layuiadmin-btn-list" onclick="openPage('{{ route('seo.sitelink.edit') }}')">新增友链</span>
                @endcan
            </div>
            <div class="layui-form">
                <table class="layui-table">
                    <thead>
                    <tr>
                        <th>排序</th>
                        <th>友链标题</th>
                        <th>友链地址</th>
                        <th>状态</th>
                        <th>类型</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                        @forelse($siteLinks as $link)
                            <tr>
                                <td>{{ $link->sort }}</td>
                                <td>{{ $link->name }}</td>
                                <td>{{ $link->url }}</td>
                                <td>
                                    <input type="checkbox" lay-skin="switch" lay-text="ON|OFF" {{ $link->status ==1 ? 'checked' : '' }} disabled>
                                <td>{{ $link->only_index ==1 ? '只在首页' : '全站' }}</td>
                                <td>
                                    @can('seo.sitelink.edit')
                                    <span class="layui-btn layui-btn-normal layui-btn-sm" onclick="openPage('{{ route('seo.sitelink.edit', ['id'=> $link->id]) }}')">
                                        <i class="layui-icon layui-icon-edit"></i>编辑
                                    </span>
                                    @endcan

                                    @can('seo.sitelink.destroy')
                                    <span class="layui-btn layui-btn-danger layui-btn-sm" onclick="_del('{{ route('seo.sitelink.destroy', ['id'=> $link->id]) }}')">
                                        <i class="layui-icon layui-icon-delete"></i>删除
                                    </span>
                                    @endcan
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6" style="text-align: center">暂无数据</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script>
        function _del(url) {
            layer.confirm('确认删除该友情链接？', {
                btn: ['删除','取消'] //按钮
            }, function(){
                var loading = layer.load( 3, {
                    shade: [0.1,'#fff']
                });
                $.ajax({
                    url: url,
                    type: 'delete',
                    dataType: 'json',
                })
                    .done(function (data) {
                        if (data.code == 200) {
                            layer.alert(data.message, {
                                icon: 1,
                                closeBtn: 0,
                                shift: 3
                            }, function (index) {
                                layer.close(index);
                                location.reload();
                            });
                        } else {
                            layer.close(loading);
                            layer.alert(data.message, {icon: 2, shift: 3});
                        }
                    })
                    .fail(function () {
                        layer.close(loading);
                        layer.alert('服务器异常', {icon: 2, shift: 3});
                    });
            }, function(){

            });


        }
        function openPage(data) {
            var openPage = layer.open({
                type: 2,
                title: '编辑友链',
                shadeClose: false,
                scrollbar: false,
                shift: 3,
                btn: ['提交', '取消'],
                shade: 0.2,
                min: true,
                area: ['700px', '500px'],
                content: [data],
                yes: function (index, layero) {
                    var dataform = $(layero).find("iframe")[0].contentWindow.formdata();
                    saveSeo(dataform, index);
                },
                cancel: function (index, layero) {
                    layer.close(index)
                }
            });
        }
        function saveSeo(data, layerindex){
            var loading = layer.load( 3, {
                shade: [0.1,'#fff']
            });
            $.ajax({
                url: '{{ route('seo.sitelink.store') }}',
                type: 'POST',
                dataType: 'json',
                data: data,
            })
                .done(function (data) {
                    if (data.code == 200) {
                        layer.alert(data.message, {
                            icon: 1,
                            closeBtn: 0,
                            shift: 3
                        }, function (index) {
                            layer.close(index);
                            location.reload();
                        });
                    } else {
                        layer.close(loading);
                        layer.alert(data.message, {icon: 2, shift: 3});
                    }
                })
                .fail(function () {
                    layer.close(loading);
                    layer.alert('服务器异常', {icon: 2, shift: 3});
                })
        }

    </script>
@endsection


@section('script')

@endsection
