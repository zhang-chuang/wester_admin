@extends('iwester::admin.base')

@section('content')
    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <h2>网站Sitemap
                <span class="layui-btn layui-btn-radius sitemap" style="float: right;">更新sitemap</span>
            </h2>
        </div>
        <div class="layui-card-body">
            <div class="layui-card">
                <div class="layui-card-header">sitemap
                    <a href="/sitemap.xml" target="_blank" style="float: right;color: #1E9FFF">查看sitemap.xml</a>
                </div>
                <div class="layui-card-body">
                    {!! $sitemap !!}
                </div>
            </div>
        </div>
    </div>
@endsection


@section('script')
    <script>
        $('.sitemap').on('click', function () {
            var loading = layer.load( 3, {
                shade: [0.1,'#fff']
            });
            layer.msg('sitemap正在更新...', {
                icon: 1
            });
            $.ajax({
                url: '{{ route('seo.sitemap.update') }}',
                type: 'POST',
                dataType: 'json'
            })
                .done(function (data) {
                    if (data.code == 200) {
                        layer.alert(data.message, {
                            icon: 1,
                            closeBtn: 0,
                            shift: 3
                        }, function () {
                            layer.closeAll();
                        });
                        location.reload();
                    } else {
                        layer.close(loading);
                        layer.alert(data.message, {icon: 2, shift: 3});
                    }
                })
                .fail(function () {
                    layer.close(loading);
                    layer.alert(data.message, {icon: 2, shift: 3});
                });
        })
    </script>
@endsection
