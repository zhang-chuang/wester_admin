@extends('iwester::admin.base')

@section('content')
    <div class="layui-card">
        <div class="layui-card-body">
            <form class="layui-form" name="forms" method="post" id="forms">
                {{csrf_field()}}
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">SEO标题</label>
                    <input type="hidden" name="id" value="{{ $categorySeo->id }}">
                    <div class="layui-input-block">
                        <input type="text" name="seo[title]" value="{{ $categorySeo->title }}" lay-verify="" placeholder="请输入SEO标题" class="layui-input" >
                    </div>
                </div>
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">SEO关键词</label>
                    <div class="layui-input-block">
                        <input type="text" name="seo[keyword]" value="{{ $categorySeo->keyword }}" lay-verify="" placeholder="请输入SEO关键词" class="layui-input" >
                    </div>
                </div>
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">SEO描述</label>
                    <div class="layui-input-block">
                        <textarea name="seo[description]" placeholder="请输入SEO描述" class="layui-textarea">{{ $categorySeo->description }}</textarea>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script src="/vendor/iwester/js/jquery.min.js"></script>
    <script>
        var formdata = function () {
            return $("#forms").serializeArray();
        }
    </script>
@endsection


@section('script')

@endsection
