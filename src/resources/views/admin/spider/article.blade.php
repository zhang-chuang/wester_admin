@extends('iwester::admin.base')
@section('css')
    <style>
        .layui-nav a {
            color: #666 !important;
        }
        .layui-nav .layui-nav-item a:hover, .layui-nav .layui-this a {
            color: #009688 !important;
        }
        .layui-laypage li, .layui-laypage li span {
            display: inline-block;
            *display: inline;
            *zoom: 1;
            vertical-align: middle;
            height: 28px;
            line-height: 28px;
            background-color: #fff;
            color: #333;
            font-size: 12px;
        }
        .layui-laypage li.active span{
            background-color: #009688;
            color: #fff;
        }
    </style>
@endsection
@section('content')
    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <h2>采集文章管理
                <a href="{{ route('spider.task') }}" class="layui-btn" style="float: right">返回采集管理</a>
            </h2>
        </div>
        <div class="layui-card-body">
            <div class="layui-form">
                <table class="layui-table">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>标题</th>
                        <th>分类</th>
                        <th>发布时间</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($articles as $article)
                        <tr>
                            <td>{{ $article->id }}</td>
                            <td><a target="_blank" href="{{ $article->detail_url }}">{{ $article->title }}</a></td>
                            <td>{{ $article->article_category }}</td>
                            <td>{{ $article->published_at }}</td>
                            <td>
                                <span class="layui-btn layui-btn-normal layui-btn-sm">
                                    <a style="color: #fff" href="{{ route('spider.task.article.caiji.edit', ['id'=> $article->id]) }}"><i class="layui-icon layui-icon-edit"></i>编辑</a>
                                </span>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="7" style="text-align: center">暂无数据</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
                <div class="layui-box layui-laypage layui-laypage-default" id="">
                    {{$articles->appends($params)->render()}}
                </div>
            </div>
        </div>
    </div>
    <script>

    </script>
@endsection


@section('script')

@endsection
