@extends('iwester::admin.base')

@section('css')

@endsection
@section('content')
    <div class="layui-card">
        <form class="layui-form" id="all-item">
            {{--基础配置--}}
            <div class="step-wrapper first-item" style="display: block">
                <div class="layui-card-header layuiadmin-card-header-auto">
                    <h2>基础配置
                        <a href="{{ route('spider.task') }}" class="layui-btn step-change-btn first-btn-check" style="float: right">返回列表</a>
                    </h2>
                </div>
                <div class="layui-form" id="first-item">
                    <div class="layui-card-body">
                        <div class="layui-form-item">
                            <label class="layui-form-label">任务名称:</label>
                            <div class="layui-input-block">
                                @if($task)
                                    <input type="hidden" name="id" value="{{ $task->id }}">
                                @endif
                                <input name="task[name]" type="text" placeholder="采集任务名称" class="layui-input" lay-verify="required" value="{{ $task->name ?? '' }}"/>
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">抓取频率:</label>
                            <div class="layui-input-block">
                                <select lay-verify="required" name="task[freq]">
                                    @foreach(\Iwester\Http\Model\Spider\SpiderTask::$freqs as $k=>$value)
                                        <option value="{{ $k }}" {{ $task && $task->freq == $k ? 'selected' : '' }}>{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">是否紧急</label>
                            <div class="layui-input-block">
                                <input type="radio" name="task[priority]" value="1" {{ !$task ?  'checked' : ''}} {{ $task && $task->priority == 1 ? 'checked' : '' }} title="一般" >
                                <input type="radio" name="task[priority]" value="2" {{ $task && $task->priority == 2 ? 'checked' : '' }} title="缓急">
                                <input type="radio" name="task[priority]" value="3" {{ $task && $task->priority == 3 ? 'checked' : '' }} title="紧急">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">页面编码:</label>
                            <div class="layui-input-block">
                                <select lay-verify="required" name="task[charset]">
                                    @foreach(\Iwester\Http\Model\Spider\SpiderTask::$charsets as $k=>$value)
                                        <option {{ $task && $task->charset == $k ? 'selected' : '' }} value="{{ $k }}">{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">COOKIE:</label>
                            <div class="layui-input-block">
                                <input name="task[cookie]" type="text" placeholder="cookie" class="layui-input" value="{{ $task->cookie ?? '' }}">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <div class="layui-input-block">
                            <span class="layui-btn step-change-btn first-btn-check" data-ele="sec-item">配置列表页参数</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @php
            $listConfig = $task ? $task->listConfig : false;
            $listUrlContent = $listConfig ? json_decode($listConfig->list_url_content, true) : false;
            $detailUrlContent = $listConfig ? json_decode($listConfig->detail_url_content, true) : false;
            @endphp
            {{--配置列表页参数--}}
            <div class="step-wrapper sec-item" style="display: none">
                <div class="layui-card-header layuiadmin-card-header-auto">
                    <h2>配置列表页参数
                        <a href="{{ route('spider.task') }}" class="layui-btn step-change-btn first-btn-check" style="float: right">返回列表</a>
                    </h2>
                </div>
                <div class="layui-form" id="sec-item">
                    <div class="layui-card-body">
                        <div class="layui-form-item">
                            <label class="layui-form-label" style="width: 100px;">数据存储模型:</label>
                            <div class="layui-input-block" style="margin-left: 150px;">
                                <select lay-verify="required" name="config[table_template]">
                                    @foreach(\Iwester\Http\Model\Spider\SpiderTask::$table_template as $k=>$value)
                                        <option {{ $listConfig && $listConfig->table_template == $value ? 'selected' : '' }} value="{{ $value }}">{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label" style="width: 100px;">列表URL规则:</label>
                            <div class="layui-input-block" style="margin-left: 150px;">
                                <input class="layui-input page_params-input" name="config[list_url]" type="text"
                                       value="{{ $listConfig->list_url ??  "http://www.dianshangwin.com/?&page=[PAGE]"}}"
                                       placeholder="http://www.xxx.com/?page=[PAGE]" lay-verify="required" />
                            </div>
                            <p style="margin-left: 150px;"><a href="javascript:" data-content="[PAGE]" class="page_params">[PAGE]</a></p>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label" style="width: 100px;">列表页码规则:</label>
                            <div class="layui-input-block" style="margin-left: 150px;">
                                <div class="" style="width: 64%; float:left;">
                                    <input class="layui-input page_params-input" name="config[list_page_param][rule]" placeholder="$('#pages a:eq(-2)')"
                                           value="{{ $listConfig->list_page_matche ?? '' }}"
                                    />
                                    <p><a href="javascript:" data-content="(.*)" class="page_params">(.*)</a> &nbsp;&nbsp;如果填数字表示指定最大页</p>
                                </div>
                                <div class="" style="width: 30%; float: left;margin-left: 1%;">
                                    <select name="config[list_page_param][type]" lay-verify="required">
                                        @foreach(\Iwester\Http\Model\Spider\SpiderTask::$contentTypes as $contentType)
                                            <option {{ $listConfig && $listConfig->list_page_attr == $contentType ? 'selected' : '' }} value="{{ $contentType }}">{{ $contentType }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        @if($listUrlContent)
                            <div id="list-wrap" data-index="{{ count($listUrlContent) }}">
                                @foreach($listUrlContent as $index=>$listUrlContentItem)
                                    @php
                                        $jichu = in_array($listUrlContentItem['key'], ['url', 'title', 'cover']) ? true : false;
                                    @endphp
                                    <div class="layui-form-item list-param-wrapp">
                                        <label class="layui-form-label" style="width: 100px;">{{ $loop->first ? '列表数据:': '' }}</label>
                                        <div class="layui-input-block" style="margin-left: 150px;">
                                            <div class="" style="width: 16%; float:left;">
                                                <input {{ $jichu ?'readonly': '' }} class="layui-input page_params-input" name="config[list_param][{{ $index }}][key]" data-type="key"
                                                       value="{{ $listUrlContentItem['key'] ?? '' }}"/>
                                            </div>
                                            <div class="" style="width: 30%; float:left;margin-left: 1%;">
                                                <input class="layui-input page_params-input" name="config[list_param][{{ $index }}][rule]" data-type="rule"
                                                       value="{{ $listUrlContentItem['rule'] ?? '' }}"/>
                                            </div>
                                            <div class="" style="width: 30%; float:left;margin-left: 1%;">
                                                <input class="layui-input page_params-input list-param-input" name="config[list_param][{{ $index }}][rule_match]" data-type="rule_match"
                                                       value="{{ $listUrlContentItem['rule_match'] ?? '' }}" placeholder="发布时间：(.*)"/>
                                            </div>
                                            <div class="" style="width: 16%; float:left;margin-left: 1%;">
                                                <select readonly name="config[list_param][{{ $index }}][type]" lay-verify="required" data-type="type">
                                                    @foreach(\Iwester\Http\Model\Spider\SpiderTask::$contentTypes as $contentType)
                                                        <option value="{{ $contentType }}" {{ $listUrlContentItem['type'] == $contentType ? 'selected':'' }}>{{ $contentType }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            @if($loop->first)
                                            <div class="" style="float:left;margin-left: 1%;">
                                                <i style="font-size: 23px;position: relative;top: 8px" class="layui-icon layui-icon-add-circle-fine list-add-btn"></i>
                                            </div>
                                            @else
                                                @if(!$jichu)
                                                <div class="list-down-btn" style="float:left;margin-left: 1%; display: none">
                                                    <i style="font-size: 23px;position: relative;top: 8px" class="layui-icon layui-icon-close"></i>
                                                </div>
                                                @endif
                                            @endif
                                        </div>
                                    </div>
                                    @if($loop->first)
                                    <div class="layui-form-item list-param-wrapp">
                                        <label class="layui-form-label" style="width: 100px;"></label>
                                        <div class="layui-input-block" style="margin-left: 150px;">
                                            <div class="" style="width: 95%; float:left;">
                                                <select class="url_prefix" readonly name="config[list_param][{{ $index }}][url_prefix]" data-type="url_prefix">
                                                    <option value="" >url前缀-不拼接</option>
                                                    @foreach(\Iwester\Http\Model\Spider\SpiderTask::$urlPrefix as $k=>$urlPrefix)
                                                        <option value="{{ $k }}" {{ isset($listUrlContentItem['url_prefix']) && $listUrlContentItem['url_prefix'] == $k ? 'selected':'' }}>{{ $urlPrefix }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                @endforeach
                            </div>
                        @else
                            <div id="list-wrap" data-index="4">
                                <div class="layui-form-item list-param-wrapp">
                                    <label class="layui-form-label" style="width: 100px;">列表数据:</label>
                                    <div class="layui-input-block" style="margin-left: 150px;">
                                        <div class="" style="width: 16%; float:left;">
                                            <input readonly class="layui-input page_params-input list-param-input" name="config[list_param][0][key]" data-type="key"
                                                   value="url"/>
                                        </div>
                                        <div class="" style="width: 30%; float:left;margin-left: 1%;">
                                            <input class="layui-input page_params-input list-param-input" name="config[list_param][0][rule]" data-type="rule"
                                                   value="" placeholder="$('#test4_1 .list_item li h2 a')"/>
                                        </div>
                                        <div class="" style="width: 30%; float:left;margin-left: 1%;">
                                            <input class="layui-input page_params-input list-param-input" name="config[list_param][0][rule_match]" data-type="rule_match"
                                                   value="" placeholder="发布时间：(.*)"/>
                                        </div>
                                        <div class="" style="width: 16%; float:left;margin-left: 1%;">
                                            <select class="list-param-input" readonly name="config[list_param][0][type]" lay-verify="required" data-type="type">
                                                @foreach(\Iwester\Http\Model\Spider\SpiderTask::$contentTypes as $contentType)
                                                    <option value="{{ $contentType }}" {{ $contentType == 'href'? 'selected':'' }}>{{ $contentType }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="" style="float:left;margin-left: 1%;">
                                            <i style="font-size: 23px;position: relative;top: 8px" class="layui-icon layui-icon-add-circle-fine list-add-btn"></i>
                                        </div>
                                        <div class="list-down-btn" style="float:left;margin-left: 1%; display: none">
                                            <i style="font-size: 23px;position: relative;top: 8px" class="layui-icon layui-icon-close"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="layui-form-item list-param-wrapp">
                                    <label class="layui-form-label" style="width: 100px;"></label>
                                    <div class="layui-input-block" style="margin-left: 150px;">
                                        <div class="" style="width: 95%; float:left;">
                                            <select class="url_prefix" readonly name="config[list_param][0][url_prefix]" data-type="url_prefix">
                                                <option value="" >url前缀-不拼接</option>
                                                @foreach(\Iwester\Http\Model\Spider\SpiderTask::$urlPrefix as $k=>$urlPrefix)
                                                    <option value="{{ $k }}">{{ $urlPrefix }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="layui-form-item list-param-wrapp">
                                    <label class="layui-form-label" style="width: 100px;"></label>
                                    <div class="layui-input-block" style="margin-left: 150px;">
                                        <div class="" style="width: 16%; float:left;">
                                            <input readonly class="layui-input list-param-input" name="config[list_param][1][key]" data-type="key"
                                                   value="title"/>
                                        </div>
                                        <div class="" style="width: 30%; float:left;margin-left: 1%;">
                                            <input class="layui-input list-param-input" name="config[list_param][1][rule]" data-type="rule"
                                                   value="" placeholder="$('#test4_1 .list_item li h2 a')"/>
                                        </div>
                                        <div class="" style="width: 30%; float:left;margin-left: 1%;">
                                            <input class="layui-input list-param-input" name="config[list_param][1][rule_match]" data-type="rule_match"
                                                   value="" placeholder="发布时间：(.*)"/>
                                        </div>
                                        <div class="" style="width: 16%; float:left;margin-left: 1%;">
                                            <select class="list-param-input" name="config[list_param][1][type]" lay-verify="required" data-type="type">
                                                @foreach(\Iwester\Http\Model\Spider\SpiderTask::$contentTypes as $contentType)
                                                    <option value="{{ $contentType }}">{{ $contentType }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="layui-form-item list-param-wrapp">
                                    <label class="layui-form-label" style="width: 100px;"></label>
                                    <div class="layui-input-block" style="margin-left: 150px;">
                                        <div class="" style="width: 16%; float:left;">
                                            <input readonly class="layui-input detail-param-input" name="config[list_param][2][key]" data-type="key"
                                                   value="cover"/>
                                        </div>
                                        <div class="" style="width: 30%; float:left;margin-left: 1%;">
                                            <input class="layui-input detail-param-input" name="config[list_param][2][rule]" data-type="rule"
                                                   value="" placeholder="$('.list_item ul li .item_pic img')"/>
                                        </div>
                                        <div class="" style="width: 30%; float:left;margin-left: 1%;">
                                            <input class="layui-input detail-param-input" name="config[list_param][2][rule_match]" data-type="rule_match"
                                                   value="" placeholder="发布时间：(.*)"/>
                                        </div>
                                        <div class="" style="width: 16%; float:left;margin-left: 1%;">
                                            <select class="detail-param-input" name="config[list_param][2][type]" lay-verify="required" data-type="type">
                                                @foreach(\Iwester\Http\Model\Spider\SpiderTask::$contentTypes as $contentType)
                                                    <option value="{{ $contentType }}">{{ $contentType }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="layui-form-item list-param-wrapp">
                                    <label class="layui-form-label" style="width: 100px;"></label>
                                    <div class="layui-input-block" style="margin-left: 150px;">
                                        <div class="" style="width: 16%; float:left;">
                                            <input readonly class="layui-input detail-param-input" name="config[list_param][3][key]" data-type="key"
                                                   value="category"/>
                                        </div>
                                        <div class="" style="width: 30%; float:left;margin-left: 1%;">
                                            <input class="layui-input detail-param-input" name="config[list_param][3][rule]" data-type="rule"
                                                   value="" placeholder="$('.list_item ul li .item')"/>
                                        </div>
                                        <div class="" style="width: 30%; float:left;margin-left: 1%;">
                                            <input class="layui-input detail-param-input" name="config[list_param][3][rule_match]" data-type="rule_match"
                                                   value="" placeholder="发布时间：(.*)"/>
                                        </div>
                                        <div class="" style="width: 16%; float:left;margin-left: 1%;">
                                            <select class="detail-param-input" name="config[list_param][3][type]" lay-verify="required" data-type="type">
                                                @foreach(\Iwester\Http\Model\Spider\SpiderTask::$contentTypes as $contentType)
                                                    <option value="{{ $contentType }}">{{ $contentType }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="layui-form-item">
                            <label class="layui-form-label" style="width: 100px;"></label>
                            <div class="layui-input-block" style="margin-left: 150px;">
                                 <span class="layui-btn step-change-btn " data-ele="first-item">
                                    返回上一层
                                </span>
                                <span class="layui-btn step-change-btn sec-btn-check" data-ele="three-item">
                                    配置详情页参数
                                </span>
                                <span class="layui-btn layui-btn-danger test-list-params">
                                    测试列表规则
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{--配置详情页参数--}}
            <div class="step-wrapper three-item" style="display: none">
                <div class="layui-card-header layuiadmin-card-header-auto">
                    <h2>配置详情页参数
                        <a href="{{ route('spider.task') }}" class="layui-btn step-change-btn first-btn-check" style="float: right">返回列表</a>
                    </h2>
                </div>

                <div class="layui-form" id="three-item">
                    <div class="layui-card-body">
                        <div class="layui-form-item">
                            <label class="layui-form-label" style="width: 100px;">详情页是否分页</label>
                            <div class="layui-input-block detail_use_page_btn" style="margin-left: 150px;">
                                <input lay-filter="detail_use_page_btn" type="radio" name="detail[detail_use_page]" value="0" title="否" {{ !$listConfig ? "checked" : ''}} {{ $listConfig && $listConfig->detail_use_page == 0 ? "checked" : ''}}>
                                <input lay-filter="detail_use_page_btn" type="radio" name="detail[detail_use_page]" value="1" title="是" {{ $listConfig && $listConfig->detail_use_page == 1 ? "checked" : ''}}>
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label" style="width: 100px;">详情页测试url</label>
                            <div class="layui-input-block" style="margin-left: 150px;">
                                <input name="detail[detail_test_url]" type="text" class="layui-input detail_test_url page_params-input"
                                       value="{{ $listConfig ? '' : ""}}" placeholder="http://www.toutiaonews.com/world/96926.html"/>
                            </div>
                        </div>

                        <div class="layui-form-item detail_use_page" style="display: {{ $listConfig && $listConfig->detail_use_page == 1 ? "block" : 'none'}}">
                            <label class="layui-form-label" style="width: 100px;">详情页码规则:</label>
                            <div class="layui-input-block" style="margin-left: 150px;">
                                <div class="" style="width: 61%; float:left;">
                                    <input class="layui-input page_params-input" name="detail[page_param][rule]"
                                           value="{{ $listConfig->detail_page_matche ??  ""}}" placeholder="$('.pagebreak li:eq(-2) a')"
                                    />
                                    <p><a href="javascript:" data-content="(.*)" class="page_params">(.*)</a></p>
                                </div>
                                <div class="" style="width: 38%; float: left;margin-left: 1%;">
                                    <select name="detail[page_param][type]" lay-verify="required">
                                        @foreach(\Iwester\Http\Model\Spider\SpiderTask::$contentTypes as $contentType)
                                            <option {{ $listConfig && $listConfig->detail_page_attr == $contentType ? 'selected' : '' }} value="{{ $contentType }}">{{ $contentType }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="layui-form-item detail_use_page" style="display: {{ $listConfig && $listConfig->detail_use_page == 1 ? "block" : 'none'}}">

                            <label class="layui-form-label" style="width: 100px;">分页url规则:</label>
                            <div class="layui-input-block" style="margin-left: 150px;">
                                <input name="detail[fenye_url]" type="text" class="layui-input detail_test_url page_params-input"
                                       value="{{ $listConfig->detail_page_url ?? "" }}" placeholder="http://www.toutiaonews.com/world/96926_[PAGE].html"/>
                            </div>
                            <p class="detail_page_params" style="margin-left: 150px;display: none"><a href="javascript:" data-content="[PAGE]" class="page_params">[PAGE]</a></p>
                        </div>
                        @if($detailUrlContent)
                            <div id="detail-wrap" data-index="{{ count($detailUrlContent) }}">
                                @foreach($detailUrlContent as $index=>$detailUrlContentItem)
                                <div class="layui-form-item detail-param-wrapp">
                                    <label class="layui-form-label" style="width: 100px;">{{ $loop->first ? '详情数据:': '' }}</label>
                                    <div class="layui-input-block" style="margin-left: 150px;">
                                        <div class="" style="width: 16%; float:left;">
                                            <input class="layui-input detail-param-input" name="detail[data_param][{{ $index }}][key]" data-type="key"
                                                   value="{{ $detailUrlContentItem['key'] ?? '' }}"/>
                                        </div>
                                        <div class="" style="width: 30%; float:left;margin-left: 1%;">
                                            <input class="layui-input detail-param-input" name="detail[data_param][{{ $index }}][rule]" data-type="rule"
                                                   value="{{ $detailUrlContentItem['rule'] ?? '' }}"/>
                                        </div>
                                        <div class="" style="width: 30%; float:left;margin-left: 1%;">
                                            <input class="layui-input detail-param-input" name="detail[data_param][{{ $index }}][rule_match]" data-type="rule_match"
                                                   value="{{ $detailUrlContentItem['rule_match'] ?? '' }}" placeholder="发布时间：(.*)"/>
                                        </div>
                                        <div class="" style="width: 16%; float:left;margin-left: 1%;">
                                            <select class="detail-param-input" name="detail[data_param][{{ $index }}][type]" lay-verify="required" data-type="type">
                                                @foreach(\Iwester\Http\Model\Spider\SpiderTask::$contentTypes as $contentType)
                                                    <option value="{{ $contentType }}" {{ $detailUrlContentItem['type'] == $contentType ? 'selected':'' }}>{{ $contentType }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @if($loop->first)
                                        <div class="detail-add-btn" style="float:left;margin-left: 1%;">
                                            <i style="font-size: 23px;position: relative;top: 8px" class="layui-icon layui-icon-add-circle-fine"></i>
                                        </div>
                                        @else
                                        <div class="detail-down-btn" style="float:left;margin-left: 1%; display: none">
                                            <i style="font-size: 23px;position: relative;top: 8px" class="layui-icon layui-icon-close"></i>
                                        </div>
                                        @endif

                                    </div>
                                </div>
                                @endforeach
                            </div>
                            @else
                            <div id="detail-wrap" data-index="5">
                                <div class="layui-form-item detail-param-wrapp">
                                    <label class="layui-form-label" style="width: 100px;">详情数据:</label>
                                    <div class="layui-input-block" style="margin-left: 150px;">
                                        <div class="" style="width: 16%; float:left;">
                                            <input readonly class="layui-input detail-param-input" name="detail[data_param][0][key]" data-type="key"
                                                   value="title"/>
                                        </div>
                                        <div class="" style="width: 30%; float:left;margin-left: 1%;">
                                            <input class="layui-input detail-param-input" name="detail[data_param][0][rule]" data-type="rule"
                                                   value="" placeholder="$('h1')"/>
                                        </div>
                                        <div class="" style="width: 30%; float:left;margin-left: 1%;">
                                            <input class="layui-input detail-param-input" name="detail[data_param][0][rule_match]" data-type="rule_match"
                                                   value="" placeholder="发布时间：(.*)"/>
                                        </div>
                                        <div class="" style="width: 16%; float:left;margin-left: 1%;">
                                            <select class="detail-param-input" name="detail[data_param][0][type]" lay-verify="required" data-type="type">
                                                @foreach(\Iwester\Http\Model\Spider\SpiderTask::$contentTypes as $contentType)
                                                    <option value="{{ $contentType }}">{{ $contentType }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="detail-add-btn" style="float:left;margin-left: 1%;">
                                            <i style="font-size: 23px;position: relative;top: 8px" class="layui-icon layui-icon-add-circle-fine"></i>
                                        </div>
                                        <div class="detail-down-btn" style="float:left;margin-left: 1%; display: none">
                                            <i style="font-size: 23px;position: relative;top: 8px" class="layui-icon layui-icon-close"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="layui-form-item detail-param-wrapp">
                                    <label class="layui-form-label" style="width: 100px;"></label>
                                    <div class="layui-input-block" style="margin-left: 150px;">
                                        <div class="" style="width: 16%; float:left;">
                                            <input readonly class="layui-input detail-param-input" name="detail[data_param][1][key]" data-type="key"
                                                   value="content"/>
                                        </div>
                                        <div class="" style="width: 30%; float:left;margin-left: 1%;">
                                            <input class="layui-input detail-param-input" name="detail[data_param][1][rule]" data-type="rule"
                                                   value="" placeholder="$('.detal_content')"/>
                                        </div>
                                        <div class="" style="width: 30%; float:left;margin-left: 1%;">
                                            <input class="layui-input detail-param-input" name="detail[data_param][1][rule_match]" data-type="rule_match"
                                                   value="" placeholder="发布时间：(.*)"/>
                                        </div>
                                        <div class="" style="width: 16%; float:left;margin-left: 1%;">
                                            <select class="detail-param-input" name="detail[data_param][1][type]" lay-verify="required" data-type="type">
                                                @foreach(\Iwester\Http\Model\Spider\SpiderTask::$contentTypes as $contentType)
                                                    <option value="{{ $contentType }}" {{ $contentType == 'html' ? 'selected':'' }}>{{ $contentType }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="layui-form-item detail-param-wrapp">
                                    <label class="layui-form-label" style="width: 100px;"></label>
                                    <div class="layui-input-block" style="margin-left: 150px;">
                                        <div class="" style="width: 16%; float:left;">
                                            <input readonly class="layui-input detail-param-input" name="detail[data_param][2][key]" data-type="key"
                                                   value="category"/>
                                        </div>
                                        <div class="" style="width: 30%; float:left;margin-left: 1%;">
                                            <input class="layui-input detail-param-input" name="detail[data_param][2][rule]" data-type="rule"
                                                   value="" placeholder="$('h1')"/>
                                        </div>
                                        <div class="" style="width: 30%; float:left;margin-left: 1%;">
                                            <input class="layui-input detail-param-input" name="detail[data_param][2][rule_match]" data-type="rule_match"
                                                   value="" placeholder="发布时间：(.*)"/>
                                        </div>
                                        <div class="" style="width: 16%; float:left;margin-left: 1%;">
                                            <select class="detail-param-input" name="detail[data_param][2][type]" lay-verify="required" data-type="type">
                                                @foreach(\Iwester\Http\Model\Spider\SpiderTask::$contentTypes as $contentType)
                                                    <option value="{{ $contentType }}">{{ $contentType }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="layui-form-item detail-param-wrapp">
                                    <label class="layui-form-label" style="width: 100px;"></label>
                                    <div class="layui-input-block" style="margin-left: 150px;">
                                        <div class="" style="width: 16%; float:left;">
                                            <input readonly class="layui-input detail-param-input" name="detail[data_param][3][key]" data-type="key"
                                                   value="published_at"/>
                                        </div>
                                        <div class="" style="width: 30%; float:left;margin-left: 1%;">
                                            <input class="layui-input detail-param-input" name="detail[data_param][3][rule]" data-type="rule"
                                                   value=""/>
                                        </div>
                                        <div class="" style="width: 30%; float:left;margin-left: 1%;">
                                            <input class="layui-input detail-param-input" name="detail[data_param][3][rule_match]" data-type="rule_match"
                                                   value="" placeholder="发布时间：(.*)"/>
                                        </div>
                                        <div class="" style="width: 16%; float:left;margin-left: 1%;">
                                            <select class="detail-param-input" name="detail[data_param][3][type]" lay-verify="required" data-type="type">
                                                @foreach(\Iwester\Http\Model\Spider\SpiderTask::$contentTypes as $contentType)
                                                    <option value="{{ $contentType }}">{{ $contentType }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="layui-form-item">
                            <label class="layui-form-label" style="width: 100px;"></label>
                            <div class="layui-input-block" style="margin-left: 150px;">
                                <span class="layui-btn step-change-btn" data-ele="sec-item">返回上一层</span>
                                <span class="layui-btn layui-btn-danger test-detail-query">测试详情页</span>
                                <span class="layui-btn" id="save_btn">配置入库</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection


@section('script')
    <script>
        // 鼠标位置
        var cursurPosition = 0;
        $(".page_params-input").on('blur', function () {
            var _this = $(this);
            if (this.selectionStart) {//非IE
                cursurPosition = this.selectionStart;
            } else {//IE
                try {
                    var range = document.selection.createRange();
                    range.moveStart("character", -this.value.length);
                    console.log(range.text.length);
                    cursurPosition = range.text.length;
                } catch (e) {
                    cursurPosition = 0;
                }
            }
        });



        var detail_test_url = '';
        function _first_check(){
            var _value = $('.first-item input[name="task[name]"]').val();
            if (_value.trim() == ''){
                layer.msg('请填写任务名称');
                return false;
            }
            return true;
        }

        $('.step-change-btn').on('click', function () {
            if ($(this).hasClass('first-btn-check')){
                if (!_first_check()) return;
            }

            var el = $(this).data('ele');
            $('.step-wrapper').hide();
            $('.'+el).show();
        })
        // 列表页
        $('.page_params').on('click', function () {
            var param = $(this).data('content');
            var _value = $(this).parents('.layui-form-item').find('.page_params-input');
            var itext = _value.val();
            _value.val(itext.substring(0, cursurPosition) + param + itext.substring(cursurPosition, itext.length));
        })
        $('.test-list-params').on('click', function () {
            var loading = layer.load( 3, {
                shade: [0.1,'#fff']
            });
            $('.detail_test_url').val('');
            detail_test_url = '';
            $.ajax({
                url: '{{ route('spider.test.article.list') }}',
                type: 'post',
                dataType: 'json',
                data: $('#all-item').serializeArray()
            })
                .done(function (data) {
                    layer.close(loading);
                    if (data.code == 200) {
                        var mes = '<p>最大页码：'+data['maxPage']+' 测试url：'+data['testPage']+'</p>';
                        var testData = data.testData;
                        var content = '';
                        for (var i=0; i < testData.length; i++){
                            for (var c in testData[i]) {
                                if (c == 'url' && detail_test_url == ''){
                                    detail_test_url = testData[i][c];
                                    $('.detail_test_url').val(detail_test_url);
                                }
                                console.log(detail_test_url);
                                content += '<p>'+c+'：'+ testData[i][c] +'</p>';
                            }
                        }
                        layer.alert(mes + content, {
                            area: ['600px', '400px'],
                            shift: 3
                        }, function (index) {
                            layer.close(index);
                        });
                    } else {
                        layer.close(loading);
                        layer.alert(data.message, {icon: 2, shift: 3});
                    }
                })
                .fail(function () {
                    layer.close(loading);
                    layer.alert('服务器异常', {icon: 2, shift: 3});
                });
        })

        $('.list-add-btn').on('click', function () {
            var cp = $('.list-param-wrapp').eq(0).clone();
            var index = $('#list-wrap').data('index') +1;
            console.log(index);
            cp.find('.list-param-input').each(function () {
                var type = $(this).data('type');
                var _value = 'config[list_param]['+index+']['+type+']';
                $(this).attr('name', _value)
            })
            cp.find('.layui-input-block>div').eq(0).find('input').attr('readonly', false);
            cp.find('.layui-form-label').text('');
            cp.find('input').val('');
            cp.find('.list-add-btn').hide();
            cp.find('.list-down-btn').show();
            $('#list-wrap').append(cp);
            layui.use('form', function(){
                var form = layui.form;
                form.render('select');
            });
            $('#list-wrap').data('index', index);
        })
        $(document).on('click','.list-down-btn', function () {
            if ($(this).parents('.list-param-wrapp').index() != 1){
                $(this).parents('.list-param-wrapp').remove();
            }else{
                layer.msg('保留基础参数');
            }
        })
        // 详情页
        $('.detail-add-btn').on('click', function () {
            var cp = $('.detail-param-wrapp').eq(0).clone();
            var index = $('#detail-wrap').data('index') +1;
            cp.find('.detail-param-input').each(function () {
                var type = $(this).data('type');
                var _value = 'detail[data_param]['+index+']['+type+']';
                $(this).attr('name', _value)
            })
            cp.find('.layui-input-block>div').eq(0).find('input').attr('readonly', false);
            cp.find('.layui-form-label').text('');
            cp.find('input').val('');
            cp.find('.detail-add-btn').hide();
            cp.find('.detail-down-btn').show();
            $('#detail-wrap').append(cp);
            layui.use('form', function(){
                var form = layui.form;
                form.render('select');
            });
            $('#detail-wrap').data('index', index);
        })
        $(document).on('click','.detail-down-btn', function () {
            $(this).parents('.detail-param-wrapp').remove();
        })
        $('.test-detail-query').on('click', function () {
            var test_url = $('.detail_test_url').val();
            if(test_url.trim() == ''){
                layer.msg('请先完成列表页参数配置测试');
                return false;
            }
            var loading = layer.load( 3, {
                shade: [0.1,'#fff']
            });
            $.ajax({
                url: '{{ route('spider.test.article.detail') }}',
                type: 'post',
                dataType: 'json',
                data: $('#all-item').serializeArray()
            })
                .done(function (data) {
                    layer.close(loading);
                    if (data.code == 200) {
                        var mes = '<p>测试url：'+data['testPage']+'</p>';
                        var testData = data.testData;
                        var content = '';
                        for (var c in testData) {
                            content += '<p>'+c+'：'+ testData[c] +'</p>';
                        }
                        layer.alert(mes + content, {
                            area: ['800px', '500px'],
                            shift: 3
                        }, function (index) {
                            layer.close(index);
                        });
                    } else {
                        layer.close(loading);
                        layer.alert(data.message, {icon: 2, shift: 3});
                    }
                })
                .fail(function () {
                    layer.close(loading);
                    layer.alert('服务器异常', {icon: 2, shift: 3});
                });
        })

        // 保存入库
        $('#save_btn').on('click', function () {
            // var test_url = $('.detail_test_url').val();
            // if(test_url.trim() == ''){
            //     layer.msg('请先完成列表页参数配置测试');
            //     return false;
            // }
            var loading = layer.load( 3, {
                shade: [0.1,'#fff']
            });
            $.ajax({
                url: '{{ route('spider.task.article.store') }}',
                type: 'post',
                dataType: 'json',
                data: $('#all-item').serializeArray()
            })
                .done(function (data) {
                    layer.close(loading);
                    if (data.code == 200) {
                        layer.alert(data.message, {
                            icon: 1,
                            closeBtn: 0,
                            shift: 3
                        }, function (index) {
                            layer.close(index);
                            window.location.href = data.href;
                        });
                    } else {
                        layer.close(loading);
                        layer.alert(data.message, {icon: 2, shift: 3});
                    }
                })
                .fail(function () {
                    layer.close(loading);
                    layer.alert('服务器异常', {icon: 2, shift: 3});
                });
        })


        layui.use('form', function(){
            var form = layui.form;
            form.on('radio(detail_use_page_btn)', function(data){
                if(data.value == 0){
                    $('.detail_use_page').hide();
                    $('.detail_page_params').hide();
                }else{
                    $('.detail_use_page').show();
                    $('.detail_page_params').show();
                }
            });
        });

    </script>
@endsection
