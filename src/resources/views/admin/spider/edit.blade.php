@extends('iwester::admin.base')
@section('css')
    <link rel="stylesheet" type="text/css" href="/vendor/iwester/static/wangEditor/css/wangEditor.min.css">
    <link rel="stylesheet" type="text/css" href="/vendor/iwester/static/select2/select2.min.css">
    <style type="text/css">
        #editor-trigger {
            height: 400px;
            /*max-height: 500px;*/
        }
        .container {
            width: 100%;
            margin: 0 auto;
            position: relative;
        }
        #tags .layui-form-select{
            display: none!important;
        }
        #tags .select2-container,#tags .select2-container--default .select2-selection--single{
            width: 100%!important;
            height: 38px;
            border-radius: 2px;
            border-color: #D2D2D2 !important;
        }
        #tags .select2-container--default .select2-selection--single .select2-selection__rendered,
        #tags .select2-container--default .select2-selection--single .select2-selection__arrow{
            height: 100%!important;
            line-height: 38px;
            color: #666;
        }
    </style>
@endsection
@section('content')
    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <h2>编辑采集文章</h2>
        </div>
        <div class="layui-card-body">
            <form class="layui-form" action="{{route('spider.task.article.caiji.store')}}" method="post">
                {{csrf_field()}}
                <input type="hidden" name="id" value="{{ $article->id }}">
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">采集标题</label>
                    <div class="layui-input-block">
                        <input type="text" name="article[title]" value="{{ $article->title }}" lay-verify="required" placeholder="请输入文章标题" class="layui-input" >
                    </div>
                </div>
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">采集分类</label>
                    <div class="layui-input-block">
                        <input type="text"  value="{{ $article->article_category }}"  disabled readonly class="layui-input" >
                    </div>
                </div>
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">文章分类</label>
                    <div class="layui-input-block">
                        <div class="layui-block">
                            <select lay-verify="required" name="article[article_category_id]">
                                <option value="">请选择分类</option>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->cate_name }}</option>
                                    @include('iwester::admin.zixun.article.article_cate_el', ['category'=> $category, 'shendu'=> 1, 'select'=> false])
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="layui-form-item" id="tags">
                    <label for="" class="layui-form-label">标签</label>
                    <div class="layui-input-block">
                        @php
                            $articleTags = null;
                        @endphp
                        <input type="hidden" id="tags_id" name="tags" value="{{ $articleTags ? $articleTags->pluck('id')->toJson() : '' }}"/>
                        <select multiple="multiple">
                            @foreach($tags as $tag)
                                <option value="{{ $tag->id }}" >{{ $tag->tag }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">title</label>
                    <div class="layui-input-block">
                        <input type="text" name="content[title]" value="" lay-verify="" placeholder="请输入title" class="layui-input" >
                    </div>
                </div>
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">keyword</label>
                    <div class="layui-input-block">
                        <input type="text" name="content[keyword]" value="" lay-verify="" placeholder="请输入keyword" class="layui-input" >
                    </div>
                </div>
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">description</label>
                    <div class="layui-input-block">
                        <input type="text" name="content[description]" value="" lay-verify="" placeholder="请输入description" class="layui-input" >
                    </div>
                </div>

                <div class="layui-form-item">
                    <label for="" class="layui-form-label">文章内容</label>
                    <div class="layui-input-block">
                        <div id="editor-container" class="container" style="padding-left: 0;padding-right: 0;">
                            <textarea lay-verify="required" id="editor-trigger" name="content[content]">{{ $article->content }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">是否公开</label>
                    <div class="layui-input-block">
                        <input type="checkbox" checked value="0" name="article[publish_status]" lay-skin="switch" lay-text="是|否">
                    </div>
                </div>
                <input type="hidden"  name="article[audit_status]" value="1">
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">文章封面</label>
                    <div class="layui-input-block">
                        <input type="text" name="article[cover]" value="{{ $article->cover }}" placeholder="图片地址" class="layui-input img-cover-input" >
                        <div class="layui-upload" style="position: relative">
                            <button type="button" class="layui-btn" id="uploadPic"><i class="layui-icon">&#xe67c;</i>选择图片</button>
                            <div class="layui-upload-list" >
                                <ul id="layui-upload-box" class="layui-clear">
                                    <input type="file" accept="image/*"
                                           style="position:absolute;top:0;left:0;opacity:0;width: 100%;height: 100%;z-index: 999;cursor: pointer"
                                           name="file" id="LOGO" onchange="uploadFile($(this),'LOGO')">
                                </ul>
                            </div>
                        </div>
                        <div class="img-cover" onclick="open_image($(this))" style="display: {{ $article  ? 'block' : 'none' }}">
                            <div style="width: auto; height: 90px;cursor: pointer;display: inline-block">
                                <img src="{{ $article->cover }}" style="width: auto; height: 100%;max-width: 700px;" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <button type="submit" class="layui-btn" lay-submit="" lay-filter="formDemo">数据入库</button>
                        <a class="layui-btn layui-btn-danger" href="javascript:;" onclick="_del('{{ route('spider.task.article.caiji.destroy', ['id'=> $article->id]) }}')">
                            <i class="layui-icon layui-icon-delete"></i>废弃</a>
                        <a class="layui-btn" href="{{ route('spider.task.article.caiji.list') }}">返 回</a>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script>
        function uploadFile(obj, fileUpload) {
            var loading = layer.load( 2, {
                shade: [0.1,'#fff']
            });
            var input = obj.parents('.layui-input-block').find('input[type=text]');
            $.ajaxFileUpload({
                url: "{{ route('uploadImg') }}",
                secureuri: false,
                fileElementId: fileUpload,
                dataType: 'json',
                success: function (data) {
                    if (data.code) {
                        input.val(data.path);
                        $('.img-cover img').attr('src', data.path)
                    } else {
                        parent.layer.alert(data.msg);
                    }
                    layer.close(loading);
                },
                error: function (data) {
                    layer.close(loading);
                    parent.layer.alert('服务异常~~');
                }
            });

            return false;
        }

        $('.img-cover-input').on('change', function(){
            $(this).parents('.layui-form-item').find('.img-cover img').attr('src', $(this).val());
        });
        function open_image(_this){
            layer.open({
                type: 1,
                title: false,
                closeBtn: 0,
                area: ['auto'],
                skin: 'layui-layer-nobg',
                shadeClose: true,
                content: _this.find('div').html()
            });
        }

        function _del(url) {
            layer.confirm('确认废弃该文章？', {
                btn: ['删除','取消'] //按钮
            }, function(){
                var loading = layer.load( 3, {
                    shade: [0.1,'#fff']
                });
                $.ajax({
                    url: url,
                    type: 'delete',
                    dataType: 'json',
                })
                    .done(function (data) {
                        if (data.code == 200) {
                            layer.alert(data.message, {
                                icon: 1,
                                closeBtn: 0,
                                shift: 3
                            }, function (index) {
                                layer.close(index);
                                window.location.href = data.url;
                            });
                        } else {
                            layer.close(loading);
                            layer.alert(data.message, {icon: 2, shift: 3});
                        }
                    })
                    .fail(function () {
                        layer.close(loading);
                        layer.alert('服务器异常', {icon: 2, shift: 3});
                    });
            }, function(){

            });


        }
    </script>
@endsection


@section('script')
    <script type="text/javascript" src="/vendor/iwester/static/wangEditor/js/wangEditor.js"></script>
    <script type="text/javascript" src="/vendor/iwester/static/select2/select2.full.min.js"></script>
    <script type="text/javascript">
        $('#tags select').select2({
            language : "zh-CN",
            placeholder:"请选择标签",//默认值
            allowClear: true
        });
        $('#tags select').on('change', function () {
            var data = $('#tags select').select2('data');
            var json_data = []
            for(var i in data){
                // json_data.push({'id': data[i]['id'], 'tag': data[i]['text']})
                json_data.push(data[i]['id'])
            }
            $('#tags_id').val(JSON.stringify(json_data))
            // console.log($('#tags_id').val())
        })

        // 阻止输出log
        // wangEditor.config.printLog = false;
        var editor = new wangEditor('editor-trigger');
        // 上传图片
        editor.config.uploadImgUrl = '{{ route('uploadImg') }}';
        editor.config.uploadImgFileName = 'file';

        // 自定义load事件
        editor.config.uploadImgFns.onload = function (resultText, xhr) {
            resultText = JSON.parse(resultText)
            // resultText 服务器端返回的text
            // xhr 是 xmlHttpRequest 对象，IE8、9中不支持

            // 上传图片时，已经将图片的名字存在 editor.uploadImgOriginalName
            var originalName = editor.uploadImgOriginalName || '';

            // 如果 resultText 是图片的url地址，可以这样插入图片：
            editor.command(null, 'insertHtml', '<img src="' + resultText.path + '" alt="' + originalName + '" style="max-width:100%;"/>');
        };

        // 隐藏网络图片
        //  editor.config.hideLinkImg = true;

        // 表情显示项
        // editor.config.emotionsShow = 'value';
        // editor.config.emotions = {
        //     'default': {
        //         title: '默认',
        //         data: './emotions.data'
        //     },
        //     'weibo': {
        //         title: '微博表情',
        //         data: [
        //             {
        //                 icon: 'http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/7a/shenshou_thumb.gif',
        //                 value: '[草泥马]'
        //             },
        //             {
        //                 icon: 'http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/60/horse2_thumb.gif',
        //                 value: '[神马]'
        //             },
        //             {
        //                 icon: 'http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/bc/fuyun_thumb.gif',
        //                 value: '[浮云]'
        //             },
        //             {
        //                 icon: 'http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/c9/geili_thumb.gif',
        //                 value: '[给力]'
        //             },
        //             {
        //                 icon: 'http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/f2/wg_thumb.gif',
        //                 value: '[围观]'
        //             },
        //             {
        //                 icon: 'http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/70/vw_thumb.gif',
        //                 value: '[威武]'
        //             }
        //         ]
        //     }
        // };

        // 插入代码时的默认语言
        // editor.config.codeDefaultLang = 'html'

        // 只粘贴纯文本
        // editor.config.pasteText = true;

        // 跨域上传
        // editor.config.uploadImgUrl = 'http://localhost:8012/upload';

        // 第三方上传
        // editor.config.customUpload = true;

        // 普通菜单配置
        // editor.config.menus = [
        //     'img',
        //     'insertcode',
        //     'eraser',
        //     'fullscreen'
        // ];
        // 只排除某几个菜单（兼容IE低版本，不支持ES5的浏览器），支持ES5的浏览器可直接用 [].map 方法
        // editor.config.menus = $.map(wangEditor.config.menus, function(item, key) {
        //     if (item === 'insertcode') {
        //         return null;
        //     }
        //     if (item === 'fullscreen') {
        //         return null;
        //     }
        //     return item;
        // });

        // onchange 事件
        // editor.onchange = function () {
        //     console.log(this.$txt.html());
        // };

        // 取消过滤js
        // editor.config.jsFilter = false;

        // 取消粘贴过来
        // editor.config.pasteFilter = false;

        // 设置 z-index
        // editor.config.zindex = 20000;

        // 语言
        // editor.config.lang = wangEditor.langs['en'];

        // 自定义菜单UI
        // editor.UI.menus.bold = {
        //     normal: '<button style="font-size:20px; margin-top:5px;">B</button>',
        //     selected: '.selected'
        // };
        // editor.UI.menus.italic = {
        //     normal: '<button style="font-size:20px; margin-top:5px;">I</button>',
        //     selected: '<button style="font-size:20px; margin-top:5px;"><i>I</i></button>'
        // };
        editor.create();
    </script>
@endsection
