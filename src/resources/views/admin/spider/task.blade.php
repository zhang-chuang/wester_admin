@extends('iwester::admin.base')
@section('css')
    <style>
        .layui-nav a {
            color: #666 !important;
        }
        .layui-nav .layui-nav-item a:hover, .layui-nav .layui-this a {
            color: #009688 !important;
        }
        .layui-laypage li, .layui-laypage li span {
            display: inline-block;
            *display: inline;
            *zoom: 1;
            vertical-align: middle;
            height: 28px;
            line-height: 28px;
            background-color: #fff;
            color: #333;
            font-size: 12px;
        }
        .layui-laypage li.active span{
            background-color: #009688;
            color: #fff;
        }
    </style>
@endsection
@section('content')
    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <h2>采集任务管理
                <span style="margin-left: 20px;font-size: 13px;">使用css选择器规则$('.xxx'), 使用匹配截取规则 - 前面内容(.*)后面内容</span>
                <a href="{{ route('spider.task.article.create') }}" class="layui-btn" style="float: right">添加采集任务</a>
                <a href="{{ route('spider.task.article.caiji.list') }}" class="layui-btn layui-btn-normal" style="float: right; margin-right: 15px;">审核采集文章</a>
            </h2>
        </div>
        <div class="layui-card-body">
            <div class="layui-form">
                <table class="layui-table">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>任务名称</th>
                        <th>抓取频率</th>
                        <th>优先级</th>
                        <th>爬取时间</th>
                        <th>爬取数量</th>
                        <th>使用率</th>
                        <th>状态</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($tasks as $task)
                        <tr>
                            <td>{{ $task->id }}</td>
                            <td>{{ $task->name }}</td>
                            <td>{{ \Iwester\Http\Model\Spider\SpiderTask::$freqs[$task->freq] }}</td>
                            <td>{{ \Iwester\Http\Model\Spider\SpiderTask::$priority[$task->priority] }}</td>
                            <td>{{ $task->cur_spider_time }}</td>
                            @if($task->listArticle)
                                <td>{{ $task->listArticle->count() }}</td>
                                @php
                                    $r = $task->listArticle->where('status', 1)->count() / $task->listArticle->count() * 100;
                                @endphp
                                <td>{{ number_format($r, 2) }}%</td>
                                @else
                                <td>0</td>
                                <td>0</td>
                            @endif
                            <td>{{ \Iwester\Http\Model\Spider\SpiderTask::$status[$task->status] }}</td>
                            <td>
                                @if($task->status == 1)
                                <span class="layui-btn layui-btn-normal layui-btn-sm">
                                    <a style="color: #fff" href="{{ route('spider.task.article.create', ['id'=> $task->id]) }}"><i class="layui-icon layui-icon-edit"></i>编辑</a>
                                </span>
                                <span class="layui-btn layui-btn-danger layui-btn-sm" onclick="_del('{{ route('spider.task.destroy', ['id'=> $task->id]) }}')">
                                    <i class="layui-icon layui-icon-delete"></i>删除
                                </span>
                                @endif
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="9" style="text-align: center">暂无数据</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
                <div class="layui-box layui-laypage layui-laypage-default" id="">
                    {{$tasks->appends($params)->render()}}
                </div>
            </div>
        </div>
    </div>
    <script>
        function _del(url) {
            layer.confirm('确认删除该采集任务？', {
                btn: ['删除','取消'] //按钮
            }, function(){
                var loading = layer.load( 3, {
                    shade: [0.1,'#fff']
                });
                $.ajax({
                    url: url,
                    type: 'delete',
                    dataType: 'json',
                })
                    .done(function (data) {
                        if (data.code == 200) {
                            layer.alert(data.message, {
                                icon: 1,
                                closeBtn: 0,
                                shift: 3
                            }, function (index) {
                                layer.close(index);
                                location.reload();
                            });
                        } else {
                            layer.close(loading);
                            layer.alert(data.message, {icon: 2, shift: 3});
                        }
                    })
                    .fail(function () {
                        layer.close(loading);
                        layer.alert('服务器异常', {icon: 2, shift: 3});
                    });
            }, function(){

            });


        }
    </script>
@endsection


@section('script')

@endsection
