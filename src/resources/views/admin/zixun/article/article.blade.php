@extends('iwester::admin.base')
@section('css')
    <style>
        .layui-nav a {
            color: #666 !important;
        }
        .layui-nav .layui-nav-item a:hover, .layui-nav .layui-this a {
            color: #009688 !important;
        }
        .layui-laypage li, .layui-laypage li span {
            display: inline-block;
            *display: inline;
            *zoom: 1;
            vertical-align: middle;
            height: 28px;
            line-height: 28px;
            background-color: #fff;
            color: #333;
            font-size: 12px;
        }
        .layui-laypage li.active span{
            background-color: #009688;
            color: #fff;
        }
    </style>
@endsection

@section('content')
    <div class="layui-card">
        <div class="layui-card-body">
            <ul class="layui-nav" lay-filter="component-nav" style="background: #fff;padding: 0">
                @foreach($typeCount as $index=>$value)
                    <li class="layui-nav-item {{ $index== $type ? 'layui-this' : '' }}"><a href="{{ route('zixun.article') }}?type={{ $index }}">{{ $value[0] }}({{ $value[1] }})</a></li>
                @endforeach
                <span class="layui-nav-bar" style="left: 396px; top: 55px; width: 0px; opacity: 0;"></span></ul>
            <br>
            <div class="layui-form">
                <form action="">
                    @if($type)
                        <input type="hidden" name="type" value="{{ $type }}">
                    @endif
                    <div class="layui-input-inline">
                        <select name="category_id" lay-verify="" >
                            <option value="">请选择分类</option>
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->cate_name }}</option>
                                @include('iwester::admin.zixun.article.article_cate_el', ['category'=> $category, 'shendu'=> 1, 'select'=> isset($params['category_id']) ? $params['category_id'] : false])
                            @endforeach
                        </select>
                    </div>
                    <div class="layui-input-inline">
                        <input type="text" name="title" id="title" placeholder="请输入文章标题" class="layui-input" value="{{ isset($params['title']) ? $params['title']: '' }}">
                    </div>
                    <button type="submit" class="layui-btn">搜索</button>
                    @can('zixun.article.edit')
                        <a href="{{ route('zixun.article.edit') }}" class="layui-btn" style="float: right"><i class="layui-icon"></i>添加文章</a>
                        @if($type == 'audit')
                        <span style="float: right" onclick="_change_edit('{{ route('zixun.article.changeAudit') }}', '批量通过', 1)" class="layui-btn">
                            <i class="layui-icon layui-icon-edit"></i>批量通过
                        </span>
                        <span style="float: right" onclick="_change_edit('{{ route('zixun.article.changeAudit') }}', '批量不通过', -1)" class="layui-btn">
                            <i class="layui-icon layui-icon-edit"></i>批量不通过
                        </span>
                        @endif
                    @endcan
                </form>
            </div>
        </div>
        <div class="layui-card-body">
            <table class="layui-table" lay-skin="line">
                <colgroup>
                    <col width="80">
                    <col width="350">
                    <col width="90">
                    <col width="90">
                    <col width="90">
                    <col width="90">
                    <col width="120">
                    <col>
                </colgroup>
                <thead>
                <tr>
                    <th><input class="id_checked_all" type="checkbox">编号</th>
                    <th>标题</th>
                    <th>公开状态</th>
                    <th>审核状态</th>
                    <th>栏目</th>
                    <th>浏览量</th>
                    <th>发布时间</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                @foreach($articles as $article)
                    <tr>
                        <td>
                            <lable>
                                <input class="id_checked" type="checkbox" name="id" value="{{ $article->id }}">{{ $article->id }}
                            </lable>
                        </td>
                        <td>
                            <a style="color:#009688 !important;{{ $article->deleted_at ?  "text-decoration:line-through": '' }}" href="{{ route('article.detail',['id'=> $article->id]) }}" target="_blank" title="点击预览">
                                @if($article->deleted_at)
                                    <button class="layui-btn layui-btn-disabled layui-btn-radius layui-btn-xs">回收站</button>
                                @endif
                                @if($article->is_top == 1)
                                    <button class="layui-btn layui-btn-disabled layui-btn-radius layui-btn-xs " style="color: red">置顶</button>
                                @endif
                                {{ $article->title }}
                            </a>
                        </td>
                        <td>{{ $article->getPublishStatus() }}</td>
                        <td>{{ $article->getAuditStatus() }}</td>
                        <td>{{ $article->articleCategory->cate_name }}</td>
                        <td>{{ $article->view_count }}</td>
                        <td>{{ $article->published_at }}</td>
                        <td>
                            @can('zixun.article.edit')
                                <a href="{{ route('zixun.article.edit', ['id'=> $article->id]) }}">
                                    <span class="layui-btn layui-btn-normal layui-btn-sm">
                                        <i class="layui-icon layui-icon-edit"></i>编辑
                                    </span>
                                </a>
                                @if($type != 'deleted')
                                    @php
                                        $top = $article->is_top == 1 ? '取消置顶' : '添加置顶';
                                    @endphp
                                    <span onclick="_change_top('{{ route('zixun.article.changeTop', ['id'=> $article->id]) }}', '{{ $top }}', {{ $article->is_top == 1 ? 0 : 1 }})" class="layui-btn layui-btn-normal layui-btn-sm">
                                        {{ $top }}
                                    </span>

                                @endif
                            @endcan
                            @can('zixun.article.destroy')
                                @if($article->deleted_at)
                                    <span class="layui-btn layui-btn-danger layui-btn-sm" onclick="_del('{{ route('zixun.article.destroy', ['id'=> $article->id]) }}')">
                                        <i class="layui-icon layui-icon-delete"></i>清除
                                    </span>
                                    <span style="margin-left: 0" class="layui-btn layui-btn-normal layui-btn-sm" onclick="_del('{{ route('zixun.article.recover', ['id'=> $article->id]) }}')">
                                        <i class="layui-icon layui-icon-ok"></i>恢复
                                    </span>
                                @else
                                    <span class="layui-btn layui-btn-danger layui-btn-sm" onclick="_del('{{ route('zixun.article.destroy', ['id'=> $article->id]) }}')">
                                        <i class="layui-icon layui-icon-delete"></i>删除
                                    </span>
                                @endif
                            @endcan
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="layui-box layui-laypage layui-laypage-default" id="">
                {{$articles->appends($params)->render()}}
            </div>
        </div>
    </div>

    <script>
        $('.layui-nav li a').on('click', function () {
            var loading = layer.load( 3, {
                shade: [0.1,'#fff']
            });
        })
        function huifu(url) {
            layer.confirm('确认恢复该文章？', {
                btn: ['确认','取消'] //按钮
            }, function(){
                var loading = layer.load( 3, {
                    shade: [0.1,'#fff']
                });
                $.ajax({
                    url: url,
                    type: 'post',
                    dataType: 'json',
                })
                    .done(function (data) {
                        if (data.code == 200) {
                            layer.alert(data.message, {
                                icon: 1,
                                closeBtn: 0,
                                shift: 3
                            }, function (index) {
                                layer.close(index);
                                $(".id_checked").removeProp("checked");
                                location.reload();
                            });
                        } else {
                            layer.close(loading);
                            layer.alert(data.message, {icon: 2, shift: 3});
                        }
                    })
                    .fail(function () {
                        layer.close(loading);
                        layer.alert('服务器异常', {icon: 2, shift: 3});
                    });
            }, function(){

            });


        }
        function _del(url) {
            layer.confirm('确认删除该文章？', {
                btn: ['删除','取消'] //按钮
            }, function(){
                var loading = layer.load( 3, {
                    shade: [0.1,'#fff']
                });
                $.ajax({
                    url: url,
                    type: 'delete',
                    dataType: 'json',
                })
                    .done(function (data) {
                        if (data.code == 200) {
                            layer.alert(data.message, {
                                icon: 1,
                                closeBtn: 0,
                                shift: 3
                            }, function (index) {
                                layer.close(index);
                                $(".id_checked").removeProp("checked");
                                location.reload();
                            });
                        } else {
                            layer.close(loading);
                            layer.alert(data.message, {icon: 2, shift: 3});
                        }
                    })
                    .fail(function () {
                        layer.close(loading);
                        layer.alert('服务器异常', {icon: 2, shift: 3});
                    });
            }, function(){

            });


        }
        function _change_top(url, title, type) {
            layer.confirm('确认'+title+'？', {
                title: title,
                btn: ['确认','取消'] //按钮
            }, function(){
                var loading = layer.load( 3, {
                    shade: [0.1,'#fff']
                });
                $.ajax({
                    url: url+'?type='+type,
                    type: 'POST',
                    dataType: 'json',
                })
                    .done(function (data) {
                        if (data.code == 200) {
                            layer.alert(data.message, {
                                icon: 1,
                                closeBtn: 0,
                                shift: 3
                            }, function (index) {
                                layer.close(index);
                                $(".id_checked").removeProp("checked");
                                location.reload();
                            });
                        } else {
                            layer.close(loading);
                            layer.alert(data.message, {icon: 2, shift: 3});
                        }
                    })
                    .fail(function () {
                        layer.close(loading);
                        layer.alert('服务器异常', {icon: 2, shift: 3});
                    });
            }, function(){

            });


        }
        
        function _change_edit(url, title, type) {
            if(idArr.length == 0){
                layer.msg('请选择需要操作的数据');
                return;
            }
            layer.confirm('确认'+title+'？', {
                title: title,
                btn: ['确认','取消'] //按钮
            }, function(){
                var loading = layer.load( 3, {
                    shade: [0.1,'#fff']
                });
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        'ids' : JSON.stringify(idArr),
                        'type': type
                    }
                })
                    .done(function (data) {
                        if (data.code == 200) {
                            layer.alert(data.message, {
                                icon: 1,
                                closeBtn: 0,
                                shift: 3
                            }, function (index) {
                                layer.close(index);
                                $(".id_checked").removeProp("checked");
                                location.reload();
                            });
                        } else {
                            layer.close(loading);
                            layer.alert(data.message, {icon: 2, shift: 3});
                        }
                    })
                    .fail(function () {
                        layer.close(loading);
                        layer.alert('服务器异常', {icon: 2, shift: 3});
                    });
            }, function(){

            });


        }

        // 全选
        $(".id_checked_all").click(function(){
            if($(this).prop("checked")){
                $(".id_checked").prop("checked",true);
            }else{
                $(".id_checked").removeProp("checked");
            }
            inputDown();
        });
        $(".id_checked").click(function(){
            inputDown();
        });

        var idArr = [];
        function inputDown() {
            idArr = [];
            var idArr1 = [];
            var len = $('.id_checked').length;
            for(var i = 0; i<len; i++){
                var $this = $('.id_checked').eq(i);
                if($this.prop('checked')){
                    var id = $this.val();
                    idArr1.push(id)
                }
            }
            for(var t=0;t<idArr1.length;t++){
                tArray(idArr1[t],idArr);
            }
            if (idArr.length == len) {
                $(".id_checked_all").prop("checked",true);
            }else{
                $(".id_checked_all").removeProp("checked");
            }
            console.log(idArr)
        }

        //数组去重
        function tArray(i,arr){
            var yap=false;
            for(var j=0;j<arr.length;j++){
                if(arr[j]==i){yap=true;break;};
            }
            if(!yap) arr.push(i);
        };
    </script>
@endsection


@section('script')

@endsection
