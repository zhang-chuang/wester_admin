@extends('iwester::admin.base')

@section('content')
    <div class="layui-card">
        <div class="layui-card-body">
            <form class="layui-form" name="forms" method="post" id="forms">
                {{csrf_field()}}
                <input type="hidden" name="id" value="{{ $category->id }}">
                @if($parentCategory)
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">父级栏目</label>
                    <input type="hidden" name="cate[parent_id]" value="{{ $parentCategory->id }}">
                    <div class="layui-input-block">
                        <input disabled type="text" value="{{ $parentCategory->cate_name }}" lay-verify="" class="layui-input layui-btn-disabled" >
                    </div>
                </div>
                @endif
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">栏目名称</label>
                    <div class="layui-input-block">
                        <input type="text" name="cate[cate_name]" value="{{ $category->cate_name }}" lay-verify="" placeholder="请输入栏目名称" class="layui-input" >
                    </div>
                </div>
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">栏目别名</label>
                    <div class="layui-input-block">
                        <input type="text" name="cate[alias]" value="{{ $category->alias }}" lay-verify="" placeholder="请输入栏目别名" class="layui-input" >
                        <p style="color: red;margin-top: 4px; font-size: 12px;">路由别名用于生成url，请使用英文，填写后请勿修改</p>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">详情URL</label>
                    <div class="layui-input-block">
                        <input type="text" name="cate[prefix]" value="{{ $category->prefix ?? 'article' }}" lay-verify="" placeholder="请输入详情URL前缀" class="layui-input" >
                        <p style="color: red;margin-top: 4px; font-size: 12px;">用于生成栏目详情url</p>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">排序</label>
                    <div class="layui-input-block">
                        <input type="number" name="cate[sort]" value="{{ $category->sort }}" lay-verify="number" placeholder="排序" class="layui-input" >
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script src="/vendor/iwester/js/jquery.min.js"></script>
    <script>
        var formdata = function () {
            return $("#forms").serializeArray();
        }
    </script>
@endsection


@section('script')
@endsection
