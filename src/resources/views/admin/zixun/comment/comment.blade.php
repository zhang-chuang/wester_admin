@extends('iwester::admin.base')
@section('css')
    <style>
        .layui-nav a {
            color: #666 !important;
        }
        .layui-nav .layui-nav-item a:hover, .layui-nav .layui-this a {
            color: #009688 !important;
        }
        .layui-laypage li, .layui-laypage li span {
            display: inline-block;
            *display: inline;
            *zoom: 1;
            vertical-align: middle;
            height: 28px;
            line-height: 28px;
            background-color: #fff;
            color: #333;
            font-size: 12px;
        }
        .layui-laypage li.active span{
            background-color: #009688;
            color: #fff;
        }
    </style>
@endsection

@section('content')
    <div class="layui-card">
        <div class="layui-card-body">
            <div class="layui-form">
                <form action="">
                    <div class="layui-input-inline">
                        <select name="audit_status" lay-verify="" >
                            <option value="" {{ !$auditStatus ? 'selected' : '' }}>请选择审核状态</option>
                            <option value="1" {{ $auditStatus == '1' ? 'selected' : '' }}>未审核</option>
                            <option value="0" {{ $auditStatus == '0' ? 'selected' : '' }}>审核通过</option>
                            <option value="-1" {{ $auditStatus == '-1' ? 'selected' : '' }}>审核不通过</option>
                        </select>
                    </div>
                    <button type="submit" class="layui-btn">搜索</button>
                    <span style="float: right" onclick="_change_edit('{{ route('zixun.article.comment.changeAudit') }}', '批量通过', 0)" class="layui-btn">
                        批量通过
                    </span>
                    <span style="float: right" onclick="_change_edit('{{ route('zixun.article.comment.changeAudit') }}', '批量不通过', -1)" class="layui-btn layui-btn-danger">
                        批量不通过
                    </span>
                    <span style="float: right" onclick="_del('{{ route('zixun.article.comment.destroy') }}')" class="layui-btn layui-btn-danger">
                        <i class="layui-icon layui-icon-delete"></i>批量删除
                    </span>
                </form>
            </div>
        </div>
        <div class="layui-card-body">
            <table class="layui-table" lay-skin="line">
                <colgroup>
                    <col width="80">
                    <col width="350">
                    <col width="350">
                    <col width="90">
                    <col width="120">
                </colgroup>
                <thead>
                <tr>
                    <th><input class="id_checked_all" type="checkbox">编号</th>
                    <th>文章标题</th>
                    <th>评论内容</th>
                    <th>审核状态</th>
                    <th>评论时间</th>
                </tr>
                </thead>
                <tbody>
                @foreach($comments as $comment)
                    <tr>
                        <td>
                            <lable>
                                <input class="id_checked" type="checkbox" name="id" value="{{ $comment->id }}">{{ $comment->id }}
                            </lable>
                        </td>
                        <td>
                            <a style="color:#009688 !important;" href="{{ route('article.detail',['id'=> $comment->article->id]) }}" target="_blank" title="点击预览">
                                {{ $comment->article->title }}
                            </a>
                        </td>
                        <td>{{ $comment->comment_content }}</td>
                        <td>{{ $comment->getAuditStatus() }}</td>
                        <td>{{ $comment->created_at }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="layui-box layui-laypage layui-laypage-default" id="">
                {{$comments->appends($params)->render()}}
            </div>
        </div>
    </div>

    <script>

        function _del(url) {
            layer.confirm('确认删除该评论？', {
                btn: ['删除','取消'] //按钮
            }, function(){
                var loading = layer.load( 3, {
                    shade: [0.1,'#fff']
                });
                $.ajax({
                    url: url,
                    type: 'delete',
                    dataType: 'json',
                    data: {
                        'ids' : JSON.stringify(idArr),
                    }
                })
                    .done(function (data) {
                        if (data.code == 200) {
                            layer.alert(data.message, {
                                icon: 1,
                                closeBtn: 0,
                                shift: 3
                            }, function (index) {
                                layer.close(index);
                                $(".id_checked").removeProp("checked");
                                location.reload();
                            });
                        } else {
                            layer.close(loading);
                            layer.alert(data.message, {icon: 2, shift: 3});
                        }
                    })
                    .fail(function () {
                        layer.close(loading);
                        layer.alert('服务器异常', {icon: 2, shift: 3});
                    });
            }, function(){

            });


        }

        
        function _change_edit(url, title, type) {
            if(idArr.length == 0){
                layer.msg('请选择需要操作的数据');
                return;
            }
            layer.confirm('确认'+title+'？', {
                title: title,
                btn: ['确认','取消'] //按钮
            }, function(){
                var loading = layer.load( 3, {
                    shade: [0.1,'#fff']
                });
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        'ids' : JSON.stringify(idArr),
                        'type': type
                    }
                })
                    .done(function (data) {
                        if (data.code == 200) {
                            layer.alert(data.message, {
                                icon: 1,
                                closeBtn: 0,
                                shift: 3
                            }, function (index) {
                                layer.close(index);
                                $(".id_checked").removeProp("checked");
                                location.reload();
                            });
                        } else {
                            layer.close(loading);
                            layer.alert(data.message, {icon: 2, shift: 3});
                        }
                    })
                    .fail(function () {
                        layer.close(loading);
                        layer.alert('服务器异常', {icon: 2, shift: 3});
                    });
            }, function(){

            });


        }

        // 全选
        $(".id_checked_all").click(function(){
            if($(this).prop("checked")){
                $(".id_checked").prop("checked",true);
            }else{
                $(".id_checked").removeProp("checked");
            }
            inputDown();
        });
        $(".id_checked").click(function(){
            inputDown();
        });

        var idArr = [];
        function inputDown() {
            idArr = [];
            var idArr1 = [];
            var len = $('.id_checked').length;
            for(var i = 0; i<len; i++){
                var $this = $('.id_checked').eq(i);
                if($this.prop('checked')){
                    var id = $this.val();
                    idArr1.push(id)
                }
            }
            for(var t=0;t<idArr1.length;t++){
                tArray(idArr1[t],idArr);
            }
            if (idArr.length == len) {
                $(".id_checked_all").prop("checked",true);
            }else{
                $(".id_checked_all").removeProp("checked");
            }
            console.log(idArr)
        }

        //数组去重
        function tArray(i,arr){
            var yap=false;
            for(var j=0;j<arr.length;j++){
                if(arr[j]==i){yap=true;break;};
            }
            if(!yap) arr.push(i);
        };
    </script>
@endsection


@section('script')

@endsection
