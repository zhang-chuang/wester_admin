@extends('iwester::admin.base')

@section('content')
    <div class="layui-card">
        <div class="layui-card-body">
            <form class="layui-form" name="forms" method="post" id="forms">
                {{csrf_field()}}
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">标签名称</label>
                    <div class="layui-input-block">
                        <input type="text" name="tag[tag]" value="{{ $tag ? $tag->tag : '' }}" lay-verify="required" placeholder="请输入标签名称" class="layui-input" >
                    </div>
                </div>
                @if($tag)
                <div class="layui-form-item">
                    <label class="layui-form-label">是否使用</label>
                    <div class="layui-input-block">
                        <input type="hidden" name="id" value="{{ $tag->id }}">
                        <input type="checkbox" value="1" name="tag[status]" lay-skin="switch" lay-text="ON|OFF" {{ $tag->status ==1 ? 'checked': '' }}>
                    </div>
                </div>
                @endif
            </form>
        </div>
    </div>
    <script src="/vendor/iwester/js/jquery.min.js"></script>
    <script>
        var formdata = function () {
            return $("#forms").serializeArray();
        }
    </script>
@endsection


@section('script')
@endsection
