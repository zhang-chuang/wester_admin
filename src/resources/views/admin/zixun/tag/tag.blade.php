@extends('iwester::admin.base')

@section('content')
    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <h2>标签管理
                @can('zixun.tag.edit')
                <span style="float: right" class="layui-btn layui-btn-normal layui-btn-sm" onclick="openPage('{{ route('zixun.tag.edit', ['id'=> 0]) }}', 'add')">
                    添加标签
                </span>
                @endcan
            </h2>
        </div>
        <div class="layui-card-body">
            <div class="layui-form">
                <table class="layui-table">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>标签名称</th>
                        <th>使用状态</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($tags as $tag)
                        <tr>
                            <td>{{ $tag->id }}</td>
                            <td>{{ $tag->tag }}</td>
                            <td>
                                <input type="checkbox" lay-skin="switch" lay-text="ON|OFF" {{ $tag->status ==1 ? 'checked' : '' }} disabled>
                            <td>
                                @can('zixun.tag.edit')
                                <span class="layui-btn layui-btn-normal layui-btn-sm" onclick="openPage('{{ route('zixun.tag.edit', ['id'=> $tag->id]) }}', 'edit')">
                                    <i class="layui-icon layui-icon-edit"></i>编辑
                                </span>
                                @endcan
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="4" style="text-align: center">暂无数据</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script>
        function openPage(data, type) {
            var openPage = layer.open({
                type: 2,
                title: type == 'add' ? '添加标签':'编辑标签',
                shadeClose: false,
                scrollbar: false,
                shift: 3,
                btn: ['提交', '取消'],
                shade: 0.2,
                min: true,
                area: ['700px', '300px'],
                content: [data],
                yes: function (index, layero) {
                    var dataform = $(layero).find("iframe")[0].contentWindow.formdata();
                    save(dataform, index, type);
                },
                cancel: function (index, layero) {
                    layer.close(index)
                }
            });
        }
        function save(data, layerindex, type){
            var loading = layer.load( 3, {
                shade: [0.1,'#fff']
            });
            $.ajax({
                url: '{{ route('zixun.tag.store') }}',
                type: 'POST',
                dataType: 'json',
                data: data,
            })
                .done(function (data) {
                    if (data.code == 200) {
                        layer.alert(data.message, {
                            icon: 1,
                            closeBtn: 0,
                            shift: 3
                        }, function (index) {
                            layer.close(index);
                            location.reload();
                        });
                    }else if (data.code == 422) {
                        layer.close(loading);
                        tip = '';
                        for (var i in data.errors) {
                            tip += data.errors[i][0] + '<br />';
                        }
                        layer.alert(tip, {icon: 2, shift: 3});
                    } else {
                        layer.close(loading);
                        layer.alert(data.message, {icon: 2, shift: 3});
                    }
                })
                .fail(function () {
                    layer.close(loading);
                    layer.alert('服务器异常', {icon: 2, shift: 3});
                });
        }

    </script>
@endsection


@section('script')

@endsection
