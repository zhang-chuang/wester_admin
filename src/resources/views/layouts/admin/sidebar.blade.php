<div class="layui-side layui-side-menu">
    <div class="layui-side-scroll">
        <div class="layui-logo" lay-href="{{route('admin.index')}}">
            <span>后台管理系统</span>
        </div>

        <ul class="layui-nav layui-nav-tree" lay-shrink="all" id="LAY-system-side-menu" lay-filter="layadmin-system-side-menu">
            <li data-name="home" class="layui-nav-item layui-nav-itemed">
                <a href="javascript:;" lay-tips="主页" lay-direction="2">
                    <i class="layui-icon layui-icon-home"></i>
                    <cite>后台管理</cite>
                </a>
                <dl class="layui-nav-child">
                    <dd data-name="console" class="layui-this">
                        <a lay-href="{{route('admin.index')}}">系统信息</a>
                    </dd>
                </dl>
            </li>
            @foreach($menus as $menu)
                @can($menu->name)
                    @if($siteConfig->on_book_model == 0  && strpos('book', $menu->name) !== false)
                        @continue
                    @endif
                    <li data-name="{{ $menu->name }}" class="layui-nav-item">
                        <a href="javascript:;" lay-tips="{{ $menu->display_name }}" lay-direction="2">
                            <i class="layui-icon {{ $menu->icon->class??'' }}"></i>
                            <cite>{{ $menu->display_name }}</cite>
                            @if($menu->subPermission->isNotEmpty())
                                <span class="layui-nav-more"></span>
                            @endif
                        </a>
                        @if($menu->subPermission->isNotEmpty())
                            <dl class="layui-nav-child">
                                @foreach($menu->subPermission as $secondMenu)
                                    @can($secondMenu->name)
                                        @if($siteConfig->on_book_model == 0 && strpos('book', $secondMenu->name) !== false)
                                            @continue
                                        @endif
                                        <dd data-name="{{ $secondMenu->name }}" >
                                        <a {{ $secondMenu->route == '' ? 'href=javascript:;' : 'lay-href='.route($secondMenu->route).'' }}>
                                            {{ $secondMenu->display_name }}
                                            @if($secondMenu->subPermission->isNotEmpty() && $secondMenu->route == '' )
                                            <span class="layui-nav-more"></span>
                                            @endif
                                        </a>
                                        @if($secondMenu->subPermission->isNotEmpty() && $secondMenu->route == '')
                                            <dl class="layui-nav-child">
                                                @foreach($secondMenu->subPermission as $threeMenu)
                                                    @can($threeMenu->name)
                                                    <dd data-name="{{ $threeMenu->name }}">
                                                        <a lay-href="{{ $threeMenu->route != '' ? route($threeMenu->route) : '#' }}">
                                                            {{ $threeMenu->display_name }}
                                                        </a>
                                                    </dd>
                                                    @endcan
                                                @endforeach
                                            </dl>
                                        @endif
                                    </dd>
                                    @endcan
                                @endforeach
                            </dl>
                        @endif
                    </li>
                @endcan
            @endforeach
            {{--
            <li data-name="seo.manage" class="layui-nav-item">
                <a href="javascript:;" lay-tips="SEO管理" lay-direction="2">
                    <i class="layui-icon layui-icon-app"></i>
                    <cite>SEO管理</cite>
                    <span class="layui-nav-more"></span>
                </a>
                <dl class="layui-nav-child">
                    <dd data-name="seo.siteconfig" >
                        <a lay-href="{{ route('seo.siteconfig') }}">站点SEO管理</a>
                    </dd>
                    <dd data-name="seo.zixun" class="">
                        <a href="javascript:;">资讯SEO管理<span class="layui-nav-more"></span></a>
                        <dl class="layui-nav-child">
                            <dd data-name="seo.zixun.category"><a lay-href="{{ route('seo.zixun.category') }}">栏目SEO管理</a></dd>
                            <dd data-name="seo.zixun.rule"><a lay-href="{{ route('seo.zixun.rule') }}">文章SEO规则</a></dd>
                        </dl>
                    </dd>
                    <dd data-name="seo.wenda">
                        <a href="javascript:;">问答SEO管理<span class="layui-nav-more"></span></a>
                        <dl class="layui-nav-child">
                            <dd data-name="list"><a lay-href="app/forum/list.html">帖子列表</a></dd>
                            <dd data-name="replys"><a lay-href="app/forum/replys.html">回帖列表</a></dd>
                        </dl>
                    </dd>
                    <dd data-name="seo.sitelink" >
                        <a lay-href="{{ route('seo.sitelink') }}">友情链接管理</a>
                    </dd>
                    <dd data-name="seo.sitemap" >
                        <a lay-href="{{ route('seo.sitemap') }}">sitemap管理</a>
                    </dd>
                </dl>
            </li>
            <li data-name="system.manage" class="layui-nav-item">
                <a href="javascript:;" lay-tips="系统管理" lay-direction="2">
                    <i class="layui-icon layui-icon-util"></i>
                    <cite>系统管理</cite>
                </a>
                <dl class="layui-nav-child">
                    <dd data-name="system.user" >
                        <a lay-href="{{ route('manage.user') }}">用户管理</a>
                    </dd>
                    <dd data-name="system.role" >
                        <a lay-href="{{ route('manage.role') }}">角色管理</a>
                    </dd>
                    <dd data-name="system.permission" >
                        <a lay-href="{{ route('manage.permission') }}">权限管理</a>
                    </dd>
                    <dd data-name="system.banner" >
                        <a lay-href="{{ route('manage.banner') }}">Banner管理</a>
                    </dd>
                </dl>
            </li>
            <li data-name="zixun.manage" class="layui-nav-item">
                <a href="javascript:;" lay-tips="资讯管理" lay-direction="2">
                    <i class="layui-icon layui-icon-util"></i>
                    <cite>资讯管理</cite>
                </a>
                <dl class="layui-nav-child">
                    <dd data-name="zixun.config" >
                       <a lay-href="{{ route('zixun.config') }}">资讯配置</a>
                    </dd>
                    <dd data-name="zixun.category" >
                        <a lay-href="{{ route('zixun.category') }}">分类管理</a>
                    </dd>
                    <dd data-name="zixun.tag" >
                        <a lay-href="{{ route('zixun.tag') }}">标签管理</a>
                    </dd>
                    <dd data-name="zixun.article" >
                        <a lay-href="{{ route('zixun.article') }}">文章管理</a>
                    </dd>
                    <dd data-name="zixun.article" >
                        <a lay-href="http://www.layuiadmin.com/admin/article">评论管理</a>
                    </dd>
                </dl>
            </li>
            <li data-name="message.manage" class="layui-nav-item">
                <a href="javascript:;" lay-tips="消息管理" lay-direction="2">
                    <i class="layui-icon layui-icon-util"></i>
                    <cite>消息管理</cite>
                </a>
                <dl class="layui-nav-child">
                    <dd data-name="message.private" >
                        <a lay-href="">我的消息</a>
                    </dd>
                </dl>
            </li>
            --}}
        </ul>
    </div>
</div>