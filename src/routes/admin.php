<?php
/*
| 前台交互路由
*/
Route::namespace('\Iwester\Http\Controllers')->group(function () {
    //资讯交互
    Route::group(['namespace' => 'Admin\Zixun', 'prefix' => 'interactive', 'middleware' => ['auth']], function () {
        //文章 - 评论点赞
        Route::post('article/comment/zan{id}', 'AdminArticleController@commentZan')->name('zixun.article.comment.zan');
        // 文章 - 点赞
        Route::post('article/zan{id}', 'AdminArticleController@articleZan')->name('zixun.article.zan');
        //文章 - 提交评论
        Route::post('article/comment/store', 'AdminArticleController@commentStore')->name('zixun.article.comment.store');

    });
});

/*
| 后台公共路由部分
*/
Route::namespace('\Iwester\Http\Controllers')->group(function () {
    // middleware('permission:system.role|system.user|system.permission');

    //发送邮件
    Route::post('sendEmail', 'Admin\BaseController@sendEmail')->name('admin.sendEmail');

    // 用户登录、注销
    Route::group(['namespace' => 'Auth', 'prefix' => 'admin'], function () {
        //登录、注销
        Route::get('login', 'LoginController@showLoginForm')->name('admin.loginForm');
        Route::post('login', 'LoginController@login')->name('admin.login');
        Route::get('logout', 'LoginController@logout')->name('admin.logout');

        Route::post('register', 'RegisterController@register')->name('admin.register');
        Route::post('resetPassword', 'ResetPasswordController@resetPassword')->name('admin.resetPassword');
    });

    //后台管理
    Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => 'auth'], function () {
        //后台布局
        Route::get('/', 'AdminIndexController@layout')->name('admin.layout');
        //后台首页
        Route::get('/index', 'AdminIndexController@index')->name('admin.index');
        //图标
        Route::get('icons', 'AdminIndexController@icons')->name('admin.icons');
        //上传图片
        Route::post('/uploadImg', 'BaseController@uploadImg')->name('uploadImg');

    });

    //个人中心
    Route::group(['namespace' => 'Admin\Personal', 'prefix' => 'admin', 'middleware' => ['auth', 'permission:personal.manage']], function () {
        // 账号信息
        Route::group(['middleware'=>['permission:account.information']],function (){
            Route::get('account/information', 'AdminAccountController@information')->name('account.information');
            Route::post('account/information/store', 'AdminAccountController@informationStore')->name('account.information.store');
        });

        // 修改头像
        Route::group(['middleware'=>['permission:account.avatar']],function (){
            Route::get('account/avatar', 'AdminAccountController@avatar')->name('account.avatar');
            Route::post('account/avatar/store', 'AdminAccountController@avatarStore')->name('account.avatar.store');
        });

        // 修改密码
        Route::group(['middleware'=>['permission:account.password']],function (){
            Route::get('account/password', 'AdminAccountController@password')->name('account.password');
            Route::post('account/password/store', 'AdminAccountController@resetPassword')->name('account.password.store');
        });

        //账号绑定
        Route::group(['middleware'=>['permission:account.bind']],function (){
            Route::get('account/bind', 'AdminAccountController@bind')->name('account.bind');
        });
    });

    //SEO管理  seo.manage
    Route::group(['namespace' => 'Admin\Seo', 'prefix' => 'admin', 'middleware' => ['auth', 'permission:seo.manage']], function () {

        // 资讯栏目SEO管理
        Route::group(['middleware'=>['permission:seo.zixun.category']],function (){
            Route::get('seo/zixun/category', 'AdminSeoZixunController@category')->name('seo.zixun.category');
            Route::get('seo/zixun/category/edit{id}', 'AdminSeoZixunController@categoryEdit')->name('seo.zixun.category.edit')->middleware('permission:seo.zixun.category.edit');
            Route::post('seo/zixun/category/store', 'AdminSeoZixunController@categoryStore')->name('seo.zixun.category.store')->middleware('permission:seo.zixun.category.edit');
        });

        // 资讯SEO规则管理
        Route::group(['middleware'=>['permission:seo.zixun.rule']],function (){
            Route::get('seo/zixun/rule', 'AdminSeoZixunController@rule')->name('seo.zixun.rule');
            Route::get('seo/zixun/rule/edit{id}', 'AdminSeoZixunController@ruleEdit')->name('seo.zixun.rule.edit')->middleware('permission:seo.zixun.rule.edit');
            Route::post('seo/zixun/rule/store', 'AdminSeoZixunController@ruleStore')->name('seo.zixun.rule.store')->middleware('permission:seo.zixun.rule.edit');
        });

        //友情链接
        Route::group(['middleware'=>['permission:seo.sitelink']],function (){
            Route::get('seo/sitelink', 'AdminSeoLinkController@sitelink')->name('seo.sitelink');
            //添加| 编辑
            Route::get('seo/sitelink/edit{id?}', 'AdminSeoLinkController@edit')->name('seo.sitelink.edit')->middleware('permission:seo.sitelink.edit');
            //保存
            Route::post('seo/sitelink/store', 'AdminSeoLinkController@store')->name('seo.sitelink.store')->middleware('permission:seo.sitelink.edit');
            //删除
            Route::delete('seo/sitelink/destroy{id}', 'AdminSeoLinkController@destroy')->name('seo.sitelink.destroy')->middleware('permission:seo.sitelink.destroy');
        });

        //sitemap
        Route::group(['middleware'=>['permission:seo.sitemap']],function (){
            Route::get('seo/sitemap', 'AdminSeoSitemapController@sitemap')->name('seo.sitemap');
            Route::post('seo/sitemap/update', 'AdminSeoSitemapController@update')->name('seo.sitemap.update');
        });

        // 书籍栏目SEO管理
        Route::group(['middleware'=>['permission:seo.book.category']],function (){
            Route::get('seo/book/category', 'AdminSeoBookController@category')->name('seo.book.category');
            Route::get('seo/book/category/edit{id}', 'AdminSeoBookController@categoryEdit')->name('seo.book.category.edit')->middleware('permission:seo.book.category.edit');
            Route::post('seo/book/category/store', 'AdminSeoBookController@categoryStore')->name('seo.book.category.store')->middleware('permission:seo.book.category.edit');
        });

        // 书籍SEO规则管理
        Route::group(['middleware'=>['permission:seo.book.rule']],function (){
            Route::get('seo/book/rule', 'AdminSeoBookController@rule')->name('seo.book.rule');
            Route::get('seo/book/rule/edit{id}', 'AdminSeoBookController@ruleEdit')->name('seo.book.rule.edit')->middleware('permission:seo.book.rule.edit');
            Route::post('seo/book/rule/store', 'AdminSeoBookController@ruleStore')->name('seo.book.rule.store')->middleware('permission:seo.book.rule.edit');
        });
    });

    //资讯管理 zixun.manage
    Route::group(['namespace' => 'Admin\Zixun', 'prefix' => 'admin', 'middleware' => ['auth', 'permission:zixun.manage']], function () {
        //资讯 - 分类管理
        Route::group(['middleware'=>['permission:zixun.category']],function (){
            Route::get('category', 'AdminCategoryController@category')->name('zixun.category');
            //添加分类
            Route::get('category/create/{id}', 'AdminCategoryController@create')->name('zixun.category.create')->middleware('permission:zixun.category.create');
            Route::post('category/store', 'AdminCategoryController@store')->name('zixun.category.store')->middleware('permission:zixun.category.create');
            // 编辑分类
            Route::get('category/edit/{pId}/{id}', 'AdminCategoryController@edit')->name('zixun.category.edit')->middleware('permission:zixun.category.edit');
            Route::post('category/update', 'AdminCategoryController@update')->name('zixun.category.update')->middleware('permission:zixun.category.edit');
             // 删除分类
            Route::delete('category/destroy{id}', 'AdminCategoryController@destroy')->name('zixun.category.destroy')->middleware('permission:zixun.category.destroy');
        });

        //资讯 - 标签管理
        Route::group(['middleware'=>['permission:zixun.tag']],function (){
            Route::get('tag', 'AdminTagController@tag')->name('zixun.tag');
            //添加
            Route::get('tag/edit{id?}', 'AdminTagController@edit')->name('zixun.tag.edit')->middleware('permission:zixun.tag.edit');
            Route::post('tag/store', 'AdminTagController@store')->name('zixun.tag.store')->middleware('permission:zixun.tag.edit');
            //删除
            Route::delete('tag/destroy{id}', 'AdminTagController@destroy')->name('zixun.tag.destroy')->middleware('permission:zixun.tag.destroy');
        });

        //资讯 - 文章管理
        Route::group(['middleware'=>['permission:zixun.article']],function (){
            Route::get('article', 'AdminArticleController@article')->name('zixun.article');
            //添加
            Route::get('article/edit{id?}', 'AdminArticleController@edit')->name('zixun.article.edit')->middleware('permission:zixun.article.edit');
            Route::post('article/store', 'AdminArticleController@store')->name('zixun.article.store')->middleware('permission:zixun.article.edit');
            //删除
            Route::delete('article/destroy{id}', 'AdminArticleController@destroy')->name('zixun.article.destroy')->middleware('permission:zixun.article.destroy');
            //恢复
            Route::post('article/recover{id}', 'AdminArticleController@recover')->name('zixun.article.recover')->middleware('permission:zixun.article.destroy');
            //置顶
            Route::post('article/changeTop{id}', 'AdminArticleController@changeTop')->name('zixun.article.changeTop')->middleware('permission:zixun.article.edit');
            //批量审核
            Route::post('article/changeAudit', 'AdminArticleController@changeAudit')->name('zixun.article.changeAudit')->middleware('permission:zixun.article.edit');
        });

        //资讯 - 评论管理
        Route::group(['middleware'=>['permission:zixun.article.comment']],function (){
            Route::get('article/comment', 'AdminArticleCommentController@comment')->name('zixun.article.comment');
            //删除
            Route::delete('article/comment/destroy', 'AdminArticleCommentController@destroy')->name('zixun.article.comment.destroy');
            //批量审核
            Route::post('article/comment/changeAudit', 'AdminArticleCommentController@changeAudit')->name('zixun.article.comment.changeAudit');
        });
    });

    //书籍管理 book.manage
    Route::group(['namespace' => 'Admin\Book', 'prefix' => 'admin/book', 'middleware' => ['auth', 'permission:book.manage']], function () {
        //书籍 - 分类管理
        Route::group(['middleware'=>['permission:book.category']],function (){
            Route::get('category', 'AdminBookCategoryController@category')->name('book.category');
            //添加分类
            Route::get('category/create/{id}', 'AdminBookCategoryController@create')->name('book.category.create')->middleware('permission:book.category.create');
            Route::post('category/store', 'AdminBookCategoryController@store')->name('book.category.store')->middleware('permission:book.category.create');
            // 编辑分类
            Route::get('category/edit/{pId}/{id}', 'AdminBookCategoryController@edit')->name('book.category.edit')->middleware('permission:book.category.edit');
            Route::post('category/update', 'AdminBookCategoryController@update')->name('book.category.update')->middleware('permission:book.category.edit');
            // 删除分类
            Route::delete('category/destroy{id}', 'AdminBookCategoryController@destroy')->name('book.category.destroy')->middleware('permission:book.category.destroy');
        });

        //书籍 - 书架管理
        Route::group(['middleware'=>['permission:book.bookshelf']],function (){
            Route::get('bookshelf', 'AdminBookShelfController@bookshelf')->name('book.bookshelf');
            //添加
            Route::get('bookshelf/edit{id?}', 'AdminBookShelfController@edit')->name('book.bookshelf.edit')->middleware('permission:book.bookshelf.edit');
            Route::post('bookshelf/store', 'AdminBookShelfController@store')->name('book.bookshelf.store')->middleware('permission:book.bookshelf.edit');
            //删除
            Route::delete('bookshelf/destroy{id}', 'AdminBookShelfController@destroy')->name('book.bookshelf.destroy')->middleware('permission:book.bookshelf.destroy');
            // ook.bookshelf.chapters
            //章节管理
            Route::get('bookshelf{id}/chapters', 'AdminBookShelfController@chapters')->name('book.bookshelf.chapters')->middleware('permission:book.bookshelf.edit');
            //章节编辑
            Route::get('chapter{id}', 'AdminBookShelfController@chapterEdit')->name('book.chapters.edit')->middleware('permission:book.bookshelf.edit');
            Route::post('chapter/store', 'AdminBookShelfController@chapterStore')->name('book.chapters.store')->middleware('permission:book.bookshelf.edit');
        });

        //书籍 - 作者管理
        Route::group(['middleware'=>['permission:book.author']],function (){
            Route::get('author', 'AdminBookAuthorController@author')->name('book.author');
            Route::get('author/edit{id}', 'AdminBookAuthorController@edit')->name('book.author.edit');
            Route::post('author/store', 'AdminBookAuthorController@store')->name('book.author.store');
        });
        //书籍 - 阅读日志管理
        Route::group(['middleware'=>['permission:book.chapter.read_log']],function (){
            Route::get('chapter/read_log', 'AdminBookShelfController@bookReadLog')->name('book.chapter.read_log');
        });

    });

    //系统管理 admin.manage
    Route::group(['namespace' => 'Admin\Manage', 'prefix' => 'admin', 'middleware' => ['auth', 'permission:system.manage']], function () {

        // 站点管理
        Route::group(['middleware'=>['permission:manage.siteconfig']],function (){
            Route::get('siteconfig', 'AdminSiteController@siteconfig')->name('manage.siteconfig');
            Route::post('siteconfig/store', 'AdminSiteController@store')->name('manage.siteconfig.store');
        });

        //用户管理
        Route::group(['middleware'=>['permission:manage.user']],function (){
            Route::get('user', 'AdminUserController@user')->name('manage.user');
            //添加
            Route::get('user/create', 'AdminUserController@create')->name('manage.user.create')->middleware('permission:manage.user.create');
            Route::post('user/store', 'AdminUserController@store')->name('manage.user.store')->middleware('permission:manage.permission.edit');
            //编辑
            Route::get('user/edit{id}', 'AdminUserController@edit')->name('manage.user.edit')->middleware('permission:manage.user.edit');
            Route::post('user/update', 'AdminUserController@update')->name('manage.user.update')->middleware('permission:manage.user.edit');
            //冻结、恢复
            Route::post('user/change{id}', 'AdminUserController@change')->name('manage.user.change')->middleware('permission:manage.user.change');
            //分配角色
            Route::get('user{id}/role', 'AdminUserController@role')->name('manage.user.role')->middleware('permission:manage.user.role');
            Route::post('user{id}/role/store', 'AdminUserController@roleStore')->name('manage.user.role_store')->middleware('permission:manage.user.role');
            //分配权限
            Route::get('user{id}/permission', 'AdminUserController@permission')->name('manage.user.permission')->middleware('permission:manage.user.permission');
            Route::post('user{id}/permission/store', 'AdminUserController@permissionStore')->name('manage.user.permission_store')->middleware('permission:manage.user.permission');
        });

        //角色管理
        Route::group(['middleware'=>['permission:manage.role']],function (){
            Route::get('role', 'AdminRoleController@role')->name('manage.role');
            //添加
            Route::get('role/edit{id?}', 'AdminRoleController@edit')->name('manage.role.edit')->middleware('permission:manage.role.edit');
            Route::post('role/store', 'AdminRoleController@store')->name('manage.role.store')->middleware('permission:manage.role.edit');
            //删除
            Route::delete('role/destroy{id}', 'AdminRoleController@destroy')->name('manage.role.destroy')->middleware('permission:manage.role.destroy');
            //分配权限
            Route::get('role{id}/permission', 'AdminRoleController@permission')->name('manage.role.permission')->middleware('permission:manage.role.permission');
            Route::post('role{id}/permission/store', 'AdminRoleController@permissionStore')->name('manage.role.permission_store')->middleware('permission:manage.role.permission');
        });

        //权限管理
        Route::group(['middleware'=>['permission:manage.permission']],function (){
            Route::get('permission/{pId?}', 'AdminPermissionController@permission')->name('manage.permission');
            //添加权限
            Route::get('permission/edit/{pId}/{id?}', 'AdminPermissionController@edit')->name('manage.permission.edit')->middleware('permission:manage.permission.edit');
            Route::post('permission/store', 'AdminPermissionController@store')->name('manage.permission.store')->middleware('permission:manage.permission.edit');
            //删除权限
            Route::delete('permission/destroy{id}', 'AdminPermissionController@destroy')->name('manage.permission.destroy')->middleware('permission:manage.permission.destroy');
        });

        // 网站栏目管理
        Route::group(['middleware'=>['permission:manage.web_column']],function (){
            Route::get('webcolumn', 'AdminWebColumnController@webcolumn')->name('manage.web_column');
            //添加
            Route::get('webcolumn/edit{id?}', 'AdminWebColumnController@edit')->name('manage.web_column.edit')->middleware('permission:manage.web_column.edit');
            Route::post('webcolumn/store', 'AdminWebColumnController@store')->name('manage.web_column.store')->middleware('permission:manage.web_column.edit');
            //删除
            Route::delete('webcolumn/destroy{id}', 'AdminWebColumnController@destroy')->name('manage.web_column.destroy')->middleware('permission:manage.web_column.destroy');
        });

        // banner管理
        Route::group(['middleware'=>['permission:manage.banner']],function (){
            Route::get('banner', 'AdminBannerController@banner')->name('manage.banner');
            //添加
            Route::get('banner/edit{batch}/{id?}', 'AdminBannerController@edit')->name('manage.banner.edit')->middleware('permission:manage.banner.edit');
            Route::post('banner/store', 'AdminBannerController@store')->name('manage.banner.store')->middleware('permission:manage.banner.edit');
            //删除
            Route::delete('banner/destroy{batch}/{id?}', 'AdminBannerController@destroy')->name('manage.banner.destroy')->middleware('permission:manage.banner.destroy');
        });
    });

    //采集工具 system.spider
    Route::group(['namespace' => 'Admin\Spider', 'prefix' => 'admin', 'middleware' => ['auth', 'permission:system.spider']], function () {
        // 采集任务
        Route::get('/spider/task', 'AdminSpiderController@task')->name('spider.task');
        //添加文章采集
        Route::get('/spider/task/article/create{id?}', 'AdminSpiderController@taskArticleCreate')->name('spider.task.article.create');
        Route::post('/spider/task/article/store', 'AdminSpiderController@taskArticleStore')->name('spider.task.article.store');
        //测试文章列表规则
        Route::post('/spider/test/article/listTest', 'AdminSpiderController@testArticlelist')->name('spider.test.article.list');
        Route::post('/spider/test/article/detailTest', 'AdminSpiderController@testDetai')->name('spider.test.article.detail');
        //删除
        Route::delete('spider/task/destroy{id}', 'AdminSpiderController@destroy')->name('spider.task.destroy');

        //采集文章列表
        Route::get('/spider/task/article/caiji/list', 'AdminSpiderArticleController@list')->name('spider.task.article.caiji.list');
        //采集文章详情
        Route::get('/spider/task/article/caiji/edit{id}', 'AdminSpiderArticleController@edit')->name('spider.task.article.caiji.edit');
        //采集文章审核
        Route::delete('/spider/task/article/caiji/destroy{id}', 'AdminSpiderArticleController@destroy')->name('spider.task.article.caiji.destroy');
        //采集文章入库
        Route::post('/spider/task/article/caiji/store', 'AdminSpiderArticleController@store')->name('spider.task.article.caiji.store');

        Route::get('/spider/test', 'AdminSpiderController@test')->name('spider.test');
    });

});









